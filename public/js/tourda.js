var tour = new Tour({
  debug: false,
  storage: false,
   redirect: true,
     template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'>« Tilbage</button><span data-role='separator'> </span><button class='btn btn-default' data-role='next'>Næste »</button>   <button class='btn btn-default' data-role='end'>Slut</button>  </div>    </div>",
  steps: [
  {
    element: "#taketour",
    title: "Lad os tage en rundtur",
    content: "Velkommen til hjælpefunktionen. Du kan tage en rundtur ved at trykke næste. Ellers se en <a href='https://www.youtube.com/watch?v=sE2dPr9yjao'>video guide</a> eller læs <a href='http://fawour.com/help'>en artikkel</a>  om sitet (engelsk) <br><br> Først, lad os kigge på din profil.",
    placement: "right",
  }, 

{
    element: "#myimageofme",
    title: "Din profil",
    content: "Velkommen til din profil. Dig og andre kan se information om dig. Ting som profil billede, proofil beskrivelse, partners og hvilke platforme du er på.",
    placement: "right",
    path: "/profile#tour1",
  },  
 
    {
    element: "#Visitorsgiven",
    title: "Besøgende givet",
    content: "Dette tal viser hvor mange besøgende du har givet dine partnere.",
    placement: "right",
  }, 
  {
    element: "#Visitorsgotten",
    title: "Besøgende fået",
    content: "Dette tal viser hvor mange besøgende du har fået af dine partnere..",
    placement: "right",
  },
    {
    element: "#Visitorsratio",
    title: "Givet/fået balalcen",
    content: "Dette er en balance der viser hvor mange besøgende du har fået vs givet. Prøv at give så mange som du modtager :) <br><br>  Næste: Lad os opdatere din information",
    placement: "right",
  },  

      {
    element: "#description",
    title: "Information om dig",
    content: "På denne side kan ændre information om dig.",
    placement: "right",
    path: "/profile/edit#tour2",
  },  
    {
    element: "#field",
    title: "Dit felt",
    content: "Her kan du sætte hvilket felt du er i. Altså hvad dit indhold handler om",
    placement: "right",
  },  
      {
    element: "#lang",
    title: "Sprog",
    content: "Indstil hvilket sprog dit indhold er på. Dvs, selvom du er dansk men dit indhold er på engelsk, så sæt det til engelsk.",
    placement: "right",
  },  

    {
    element: "#BlogUrl",
    title: "Dig på nettet",
    content: "Til slut kan du indsætte links til forskellige medier du er på <br><br> Næste: Lad os lave nogle links",
    placement: "left",
  },  

      {
    element: "#url",
    title: "Lav nogle links",
    content: "På denne side kan du indesende linksne du gerne vil have flere besøgende på. Hvert link du indsender laver et share-link til dine partnere. Grunden til vi gør det, er at vi kan se hvor mange besøgende der har klikket på dit link.",
    placement: "right",
    path: "/links/create#tour3",
  }, 

           {
    element: "#description",
    title: "Link beskrivelse",
    content: "Beskriv kort her hvad dit link handler om. Det hjælper dine partnere med at beslutte sig om de skal dele dit link eller ej. <br><br> Næste: Lad os se på dine gemte links.",
    placement: "right",
  },  
    {
    element: "#linkAbout",
    title: "Dette er dine gemte links",
    content: "På denne side kan du se de links du har gemt. Lad os se hvad man kan her (tryk næste).",
    placement: "right",
    path: "/links/myLinks#tour4",
  }, 
       {
    element: "#linkdes",
    title: "Link beskrivelse",
    content: "Dette viser dig blot hvilken beskrivelse du skrev på linket.",
    placement: "right",
  }, 

         {
    element: "#linkgotten",
    title: "Besøgende fået",
    content: "Her kan du se den totale mængde af besøgende dine partnere har givet dig, på et link.",
    placement: "left",
  }, 
       {
    element: "#linkmore",
    title: "Mere",
    content: "Her er et par mulighedder. Du kan trykke på øjet for at se mere data for et link. Du kan ændre beskrivelsen, eller slette det ved at klikke på krydset.<br><br> Næste: Lad os se hvordan du finder partnere.",
    placement: "left",
  }, 
      {
    element: "#medium",
    title: "Udforsk partnere",
    content: "Her kan du finde partnere at arbejde sammen med. Du kan indstille søgningen: Hvilket felt skal de være i, hvilket sprog skal deres indhold være på og hvilke platforme skal partnerne være på. Du kan også søge mere avanceret ved at trykke på advaneret søgning. ",
    placement: "right",
    path: "/partners/explore#tour5",
  }, 

    {
    element: "#helpBlockloength",
    title: "Søgningen",
    content: "Dette er en blot en hjælpetekst for at tydeliggøre. <br> <br> Næste: Antaget du har nogle partnere nu, lad os se hvordan du giver dem besøgende.",
    placement: "left",
  }, 
       {
    element: "#byLinks",
    title: "Dine partners links",
    content: "På denne side kan du se de links dine partnere har gemt.",
    placement: "right",
    path: "/links/share#tour6",
  }, 

     {
    element: "#desLinks",
    title: "Link beksrivelse",
    content: "For at hjælpe dig med at bslutte, om du skal dele linket eller ej, er der her en beskrivelse af hvad linket handler om.",
    placement: "right",
  }, 
   {
    element: "#givenLinks",
    title: "Besøgende givet",
    content: "Dette nummer viser, hvor mange besøgende du har givet til dette link.",
    placement: "left",
  }, 
   {
    element: "#linkLinks",
    title: "Linket",
    content: "Dette er linket i sig selv. Der hvor følgerne bliver redirected til. Du kan klikke åp det, er se hvad det handler om.",
    placement: "left",
  }, 

      {
    element: "#shareLinks",
    title: "Hjælp dine partnere!",
    content: "Det er her det hele handler om. Her er linket du kan kopirer til din facebook post, blog post, eller youtube video osv. Når en følger klikker på dette share-link, bliver de redirected til de originale link. Samtidig kan vi tælle hvad der skete! Vi giver dig +1 på ‘besøgende givet’ og den pågældende partner får +1 i ‘besøgende fået’. <br><br> Næste: Se hvordan du skriver til dine partnere.",
    placement: "left",
  }, 
    {
    element: "#byContainer",
    title: "Privat besked",
    content: "Her kan du vælge hvilke af dine partnere du vil skrive til privat.",
    placement: "right",
    path: "/messages#tour7",
  }, 
     {
    element: "#messsContainer",
    title: "Besked området",
    content: "Hvis du har valgt en partner at skrive til, vil en besked-kasse poppe op. Her kan du skrive din besked og trykke send.<br><br> Næste: Lad os se hvordan du chatter med dine partnere",
    placement: "left",
  }, 

     
        {
    element: "#byContainer",
    title: "Chatten",
    content: "",
    placement: "right",
    path: "/chat#tour8",
  }, 

       {
    element: "#messcoun",
    title: "Chatten",
    content: "Her kan du skrive med alle brugere på sitet. Gør det venligst på engelsk :).",
    placement: "left",
  },

  
        {
    element: "#sectioonndcah",
    title: "Chat forum",
    content: " Chat om noget specifikt. Her kan du vælge hvilket niche du vil skrive om. Hvis du eksempelvis gerne vil diskutere gaming, så vælg gaming! Vær sød at diskuter på engelsk, så alle kan få glæde af debatten, og deltage. <br><br> Næste: Lad os se analytics modulet.",
    placement: "right",
   
  }, 
        {
    element: "#analytics1",
    title: "Besøgende fået og givet",
    content: "Denne side viser dig hvor mange besøgende du har givet og fået den sidste månede. Vist som en lækker graf!",
    placement: "right",
    path: "/analytics/visitors#tour9",
  }, 

            {
    element: "#analytics2",
    title: "Se hvem der gav dig besøgende",
    content: "I en dejlig cirkel diagram, kan du se fordeling af hvem der giver dig besøgende.",
    placement: "right",
   
  }, 
              {
    element: "#analytics3",
    title: "Besøgende du giver",
    content: "Igen et herligt diagram. Se her fordelingen over hvem du giver besgøende.<br><br> Dette er slutningen er rundturen. <a href='/profile#tour1'>Genstart?</a> <br><br> Stadig ikke sikker på hvordan man bruger sitet? <a href='https://www.youtube.com/watch?v=sE2dPr9yjao'>Se en video</a> eller <a href='http://fawour.com/help'>læs en artikkel om sitet</a> ellers <a href='https://docs.google.com/forms/d/e/1FAIpQLSez07_zXZHcpJoGlLaJ_QAtXNr2gxyKks5qWHXs3NrK6s_0qQ/viewform'>kontakt os</a>",
    placement: "right",
   
  }, 
]});