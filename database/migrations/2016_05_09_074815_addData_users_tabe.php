<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataUsersTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('users', function(Blueprint $table)
        {
            $table->boolean('EmailEach2dayNoti')->default(true)->after('verified');
            $table->boolean('EmailifNotActiveIn7Days')->default(true)->after('password');
            $table->timestamp('last_login')->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('EmailEach2dayNoti');
            $table->dropColumn('EmailifNotActiveIn7Days');
            $table->dropColumn('last_login');

        });
    }
}
