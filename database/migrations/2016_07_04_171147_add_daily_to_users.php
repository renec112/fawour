<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDailyToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
             $table->integer('GottenThisday')->after('ratio');
             $table->integer('GivenThisDay')->after('ratio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('GottenThisday');
            $table->dropColumn('GivenThisDay');
        });
    }
}
