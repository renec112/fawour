<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectedData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url_to_partner', function (Blueprint $table) {

            $table->increments('id');
            
            $table->integer('url_id')->unsigned();
            $table->foreign('url_id')->references('id')->on('url');

            $table->integer('trafic_from_user')->nullable();
            $table->integer('trafic_to_user')->nullable();

            $table->text('url');
            $table->integer('countgiven');
            $table->string('code');
            $table->boolean('can_be_shared')->default(true);

            $table->string('ip1')->nullable();
            $table->string('ip2')->nullable();
            $table->string('ip3')->nullable();
            $table->string('ip4')->nullable();
            $table->string('ip5')->nullable();    
            $table->string('lastIp')->nullable();      

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('url_to_partner');
    }
}
