<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('Avatarpath')->nullable();
            $table->text('description')->nullable();

            $table->boolean("verified")->default(false);
            $table->string("emailCode")->nullable();
            $table->string('password', 60);
            $table->rememberToken();
  
            $table->integer('notifications')->default(0);

            $table->boolean('show_menu')->default(true);
            $table->boolean('is_creator')->default(true);

            $table->integer('pointsGiven')->default(0);
            $table->integer('pointsGotten')->default(0);
            $table->float('ratio')->default(0);


            $table->string('field')->default("Not set");
            $table->string('lang')->default("Not set");

            $table->string('facebookUrl', 100)->nullable();
            $table->string('instagramUrl', 100)->nullable();
            $table->string('vineUrl', 100)->nullable();
            $table->string('pinterestUrl', 100)->nullable();
            $table->string('twitterUrl', 100)->nullable();
            $table->string('YoutubeUrl')->nullable();
            $table->string('tumblrUrl', 100)->nullable();
            $table->string('BlogUrl' , 100)->nullable();
            $table->string('googleplusUrl', 100)->nullable();
            $table->string('linkedinUrl', 100)->nullable();

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');

    }
}
