<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('activities', function (Blueprint $table) {

            $table->increments('id');

            $table->integer("user_id");
            $table->integer("from_id");
            $table->string("type");
            $table->string("route")->nullable();
            $table->string("data")->nullable();
            $table->boolean('notifyThis')->default(false);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activities');
    }
}
