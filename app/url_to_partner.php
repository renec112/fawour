<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Url;

class url_to_partner extends Model
{
	protected $table = 'url_to_partner';
	
	protected $fillable = [
		'url_id',
		'trafic_from_user',
		'trafic_to_user',
		'url',
		'countgiven',
		'code',
		'can_be_shared',
		'ip1',
		'ip2',
		'ip3',
		'ip4',
		'ip5',
		'lastIp',
	

	];

	public function urlCreate()
	{
		return $this->belongsTo("App\url");

	}



}