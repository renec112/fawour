<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\url_to_partner;


class Url extends Model
{
    /**
     * Fillable fields for a Flyer.
     *
     * @var array
     */
    protected $table = 'url';
    protected $fillable = [
        'url',
        'description',
        'code'
    ];


    /**
     * Find the Flyer at the given address.
     *
     * @param string $zip
     * @param string $street
     * @return Builder
     */


    public static function locatedAt($url_table_id, $code)
    {
        return static::where(compact('id', 'code'))->firstOrFail();
    }
    public static function getUrl($code)
    {
        return static::where(compact('code'))->firstOrFail();
    }
    /**
     * A flyer is composed of many photos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partnerUrls()
    {
        return $this->hasMany('App\url_to_partner');
    }

    public function createPartnersUrltoshare()
    {
        // Get the id of current created url
        $current_id = $this->flyers()->save($url)->id;
    }

    /**
     * A Flyer is owned by a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}