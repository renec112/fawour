<?php

namespace App\Http\Controllers;

use App\Url;
use App\User;
use App\url_to_partner;
use Auth;
use App\Http\Requests;
use App\Http\Requests\FlyerRequest;
use App\Http\Controllers\Controller;
use App\Activity;
use Illuminate\Http\Request;

class FlyersController extends Controller
{
    public function __construct()
    {
        // block access to all methods unless logged in
    	$this->middleware('auth', ['except' => ['show']]);

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }
    public function showfull($id)
    {
        $urlfull = Url::where("user_id", Auth::user()->id)->where("id", $id)->first();
        $mypartners = $this->user->partners()->pluck('id')->all();
        $partnerData = url_to_partner::
        join('users', 'users.id', '=', 'url_to_partner.trafic_from_user')
        ->select(['url_to_partner.*', 'users.name', 'users.email', 'users.id'])
        ->where("url_id", $id)
        ->whereIn("trafic_from_user",  $mypartners)->get();
        return view("urls.urlFull")->with("urlfull", $urlfull)->with("partnerData", $partnerData);
    }

    public function postFull(Request $request){
        $linkid = $request->input("linkid");
        foreach($request->input("user") as $user => $canshare)
            {
                url_to_partner::where("url_id",  $linkid)
                    ->where("trafic_from_user",  $user)
                    ->update([
                "can_be_shared" => $canshare,
                ]);
            }
        return redirect()->route('showfull', $linkid);
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(Auth::user()->ratio < 0.4 and Auth::user()->pointsGotten >100 ){
            flash()->overlay(trans('app.cantcratenewlinks'), trans('app.sharebeforemorehelp'));
            return redirect()->route('share_url');
        }

        return view('urls.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(FlyerRequest $request)
    {

        $this->user->publish(
            new url($request->all())
        );
        #update log
        $notification = new Activity;
        $notification->from_id ="0";
        $notification->user_id = $this->user->id;
        $notification->type = 'LinkCreatedByYou';
         $notification->data = $request->description;
        $notification->route = $this->user->name;
        $notification->save();

        // show succes and redirect
        flash()->success(trans('app.Success'),  trans('app.urlhasbeeenreated'));
        return redirect()->route('myUrls');
    }

    /**
     * Display the specified resource.
     *
     * @return Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
