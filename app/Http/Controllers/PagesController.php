<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use App\Chat;
use App\User;
use App\Url;
use App\messages;
use App\analytics;
use App\url_to_partner;
use App\Activity;
Use Carbon\Carbon;
use Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;


class PagesController extends Controller
{
    public function home()
    {
        
        $lastUsers = User::whereNotNull('description')->orderBy('created_at','desc')->take(6)->get();
        return view('pages.home')->with("lastUsers", $lastUsers);
    }
    public function about()
    {
        return view('pages.about');
    }
    public function example()
    {
        return view('pages.example');
    }

    public function messages()
    {
        if(!Auth::check()){
             return logInPlease();
        }

        if(Auth::check()){
            $lastMessage = null;
            if(\Request::input('from') != null){
                if(!Auth::user()->isPartnerWith(User::where("id", \Request::input('from'))->first())){
                     $latestmessage2 = messages::where("user_id", Auth::User()->id)->latest()->limit(1)->first();
                     // check if null
                     if(isset($latestmessage2)){
                         $lastMessage = $latestmessage2->created_at->diffForHumans();
                     }else{
                        $lastMessage = null;
                        
                     }
                     

                }

            }
            
        //xx
            $messages = messages::where(function($query){
                return $query->where("user_id", Auth::user()->id)
                ->where("to_user", \Request::input('from'))
                ->orWhere("user_id", \Request::input('from'))
                ->where("to_user", Auth::user()->id);
            })
                ->orderBy("created_at", "desc")
                ->simplePaginate(8);            
            $writingTo = User::where("id", \Request::input('from'))->first();
            return view("pages.messages")
                ->with("messages", $messages)
                ->with("writingTo", $writingTo)
                ->with("lastMessage", $lastMessage);
            }
        return view("home");
        


    }
    public function analyticblank(){
         return view("pages.analyticsmenu");
    }
    public function analytics()
    
    {
        if(!Auth::check()){
             return logInPlease();
        }else{
            $data = analytics::where("user_id",  Auth::user()->id)->orderBy("created_at", "desc")->get()->reverse();
        
            return view("pages.analytics")->with("data", $data);
        }

    }
     public function analytics2()
    {
        if(!Auth::check()){
             return logInPlease();
        }else{


            $to = DB::table('url_to_partner')
            ->Join('users', 'url_to_partner.trafic_from_user', '=', 'users.id')
            ->where("trafic_to_user", Auth::user()->id)
            ->where("countgiven", ">","0")
            ->groupBy('users.id')
            ->select('url_to_partner.trafic_from_user', 'users.id', 'users.name', DB::raw('SUM(url_to_partner.countgiven) as points'))
            ->orderBy("points", "desc")
            ->get();
      
            if(empty($to)){
           
                $to = false;
               
            }
            return view("pages.analytics2")->with("to", $to);
        }
    }
     public function analytics3()
    {
        if(!Auth::check()){
             return logInPlease();
        }else{


            $to = DB::table('url_to_partner')
            ->Join('users', 'url_to_partner.trafic_to_user', '=', 'users.id')
            ->where("trafic_from_user", Auth::user()->id)
            ->where("countgiven", ">","0")
            ->groupBy('users.id')
            ->select('url_to_partner.trafic_from_user', 'users.id', 'users.name', DB::raw('SUM(url_to_partner.countgiven) as points'))
            ->orderBy("points", "desc")
            ->get();
      
            if(empty($to)){
                $to = false;
            }
            return view("pages.analytics3")->with("to", $to);
        }
    }

    public function myUrls(Request $request)
    {
        if(Auth::check()){
            // tye
            $url_database = Url::where(function($query){
                return $query
                ->where("user_id", Auth::user()->id)
                ->where("can_be_shared", 1);
                  })

                ->orderBy("created_at", "desc")
                ->simplePaginate(15);
            if($request->ajax()){
                $returnHTML = view('pages.ajaxpartials.myurls')->with('url_database', $url_database)->render();
                return response()->json(array('success' => true, 'html'=>$returnHTML));
            }else {         
                return view("urls.myUrls")->with("url_database", $url_database);
            }

            }
        if(!Auth::check()){
            return logInPlease();
        }
    }
    public function editFix(){
       return redirect()->route('myUrls');
    }
    public function urlEdit($url)
    {
        $link = Url::where("id", $url)->first();
        if(!Auth::check()){
            abort("404");
        }
        if($link->user_id != Auth::user()->id){
            abort("404");
        }

        return view("urls.edit")
        ->with("link", $link);
    }
    public function urlDelete($url)
    {
        $link = Url::where("id", $url)->first();
        if(!Auth::check()){
            abort("404");
        }
        if($link->user_id != Auth::user()->id){
            abort("404");
        }
        $link->can_be_shared = 0;
        $link->save();
        flash()->success(trans('app.linkwasdeleted'), '');
        return redirect()->route("myUrls");
    }

    public function postLinkEdit(Request $request)
    {
        $this->validate($request, [
            "description" => "max:550"
        ]);

        Url::where("id", $request->id)->update([
        "description" => $request->input("description"),
        ]);
        flash()->success(trans('app.Success'), trans('app.linkwasupdated'));
        return redirect()->route("myUrls");
    }

    public function share(Request $request)
    {
        if(Auth::check()){
            $myFreindsiD = Auth::user()->partnersOfMine->pluck('id');
            $userCanShare = url_to_partner::
                            join('users', 'users.id', '=', 'url_to_partner.trafic_to_user')
                            ->join('url', 'url.id', '=', 'url_to_partner.url_id')
                            ->select(['url_to_partner.*', 'users.name', 'users.ratio', 'users.email', 'url.description', 'url.can_be_shared'])
                            ->whereIn("trafic_to_user",  Auth::user()->partners()->pluck('id'))
                            ->where("trafic_from_user", Auth::user()->id)
                            ->where("url_to_partner.can_be_shared", 1)
                            ->where("url.can_be_shared", 1)
                ->orderBy("ratio", "desc")
                ->simplepaginate(20);

             if($request->ajax()){
                $returnHTML = view('urls.ajax.icanshare')->with('userCanShare', $userCanShare)->render();
                return response()->json(array('success' => true, 'html'=>$returnHTML));
            }else {         
                return view("urls.icanShare")->with("userCanShare", $userCanShare);
            }
        }
        if(!Auth::check()){
            return logInPlease();
        }
    }
    public function linkExplore()
    {
        if(!Auth::check()){
            return logInPlease();
        }
        $linkexplore = url::
                join('users', 'users.id', '=', 'url.user_id')
                ->select(['url.*', 'users.name', 'users.ratio', 'users.description as userdesc',])
                ->whereNotIn('users.id', [Auth::user()->id])
                ->where("url.can_be_shared", 1)
                ->where("users.field", Auth::user()->field)
                ->where("users.lang", Auth::user()->lang)
                ->orderBy("users.last_login", "desc")

                ->take(100)

                ->inRandomOrder()->take(10)->get();
     
    
        return view("urls.explore")->with("linkexplore", $linkexplore);

    }

    public function Urls()
    {
        return view('urls.urls');
    }
    public function adminNewest(){
        if(!Auth::check()){
            return logInPlease();
        }
        if(Auth::user()->id == 1){
        $newUser = user::where("verified", 1)
        ->orderBy("created_at", "desc")->take(20)->get();
        return view("test.newest")->with("newUser", $newUser);
    }
    }

    public function partners()
    {
        return view('partners.partners');
    }
    public function updates()
    {
        return view('pages.updates');
    }
   public function test()
    {
    }
    public function lang()
    {
        return view('pages.lang');
    }
    public function setlang()
    {
        Session::set('locale', Input::get('locale'));
        if(Auth::check()){
            Auth::user()->update([
            "linkedinUrl" => Input::get('locale'),
        ]);

        }
        return Redirect::back();

    }

    public function translan($lang)
    {
        Session::set('locale', $lang);

        return redirect()->route("home");

    }
   public function Stats()
    {
        $userscount = User::where("verified", 1)->count();
        $linksCreated = Url::count();
        $userstraffic = User::where("verified", 1)->sum('pointsGotten');

        $browser_total_raw = DB::raw('count(*) as total');
        $user_info = User::getQuery()

                     ->select('field', $browser_total_raw)
                     ->groupBy('field')
                        ->where("verified", 1)
                     ->pluck('total','field');
   
              //dd($user_info);     
        return view('pages.stats')
        ->with("userscount", $userscount)
        ->with("linksCreated", $linksCreated)
        ->with("userstraffic", $userstraffic)
        ->with("user_info", $user_info);
        
    }
    public function faq()
    {
        return view('pages.faq');
    }
    public function help()
    {
        return view('pages.help');
    }
       public function home2()
    {
        if(Auth::check()){
            $data = analytics::where("user_id",  Auth::user()->id)->orderBy("created_at", "desc")->take(7)->get()->reverse();      
            if(empty($to)){
           
                $to = false; 
            }
            $activity = Activity::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->take(6)->get();
            $langUser = "en";
            if (!empty(Auth::user()->linkedinUrl)) {
                $langUser = Auth::user()->linkedinUrl;
            }
            $chat = Chat::where("lang", $langUser)->latest()->take(4)->get();
              
            return view("pages.dashboard")
            ->with("activity", $activity)
            ->with("chat", $chat)
            ->with("data", $data);
        }

        return view('pages.home2');
    }
     public function deletePage()
    {
        return view('pages.delete');
    }
    public function deletereally($id)
    {
        if(\Auth::user()->id==$id) {
            $userthisDelete = User::find($id);
            $userthisDelete->verified = 0;
            $userthisDelete->emailCode = "DELETED";
            $userthisDelete->save();

            Auth::logout();

            flash()->info('Profile deleted', 'We hope to see you another day.');
            return redirect()->route("home");

        } else{
            abort("404");
        }
    }

}
