<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\helpers;
use App\Url;
use App\Activity;
use Auth;
use Carbon\Carbon;
class PartnerController extends Controller
{

	public function explore(request $request )
	{
	if(!Auth::check()){
		return logInPlease();
	}
	$field = $request->input("field");
	$lang = $request->input("lang");
	$medium = $request->input("medium");
	$given = $request->input("given");
	$online = $request->input("online");

	if($medium == null)
	{
		return view("partners.explore");
	} 
	if($given == null)
	{
		$given = 0;
	}
	if($online == null)
	{
		$online = 1000;
	}  
	// Se if field is any
	if($field == "any" && $lang != "any" && $medium != "any"){
		$userslisted = 
		User::where('lang', $lang)
		->whereNotNull ($medium)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}
	// Se if lang is any
	elseif($lang == "any" && $field != "any" && $medium != "any"){
		$userslisted = 
		User::where('field', $field)
		->whereNotNull ($medium)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}
	// Se if field and lang is any
	elseif($field == "any" && $lang == "any" && $medium != "any"){
			$userslisted = 
		User::whereNotNull ($medium)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}
		// Se if medium is any
	elseif($medium == "any" && $lang != "any" &&  $field != "any"){
		$userslisted = 
		User::where('field', $field)
		->where('lang', $lang)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}

	// Se if medium is any and lang is any
	elseif($medium == "any" && $lang == "any" &&  $field != "any"){
		$userslisted = 
		User::where('field', $field)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}

		// Se if medium is any and field is any
	elseif($medium == "any" && $lang != "any" &&  $field == "any"){
		$userslisted = 
		User::where('lang', $lang)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}

		// Se if everything is any
	elseif($medium == "any" && $lang == "any" &&  $field == "any"){
		$userslisted = 
		User::where("verified", 1)
		->where('pointsGiven', '>=', $given)
		->where('last_login', '>=', Carbon::now()->subDays($online)->toDateTimeString())
		->orderBy("ratio", "desc")->simplePaginate(21);

	}
	
	else{

		$userslisted = 
		User::where('field', $field)
		->where('lang', $lang)
		->where('pointsGiven', '>=', $given)
		->whereNotNull ($medium)
	    ->orderBy("ratio", "desc")->simplePaginate(21);
	}

	return view("partners.explore")->with("userslisted", $userslisted);
	}

	public function getResults(request $request )
	{
	$query = $request->input("query");
	$userslisted = User::where('name',  'LIKE',  "%$query%")
		->where('verified', 1)
        ->orderBy("ratio", "desc")->simplePaginate(21);
		return view("partners.find")->with("userslisted", $userslisted);
	}
	public function getPartners()
	{
		if(!Auth::check()){
			return logInPlease();
		}
		$partners = Auth::user()->partners();
		return view("partners.myPartnerslist")
		->with("partners", $partners);

	}

	public function myPartners()
	{	
		if(Auth::check()){
		$partners = Auth::user()->partners();
		$requests = Auth::user()->partnerRequest();
		$urls = Url::where("user_id", Auth::User()->id)
		->where("can_be_shared", 1)
		->count();
		return view("partners.myPartners")
		->with("partners", $partners)
		->with("requests", $requests)
		->with("urls", $urls);
		} else{
			return logInPlease();
		}
		
	}

	public function getAdd($name)
	{
		$user = User::where("name", $name)->first();
		if (!$user)
		{
			flash()->error(trans('app.Error'), trans('app.nouserfound'));
			return redirect()->route("search");
		}
		if (Auth::user()->hasPartnerRequestspending($user) || $user->hasPartnerRequestspending(Auth::user()))
		{
			flash()->info('', trans('app.alreadypending'));
			return redirect()
			->route("partner.profile", ["name" => $name]);
		}
		if (Auth::user()->id === $user->id)
		{
			return redirect()->route("home");
		}
		if (Auth::user()->isPartnerWith($user)) 
		{
			flash()->info('Info',  trans('app.aldreadypartners'));
			return redirect()
			->route("partner.profile", ["name" => $name]);
		}

		if(Auth::user()->partnersOfMine->count() > 25){
			flash()->info('Too many partners', 'You have over 25 partners. Clean up before adding more');
			return redirect()
			->route("partner.profile", ["name" => $name]);
		} else if($user->partners()->count() > 30){
			flash()->info('To many partners', 'This user has too many partners');
			return redirect()
			->route("partner.profile", ["name" => $name]);
		} else{
		//- - - - - - - - - - - - - - - - - notify
		$notification = new Activity;
		$notification->user_id = $user->id;
		//increment notifyication
		$userIncrementNotifcation = User::where("id", $user->id)->first();
		$userIncrementNotifcation->increment('notifications');
		$notification->from_id = Auth::User()->id;
		$notification->notifyThis = true;
		$notification->type = 'partnerRequestSend';
		$notification->route = Auth::User()->name;
		$notification->save();
		// notify end

		Auth::user()->addPartner($user);

		$notification = new Activity;
		$notification->user_id = Auth::user()->id;
		//increment notifyication
		$userIncrementNotifcation = User::where("id", Auth::user()->id)->first();
		$userIncrementNotifcation->increment('notifications');
		$notification->from_id = $user->id;
		$notification->notifyThis = false;
		$notification->type = 'partnerYouadded';
		$notification->data =  $user->name;
		$notification->route = Auth::User()->name;
		$notification->save();

		flash()->success(trans('app.Success'), trans('app.teamupinvitesent'));

		return redirect()->route("partner.profile", ["name" => $name]);
		}
	}

	public function getAccept($name)
	{
		$user = User::where("name", $name)->first();
		if (!$user)
		{
			flash()->error('Error', 'No user found.');
			return redirect()->route("search");
		}

		if (!Auth::user()->hasPartnerRequestReceived($user)) 
		{
			return redirect()->route("home");
		}
		//- - - - - - - - - - - - - - - - - notify
		$notification = new Activity;
		$notification->user_id = $user->id;
		//increment notifyication
		$userIncrementNotifcation = User::where("id", $user->id)->first();
		$userIncrementNotifcation->increment('notifications');
		$notification->from_id = Auth::User()->id;
		$notification->notifyThis = true;
		$notification->type = 'partnerRequestAccepted';
		$notification->route = Auth::User()->name;
		$notification->save();
		// notify end

		//Accept freind, and add urls
		Auth::user()->acceptPartnerRequest($user);

		flash()->Success('Team up!', trans('app.nowyouarepartners'));
		return redirect()->route("myPartners");
	}

	public function getDecline($name)
	{
		$user = User::where("name", $name)->first();
		if(!Auth::user()->hasPartnerRequestspending($user)){
			Auth::user()->declinePartnerRequest($user);
		}
		// set all canshare = 0, in url to partner where id match the removed partner to 

		// return view with succes 
		flash()->info(trans('app.devlined'), '');
		return redirect()->route("myPartners");
	}

	public function postDelete($name)
	{
		$user = User::where("name", $name)->first();
		if (!Auth::user()->isPartnerWith($user)) 
		{
			return redirect()->route("home");
		}
		Auth::user()->deletePartner($user);
		flash()->Success(trans('app.partnerdeleted'), '');
		return redirect()->route("myPartners");
	}
}