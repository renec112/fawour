<?php

namespace App\Http\Controllers;
use DB;
use App\Url;
use App\User;
use App\ip;
use App\url_to_partner;
use App\Activity;
use App\Http\Requests;
use App\Http\Requests\FlyerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class redirectController extends Controller
{
    public function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
        }

    public function redirect($code)
    {
        $url_database = (url_to_partner::where(DB::raw('BINARY `code`'), $code)->first());

    	if(isset($url_database))
    	{   
            $ShouldNotify = false;
            //notify if uupdated at is older
            if($url_database->updated_at->diffInDays( \Carbon\Carbon::now() ) > 7){
                $ShouldNotify = true;
            } 
            $userIp = $this->get_client_ip();
            $shouldCount = true;
            if($url_database->ip1 == $userIp or $url_database->ip2 == $userIp or $url_database->ip3 == $userIp or $url_database->ip4 == $userIp or $url_database->ip5 == $userIp)
            {
                $shouldCount = false;
            }
            else if($url_database->ip1 == null){
                $shouldCount = true;
            }

            // store ip and check latest
            $checkkingIp = true;
            
            if($url_database->lastIp == null and $checkkingIp == true){
                    $url_database->update(['ip1' => ($userIp)]);
                    $url_database->update(['lastIp' => (1)]);
                    $checkkingIp = false;
                    
                    $ShouldNotify = true;
            }
            if($url_database->lastIp == 5 and $checkkingIp == true){
                    $url_database->update(['ip1' => ($userIp)]);
                    $url_database->update(['lastIp' => (1)]);
                    $checkkingIp = false;
            }
            if($url_database->lastIp == 4 and $checkkingIp == true){
                    $url_database->update(['ip5' => ($userIp)]);
                    $url_database->update(['lastIp' => (5)]);
                    $checkkingIp = false;
            }
            if($url_database->lastIp == 3 and $checkkingIp == true){
                    $url_database->update(['ip4' => ($userIp)]);
                    $url_database->update(['lastIp' => (4)]);
                    $checkkingIp = false;
            }
                  if($url_database->lastIp == 2 and $checkkingIp == true){
                    $url_database->update(['ip3' => ($userIp)]);
                    $url_database->update(['lastIp' => (3)]);
                    $checkkingIp = false;
            }
            if($url_database->lastIp == 1 and $checkkingIp == true){
                    $url_database->update(['ip2' => ($userIp)]);
                    $url_database->update(['lastIp' => (2)]);
                    $checkkingIp = false;
            }  
            //detect bot
    
            if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) {
                $shouldCount = false;
            }
            if (
            strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||          
            strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false) {
                $shouldCount = false;
            }
            //increment stuff

            if($shouldCount == true){
                   
                		$url_database->increment('countgiven');
                        //Increment and update rato  the guy who gave
                        $databaseToGive = (User::where("id",$url_database->trafic_from_user));
                        $databaseToGive->increment('pointsGiven');
                        
                        $GavePointsGiven = $databaseToGive->pluck("pointsGiven")->first();
                        $GavePointsGotten = $databaseToGive->pluck("pointsGotten")->first();
                
                        if($GavePointsGiven != 0 and $GavePointsGotten != 0){
                              $databaseToGive->update(['ratio' => ($GavePointsGiven /  $GavePointsGotten)]);
                        }
                        // Increment and update ratio the gu who got
                        $databaseToGet = (User::where("id",$url_database->trafic_to_user));
                        $databaseToGet->increment('pointsGotten');
                       
                        $GotPointsGiven = $databaseToGet->pluck("pointsGiven")->first();
                        $GotPointsGotten = $databaseToGet->pluck("pointsGotten")->first();
                        if($GotPointsGiven != 0 and $GotPointsGotten != 0){
                            $databaseToGet->update(['ratio' => ($GotPointsGiven / $GotPointsGotten)]);
                        }
                        // get url database and increment it
                        $url_not_partner_db = Url::where("id", $url_database->url_id)->first();
                        $url_not_partner_db->increment('count');
                                 // notifcation
                        if( $ShouldNotify == true){
                            $notification = new Activity;
                            $notification->user_id = $databaseToGet->pluck("id")->first();
                            //increment notifyication
                            $userIncrementNotifcation = User::where("id", $databaseToGet->pluck("id")->first())->first();
                            $userIncrementNotifcation->increment('notifications');
                            $notification->from_id = $databaseToGive->pluck("id")->first();
                            $notification->type = 'trafficGotten';
                            $notification->notifyThis = true;
                            $notification->route = User::where("id", $databaseToGive->pluck("id")->first())->pluck("name")->first();
                            $notification->data = $url_not_partner_db->id;
                            $notification->save();
                            // notify end
                        }
            }


            //redirect
    		header("location: $url_database->url");
    	}
    	else {

    	flash()->error("Error", "Url doens't exist");
        return view("pages.home");

}
    	//$url = $url_database->url()->get();
    	//echo $url;
     	

    }
}
