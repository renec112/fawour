<?php 
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Socialite;
use App\Http\Requests;
use Auth;
class RegistrationController extends Controller 
{
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            return view('auth.login');
        }

        $user = User::where("emailCode", $confirmation_code)->first();

        if ( ! $user)
        {
            flash()->error( trans('app.useralreadreq'), '');
            return view('auth.login');

        }

        $user->verified = 1;
        $user->emailCode = null;
        $user->save();

        flash()->success(trans('app.userisCon'), trans('app.youanlogin'));
        return view('auth.login');
    }
    public function reset(){
        return view('auth.resetcode');
    }
    public function postReset(Request $request)
    {
        $this->validate($request, [
            "email" => "required|email|max:255"
        ]);

        $user = User::where("email", $request->email)
            ->where("verified", 0)
            ->first();
        if ( ! $user)
            {
                flash()->error('User with unverified email not found', '');
                return view('auth.resetcode');

            }
    
        $data = array( 'email' => $user->email, 'name' =>$user->name, 'code' => $user->emailCode );
        Mail::send('email.verify', $data, function($message) use ($data){
            $message->to($data['email'], $data['name'] )
            ->subject('Verify your email address');
        });
        flash(trans('app.newmeail'));
        return redirect('/login');

    }
    public function redirectToProvider($provider)
    {
        if($provider == "facebook")
        {
            return Socialite::driver($provider)->with([$provider => 'client_id', $provider => 'client_secret'])->redirect();
        }
        elseif($provider == "twitter"){
            return Socialite::with('twitter')->redirect();
        }
        elseif($provider == "google"){
            return Socialite::with('google')->redirect();
        }
    }
    public function handleProviderCallback($provider)
    {
        #Get user
        $user = Socialite::with($provider)->user();
        if($user->email != null){
            $emailToCheck = strtolower($user->email);
             if ($emailToCheck != null and User::where('email', '=', $emailToCheck)->exists()) {
           // user found login
                $userToLogin = User::where('email', '=', $emailToCheck)->first();
                Auth::loginUsingId($userToLogin->id);
                return redirect()->route('home');
            } else
            {
                $name = $user->name . str_random(5);
                if(User::where('name', '=', $user->name)->exists()){
                    flash()->error('Name already taken', '');
                    return redirect()->route('home');
                }
                $pwd = bin2hex(openssl_random_pseudo_bytes(6));
                $name = str_replace(" ","",$name);
                $lang = \Lang::getLocale();
                #create user
                 user::create([
                    'name' => $name,
                    'email' => $user->email,
                    'password' => $pwd,
                    'emailCode' => $provider,
                    'linkedinUrl' => $lang,
                    ]);
                return redirect()->route('home');
            }
        }else{
            flash()->error('Could not find email. Continue to the site with another service or with email.', '');
        }
    }
    protected static function boot()
   {
       parent::boot();

       self::saving(function ($model) {
           $model->email = strtolower($model->email);
       });
   }
}
