<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Url;
use App\Activity;
use Auth;
use App\url_to_partner;
use Image;
use File;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
	public function notifications(){
		if(!Auth::check()){
            return logInPlease();
        }
        
		//reset notifications
		Auth::user()->update([
			"notifications" => 0,
		]);
		// get all activites
		$activity =  Activity::where("user_id", Auth::user()->id)->orderBy('created_at', 'desc')->simplePaginate(20);
		// set all activities to seen
		Activity::where("user_id", Auth::user()->id)
		->where("notifyThis", 1)
		->update([
			"notifyThis" => 0,
		]);

		return view("pages.activity")->with("activity", $activity);

		
	}
	public function emailSet($email, $name)
	{
		$user1 = User::where("email", $email)
		->where("name", $name)
		->firstorfail();

		$user1->update([
		"EmailEach2dayNoti" => 0
		]);
		flash()->success("No more emails from us", "");
		return view("pages.home2");

	}
	public function getProfile($name)
	{
		$user1 = User::where("name", $name)->first();
		if (!$user1){
			abort(404);
		}
		if(!Auth::check()){
			return view("partners.profile")->with("user1", $user1);
		}

		if (Auth::user()->getName() == $user1->getName())
		{
			return redirect()->route("myPartners");
		}

		$isfreinds = $this->user->isPartnerWith($user1);
		if($isfreinds == true){
			$dataYougot = url_to_partner::where("trafic_from_user", $user1->id)
			->where("trafic_to_user", Auth::user()->id)
			->sum("countgiven");
			$dataYougive = url_to_partner::where("trafic_to_user", $user1->id)
			->where("trafic_from_user", Auth::user()->id)
			->sum("countgiven");
			
		} else {
			$dataYougot = 0;
			$dataYougive = 0;
		}
		$partnerLinks = Url::where("user_id", $user1->id)->where("can_be_shared", 1)->count();
		return view("partners.profile")
		->with("user1", $user1)
		->with("dataYougive", $dataYougive)
		->with("dataYougot", $dataYougot)
		->with("partnerLinks", $partnerLinks);
	}
		public function getLinks($name)
		{
		$user1 = User::where("name", $name)->first();
		if (!$user1){
			abort(404);		
		}
		if(!Auth::check()){
			 return logInPlease();
		}
		$isfreinds = $this->user->isPartnerWith($user1);
		if($isfreinds == true){
		 	$userCanShare = url_to_partner::
	                            join('users', 'users.id', '=', 'url_to_partner.trafic_to_user')
	                            ->join('url', 'url.id', '=', 'url_to_partner.url_id')
	                            ->select(['url_to_partner.*', 'users.name', 'users.email', 'url.description', 'url.can_be_shared'])
	                            ->where("trafic_to_user",  $user1->id)
	                            ->where("trafic_from_user", Auth::user()->id)
	                            ->where("url_to_partner.can_be_shared", 1)
	                            ->where("url.can_be_shared", 1)
	                ->orderBy("created_at", "desc")
	                ->simplePaginate(15);
			return view("urls.partnerUrls")->with("userCanShare", $userCanShare)->with("partner", $user1);
		} else{
		 	$userCanShare = url::
            where("user_id", $user1->id)
            ->where("can_be_shared", 1)
            ->orderBy("created_at", "desc")
            ->simplePaginate(15);
			return view("urls.notpartnerUrls")->with("url_database", $userCanShare)->with("partner", $user1);

		}
	}


	public function getEdit()
	{
		if(!Auth::check()){
            return logInPlease();
        }
		return view("user.edit");
	}
	public function postEdit(Request $request)
	{
		#Check if user wants to change name
		$updateName = false;
		if((Auth::user()->name) != $request->input("name")){
			$updateName = true;
		}
		if ($updateName == true){
			$validator = $this->validate($request, [
				"description" => "max:550",
				"name" =>  "required|max:45|alpha_dash|unique:users",
			]);
		} else{
			$validator = $this->validate($request, [
				"description" => "max:550",
			]);
		}
		$youtubeurl = $request->input("YoutubeUrl");
		if($youtubeurl === ''){
			$youtubeurl = null;
		}
		$blogurl = $request->input("BlogUrl");
		if($blogurl === ''){
			$blogurl = null;
		}
		$facebookurl = $request->input("facebookUrl");
		if($facebookurl === ''){
			$facebookurl = null;
		}
		$instagramUrl = $request->input("instagramUrl");
		if($instagramUrl === ''){
			$instagramUrl = null;
		}
		$twitterurl = $request->input("twitterUrl");
		if($twitterurl === ''){
			$twitterurl = null;
		}
		$vineurl = $request->input("vineUrl");
		if($vineurl === ''){
			$vineurl = null;
		}
		$pinteresturl = $request->input("pinterestUrl");
		if($pinteresturl === ''){
			$pinteresturl = null;
		}
		$tumblerurl = $request->input("tumblrUrl");
		if($tumblerurl === ''){
			$tumblerurl = null;
		}

		if ($updateName == true){
			Auth::user()->update([
			"name" => $request->input("name"),
			"description" => $request->input("description"),
			"BlogUrl" => $blogurl,
			"YoutubeUrl" => $youtubeurl,
			"facebookUrl" => $facebookurl,
			"instagramUrl" => $instagramUrl,
			"twitterUrl" => $twitterurl,
			"vineUrl" => $vineurl,
			"pinterestUrl" => $pinteresturl,
			"tumblrUrl" => $tumblerurl,
			"field" => $request->input("field"),
			"lang" => $request->input("lang"),
			]);
		} else{
		Auth::user()->update([
			"description" => $request->input("description"),
			"BlogUrl" => $blogurl,
			"YoutubeUrl" => $youtubeurl,
			"facebookUrl" => $facebookurl,
			"instagramUrl" => $instagramUrl,
			"twitterUrl" => $twitterurl,
			"vineUrl" => $vineurl,
			"pinterestUrl" => $pinteresturl,
			"tumblrUrl" => $tumblerurl,
			"field" => $request->input("field"),
			"lang" => $request->input("lang"),
			]);
		}

        #update log
        $notification = new Activity;
        $notification->from_id =0;
        $notification->user_id = Auth::user()->id;
        $notification->type = 'profileUpdated';
         $notification->data = "null";
        $notification->route = "null";
        $notification->save();
        flash()->success(trans('app.Success'), trans('app.userupdated'));
		return redirect()->route("user.edit");
	}
	public function getAvatar()
	{
		$user = User::where("id", Auth::user()->id)->first();
		if (!$user){
			abort(404);
		}
		return view("user.avatar");
	}
	public function postAvatar(Request $request)
	{
		if($request->hasFile("avatar")){
			$file = $request->file("avatar");
		    // Tell the validator that this file should be an image
	       $fileArray = array('image' => $file);
		    $rules = array(
		      'image' => 'mimes:jpeg,jpg,png|required|max:1000' // max 10000kb
		    );

		    // Now pass the input and rules into the validator
		    $validator = Validator::make($fileArray, $rules);

		    // Check to see if validation fails or passes
		       if ($validator->fails()) {
		       
            $this->throwValidationException(
                $request, $validator
            );
        }
		    if ($validator->fails())
		    {
		          // Redirect or return json to frontend with a helpful message to inform the user 
		          // that the provided file was not an adequate type
		    	flash()->Error('Error',  response()->json(['error' => $validator->errors()->getMessages()], 400));
					return view("user.avatar");
		          //return response()->json(['error' => $validator->errors()->getMessages()], 400);
		    } else
		    {
		    	  // Delete current image before uploading new image
            if (Auth::user()->Avatarpath != null) {
            	$path = '/uploads/avatars/';
            	$filename22 = Auth::user()->Avatarpath;
   
            	File::Delete(public_path( $path . "20" . $filename22) );
            	File::Delete(public_path( $path . "80" . $filename22) );
            	File::Delete(public_path( $path . "35" . $filename22) );
            	File::Delete(public_path( $path . "120" . $filename22) );
            	File::Delete(public_path( $path . $filename22) );

            }

				    $avatar = $request->file("avatar");
					$filename = Auth::user()->name . time() . "." . $avatar->getClientOriginalExtension();
					Image::make($avatar)->fit(300,300)
					->save(public_path("/uploads/avatars/" . $filename ));

					$filename = Auth::user()->name . time() . "." . $avatar->getClientOriginalExtension();
					Image::make($avatar)->fit(20,20)
					->save(public_path("/uploads/avatars/20" . $filename ));

					$filename = Auth::user()->name . time() . "." . $avatar->getClientOriginalExtension();
					Image::make($avatar)->fit(80,80)
					->save(public_path("/uploads/avatars/80" . $filename ));

					$filename = Auth::user()->name . time() . "." . $avatar->getClientOriginalExtension();
					Image::make($avatar)->fit(120,120)
					->save(public_path("/uploads/avatars/120" . $filename ));

					$filename = Auth::user()->name . time() . "." . $avatar->getClientOriginalExtension();
					Image::make($avatar)->fit(35,35)
					->save(public_path("/uploads/avatars/35" . $filename ));

					$user = Auth::user();
					$user->Avatarpath = $filename;
					$user->save();

					flash()->success(trans('app.Success'), trans('app.imageupdated'));
					return view("user.avatar");
				}
		    };

		
		
	}

    public function settings()
    {	
		if(!Auth::check()){
			 return logInPlease();
		}
    	$user = User::Where("id", Auth::user()->id)->first();
    	if (!$user){
			 return logInPlease();
		}

        return view('user.settings');
    }
   public function postSettings(Request $request)
    {
		Auth::user()->update([
		"EmailEach2dayNoti" => $request->input("EmailEach2dayNoti"),
		"linkedinUrl" => $request->input("locale"), 
		]);
		\App::setLocale(Auth::user()->linkedinUrl);

    	flash()->success('Success!', 'settings updated!');
        return view('user.settings');
    }
    public function deleteUser(Request $request){
    	$user = User::where("id", Auth::user()->id)->first();
    	if (!$user){
			abort(404);
		}
		


    }
}
