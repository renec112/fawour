<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    /*use AuthenticatesAndRegistersUsers, ThrottlesLogins {
        AuthenticatesUsers::login as defaultLogin;
    }*/
    use AuthenticatesAndRegistersUsers, ThrottlesLogins {
            AuthenticatesAndRegistersUsers::login as defaultLogin;
    }
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $user = $this->create($request->all());
        //gets users information to send
        $userEmailData = User::where("email", $request->email)->first();
        //splits the information in an array that mail::send can use
        $data = array( 'email' => $userEmailData->email, 'name' =>$userEmailData->name, 'code' => $userEmailData->emailCode );
        //send email
        Mail::send('email.verify', $data, function($message) use ($data){
            $message->to($data['email'], $data['name'] )
            ->subject('Welcome to Fawour');
        });
        flash()->overlay(trans('app.thankyou'), trans('app.confrimemail'), 'info');

        return redirect('/login');
    }
    public function login(Request $request)
    {
        $checkThisUser = User::where("email", strtolower($request->email))->first();
        if ( ! $checkThisUser)
        {
            flash()->error('User not found', '');
            return view('auth.login');
        }
        if($checkThisUser->emailCode == "DELETED"){
            flash('Your profile was deleted. Contact us for help.');
            return redirect('/login');
        }
        if($checkThisUser->emailCode == "BANNED"){
            flash('Your are banned from this site.');
            return redirect('/login');
        }
        elseif($checkThisUser->verified == 0){
            flash(trans('app.pleaseocnfrim'));
            return redirect('/login');
        }



        $this->defaultLogin($request);
        return redirect('/login');
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        parent::__construct();
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:45|alpha_dash',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $lang = \Lang::getLocale();
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'emailCode' => str_random(30),
            'password' => bcrypt($data['password']),
            'linkedinUrl' => $lang,
        ]);

        
    }


}
