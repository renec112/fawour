<?php 
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Chat;
use Auth;
class ChatController extends Controller 
{
    public function chatSite(Request $request, Chat $chat)
    {
        if(!Auth::check()){
            return logInPlease();
        }
        if($request->input("forum") == 0){
            return view("pages.chat");
        } else{
            return view("pages.chat");
        }
    }
    public function chatGet(Request $request, Chat $chat){
        $langUser = "en";
        if (!empty(Auth::user()->linkedinUrl)) {
            $langUser = Auth::user()->linkedinUrl;
        }

        $allPosts = $chat->where("forum", $request->input("forum"))->with("user");
        $chatPost = $allPosts->where("lang", $langUser)->orderBy("created_at","desc")
        ->take($request->get("limit", 20))
        ->get();
        return response()->json([
            'posts' => $chatPost,
            'total' => $chatPost->count(),
        ]);
    }

    public function create(Request $request, Chat $chat)
    {
    	//validate submission data
    	$this->validate($request, [
    		'body' => 'required|max:200',
    		'forum' => 'required',
    	]);
         if (!strstr($request->body, "www") && !strstr($request->body, ".com") && !strstr($request->body, "http")) {
            # Check if user lang is set, else think its english
            $langUser = "en";
            if (!empty(Auth::user()->linkedinUrl)) {
                $langUser = Auth::user()->linkedinUrl;
            }
            #create post
            $Createdchat = $request->User()->chat()->create([
                'body' => $request->body,
                'forum' => $request->forum,
                'lang' => $langUser,
             ]);
                return response()->json($chat->with("user")->find($Createdchat->id));
            } else {
                 return response()->error();
            }          
    	

    }

}
