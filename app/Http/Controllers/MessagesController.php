<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Activity;
use App\messages;
Use Carbon\Carbon;

class MessagesController extends Controller
{
    public function postMessages(request $request){
            //- - - - - - - - - - - - - - - - - notify
            $usertoSend = User::where("id", $request->input("formFrom"))->first();
            $sendMessage = false;
            if($this->user->isPartnerWith($usertoSend)){
                $sendMessage = true;
            } else {
                $latestmessage = messages::where("user_id", Auth::User()->id)->latest()->limit(1)->first();
                 if(isset($latestmessage)){
                    $latestmessage2 = strtotime($latestmessage->created_at);
                    $dateTwelveHoursAgo = strtotime("-60 minute");
                    if ($latestmessage2 > $dateTwelveHoursAgo) {
                         flash()->error(trans('app.waitabit'), trans('app.onhourwaitsorry'));
                    }
                    else {
                          
                          $sendMessage = true;
                    }
                }else{
                    $sendMessage = true;    
                }
                

            }

            if($sendMessage == true){
                $notification = new Activity;
                $notification->user_id = $usertoSend;
                //increment notifyication
                $userIncrementNotifcation = User::where("id", $usertoSend->id)->first();
                $userIncrementNotifcation->increment('notifications');
                $notification->from_id = Auth::User()->id;
                $notification->user_id = $usertoSend->id;
                $notification->notifyThis = true;
                $notification->type = 'MessageSent';
                $notification->route = Auth::User()->name;
                $notification->save();
                // notify end

        	$this->validate($request, [
        		"messages" => "required |max:1000",
    		]);
            Auth::user()->messages()->create([
            	"body" => $request->input("messages"),
                "to_user" => $request->input("formFrom"),
            ]);
            
            
            flash()->success(trans('app.Success'), trans('app.messagewassent'));
            $redirecturl = "/messages?from=" .  $usertoSend->id;
            return redirect($redirecturl);
        } else {
            $redirecturl = "/messages?from=" .  $usertoSend->id;
            return redirect($redirecturl)->withInput();
        }

    }
}
