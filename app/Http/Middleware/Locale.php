<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Locale
{
    /**
     * Handle an incoming request.edit
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(Auth::check() && isset(Auth::User()->linkedinUrl)){
            Session::set('locale', Auth::User()->linkedinUrl);
            App::setLocale(Session::get('locale'));
        }
        else  if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }
       
        else { // This is optional as Laravel will automatically set the fallback language if there is none specified
            App::setLocale(Config::get('app.fallback_locale'));
        }
        return $next($request);
    }
}
