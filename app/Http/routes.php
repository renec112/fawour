<?php
Route::get('/l/{code}', ['as' => 'redirect', 'uses' => 'redirectController@redirect']);

Route::group(['middleware' => ['web']], function () {

    Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home2',] );
    Route::auth();
    Route::get('register/resetemail', ['as' => 'reset','uses' => 'RegistrationController@reset' ]);
    Route::post('register/resetemail', [ 'uses' => 'RegistrationController@postReset']);
    Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegistrationController@confirm'
    ]);
    Route::get('auth/{provider}', 'RegistrationController@redirectToProvider');
    Route::get('auth/callback/{provider}', 'RegistrationController@handleProviderCallback');
});
Route::group(['middleware' => ['web']], function () {
    Route::resource('urlcreate', 'FlyersController');
    
    Route::get('profile/edit', ['as' => 'user.edit', 'uses' => 'ProfileController@getEdit']);
    Route::get('profile/updateEmailSettings/{email}/{name}', ['as' => 'user.emailset', 'uses' => 'ProfileController@emailSet']);
    Route::post('profile/edit', ['uses' => 'ProfileController@postEdit']);
    Route::get('profile/avatar', ['as' => 'user.avatar', 'uses' => 'ProfileController@getAvatar']);
    Route::post('profile/avatar', ['as' => 'user.avatar', 'uses' => 'ProfileController@postAvatar']);
    Route::get('profile/notifications', ['as' => 'notifications', 'uses' => 'ProfileController@notifications']);

    Route::get('messages', ['as' => 'messages', 'uses' => 'PagesController@messages']);
    Route::post('messages/post', ['as' => 'messages.post', 'uses' => 'MessagesController@postMessages']);

    Route::get('lang', ['as' => 'lang', 'uses' => 'PagesController@lang']);
    Route::post('langset',  ['before' => 'csrf', 'as' => 'langset', 'uses' => 'PagesController@setlang']);
    Route::get('la/{lang}',  ['as' => 'la', 'uses' => 'PagesController@translan']);

    Route::get('analytics/visitors', ['as' => 'analytics', 'uses' => 'PagesController@analytics']);
    Route::get('analytics/gottenby', ['as' => 'analytics2', 'uses' => 'PagesController@analytics2']);
    Route::get('analytics/givento', ['as' => 'analytics2', 'uses' => 'PagesController@analytics3']);
    Route::get('analytics', ['as' => 'analytics', 'uses' => 'PagesController@analyticblank']);

    

    Route::delete('/profile/delete/{id}', 'PagesController@deletereally');
    Route::get('profile', ['as' => 'myPartners', 'uses' => 'PartnerController@myPartners']);
    Route::get('partners/explore', ['as' => 'partners.explore', 'uses' => 'PartnerController@explore']);
    Route::get('partner/{name}', ['as' => 'partner.profile', 'uses' => 'ProfileController@getProfile']);
    Route::get('partner/{name}/links', ['as' => 'partner.link', 'uses' => 'ProfileController@getLinks']);
    Route::get('partners/search', ['as' => 'search', 'uses' => 'PartnerController@getResults']);
    Route::get('partners/myPartners', ['as' => 'search', 'uses' => 'PartnerController@getPartners']);
    Route::get('partners', ['as' => 'partners', 'uses' => 'PagesController@partners']);
    Route::get('partner', ['as' => 'partner', 'uses' => 'PagesController@partners']);
    
    Route::get('partner/add/{name}', ['as' => 'partner.add', 'uses' => 'PartnerController@getAdd']);
    Route::get('partner/accept/{name}', ['as' => 'partner.accept', 'uses' => 'PartnerController@getAccept']);
    Route::get('partner/decline/{name}', ['as' => 'partner.decline', 'uses' => 'PartnerController@getDecline']);
    Route::post('partner/delete/{name}', ['as' => 'partner.delete', 'uses' => 'PartnerController@postDelete']);


    Route::get('links/', ['as' => 'urls', 'uses' => 'PagesController@urls']);
    Route::get('links/create', ['as' => 'create_url', 'uses' => 'FlyersController@create']);
    Route::get('links/share', ['as' => 'share_url', 'uses' => 'PagesController@share']);

    Route::get('links/explore', ['as' => 'linkExplore', 'uses' => 'PagesController@linkExplore']);
    
    Route::get('links/edit/{id}', ['as' => 'edit_url', 'uses' => 'PagesController@urlEdit']);
    Route::get('links/delete/{id}', ['as' => 'delete_url', 'uses' => 'PagesController@urlDelete']);
    Route::post('links/edit/{id}', [ 'uses' => 'PagesController@postLinkEdit']);

    Route::get('links/myLinks', ['as' => 'myUrls', 'uses' => 'PagesController@myUrls']);
    Route::get('links/edit', ['as' => 'editLinkFail', 'uses' => 'PagesController@editFix']);
    Route::post('/links/myLinks/{id}', ['as' => 'postFull', 'uses' => 'FlyersController@postFull']);
    Route::get('/links/myLinks/{id}', ['as' => 'showfull', 'uses' => 'FlyersController@showfull']);


    Route::get('updates', ['as' => 'updates', 'uses' => 'PagesController@updates']);
    Route::get('/admin/newest', ['as' => 'updates', 'uses' => 'PagesController@adminNewest']);
    
    Route::post("/chat", "ChatController@create");
    Route::get('/chat', ['as' => 'chat', 'uses' => 'ChatController@chatSite']);
    Route::get('/chatGet', ['as' => 'chatGet', 'uses' => 'ChatController@chatGet']);
    Route::get('/profile/settings', ['as' => 'settings', 'uses' => 'ProfileController@settings']);
    Route::post('/profile/settings', ['uses' => 'ProfileController@postSettings']);
    Route::get('/home', ['as' => 'home2', 'uses' => 'PagesController@home',] );
    Route::get('/test', ['as' => 'test', 'uses' => 'PagesController@test',] );
    Route::get('/stats', ['as' => 'stats', 'uses' => 'PagesController@stats',] );
    Route::get('/faq', ['as' => 'faq', 'uses' => 'PagesController@faq',] );
    Route::get('/help', ['as' => 'help', 'uses' => 'PagesController@help',] );
    Route::get('/more', ['as' => 'home2', 'uses' => 'PagesController@home',] );
    Route::get('/example', ['as' => 'home2', 'uses' => 'PagesController@example',] );
    Route::get('/about', ['as' => 'about', 'uses' => 'PagesController@about',] );
    Route::get('profile/settings/delete', ['as' => 'home2', 'uses' => 'PagesController@deletePage',] );
 });
