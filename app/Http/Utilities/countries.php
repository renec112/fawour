<?php

namespace App\Http\Utilities;

class Country
{

    protected static $field = [
        "Health & fitness"                           => "ha",
        "Gaming"                                       => "gam",
        "Beauty and fashion"                           => "bf",
        "Music"                                        => "mu",
        "Tech"                                         => "te",
        "Design & photography"                       => "de",
        "Comedy & reality"                           => "cr",
        "Lifestyle"                                    => "li",
        "Food"                                         => "fo",
        "Traveling"                                    => "tr",
        "Other"                                        => "ot",

    ];

    protected static $lang = [
        "English"                                       => "en",
        "German"                                        => "ge",
        "French"                                        => "fr",
        "Danish"                                        => "da",
        "Chinese"                                       => "ch",
        "Japanese"                                      => "ja",
        "Spanish"                                       => "sp",
        "Dutch"                                         => "du",
        "Italian"                                       => "it",
        "Portuguese"                                    => "po",

    ];
    //ev is for everything in chat
    public static function allfield()
    {
        return static::$field;
    }

        public static function alllang()
    {
        return static::$lang;
    }

    public static function getLanguageName($languageCode){
          if ($languageCode == "Not set")
        {
            return null;
        } else {
        $flippedLang = array_flip(static::$lang);
        return $flippedLang[$languageCode];
    }
    }
    public static function getFieldName($fieldCode){
        if ($fieldCode == "Not set")
        {
            return "Not set";
        } else {
        $flippedField = array_flip(static::$field);
        return $flippedField[$fieldCode];
    }
    }
}

?>