<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;

class Messages extends Model
{
    protected $table = "messages";

    protected $fillable = [
    "body",
    "to_user",
    "user_id",
    ]; 
    public function user()
    {
        return $this->belongsTo("App\user", "user_id");
    }
}