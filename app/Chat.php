<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable =["body", "forum", "lang"];
	protected $table = 'chat';
	protected $appends =['humanCreatedAt'];


    public function user(){
        return $this->belongsTo('App\User');
    }
    public function getHumanCreatedAtAttribute()
    {
    	return $this->created_at->diffForHumans();
    }
}
