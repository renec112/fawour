<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\User;

class SendEmailNoti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:SendEmailNoti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email if notification is older than 2 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $getusersToEmail = User::where("notifications", '>', 0)
        ->where("EmailEach2dayNoti", 1)
        ->where("verified", 1)
        ->get();
            foreach ($getusersToEmail as $mailuser) 
            {
                
                $data = array( 'email' => $mailuser->email, 'name' => $mailuser->name );
                Mail::send('email.emailNotify', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'] )
                    ->subject('Activity on your Fawour profile');
                });
            }
    }
}
