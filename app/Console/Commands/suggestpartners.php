<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\User;
Use Auth;
use Carbon\Carbon;

class suggestpartners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:suggestpartners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mails about partners to team up with';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
          $getusersToEmail = User::
        where("verified", 1) 
        //->where("id", 1)
        ->where('last_login', '<=', Carbon::now()->subDays(4)->toDateTimeString())
        ->where("EmailEach2dayNoti", 1)
        ->get();

        foreach ($getusersToEmail as $mailuser) 
            { 
              if($mailuser->field == "Not set" || $mailuser->lang == "Not set"){

                $theseuser = User::where('last_login', '>=', Carbon::now()->subDays(30)->toDateTimeString())
                ->whereNotIn('id', [$mailuser->id])
                ->inRandomOrder()->take(4)->get();

              } 
              else if ($mailuser->field != "Not set" && $mailuser->lang != "Not set")
              {
                     $theseuser = User::where('last_login', '>=', Carbon::now()->subDays(30)->toDateTimeString())
                     ->where('field', $mailuser->field)
                     ->where('lang', $mailuser->lang)
                    ->whereNotIn('id', [$mailuser->id])
                     ->inRandomOrder()->take(4)->get();
              }
               $data = array( 
                'email' => $mailuser->email, 
                'name' => $mailuser->name,
                'theseuser' => $theseuser,
                );
                Mail::send('email.suggestpartners', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'], $data['theseuser'] )
                    ->subject('New partners to team up with');
                });
             
            
            }
        }
}
