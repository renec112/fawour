<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\User;
use Carbon\Carbon;

class MailNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:MailNews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $getusersToEmail = User::
        where("verified", 1)
        ->where('last_login', '<=', Carbon::now()->subDays(4)->toDateTimeString())
        ->where("EmailEach2dayNoti", 1)
        ->get();

        foreach ($getusersToEmail as $mailuser) 
            {
                
                $data = array( 'email' => $mailuser->email, 'name' => $mailuser->name );
                Mail::send('email.MailNews', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'] )
                        ->subject("New video tutorials");
                });
            }
         $getusersToEmail2 = User::where("verified", 1)
        ->where('created_at', '<=', Carbon::now()->subDays(4)->toDateTimeString())
        ->whereNull('last_login')
        ->where("EmailEach2dayNoti", 1)
        ->get();
        foreach ($getusersToEmail2 as $mailuser) 
            {
                
                $data = array( 'email' => $mailuser->email, 'name' => $mailuser->name );
                Mail::send('email.MailNews', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'] )
                        ->subject("New video tutorials");
                });
            }
    }
}
