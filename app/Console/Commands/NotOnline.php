<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\User;
use Carbon\Carbon;

class NotOnline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:NotOnline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email if not active for a while';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $getusersToEmail = User::
        where("verified", 1)
        ->where('last_login', '<=', Carbon::now()->subDays(7)->toDateTimeString())
        ->where("EmailEach2dayNoti", 1)
        ->get();

        foreach ($getusersToEmail as $mailuser) 
            {
                
                $data = array( 'email' => $mailuser->email, 'name' => $mailuser->name );
                Mail::send('email.notOnline', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'] )
                    ->subject('Stuff is happening on Fawour');
                });
            }
         $getusersToEmail2 = User::where("verified", 1)
        ->where('created_at', '<=', Carbon::now()->subDays(7)->toDateTimeString())
        ->whereNull('last_login')
        ->where("EmailEach2dayNoti", 1)
        ->get();
        foreach ($getusersToEmail2 as $mailuser) 
            {
                
                $data = array( 'email' => $mailuser->email, 'name' => $mailuser->name );
                Mail::send('email.notOnline', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'] )
                    ->subject('Stuff is happening on Fawour');
                });
            }
    }
}
