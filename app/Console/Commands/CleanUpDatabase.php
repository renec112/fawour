<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Url;
use App\Activity;
use Carbon\Carbon;
use App\Chat;
use App\messages;


class CleanUpDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:CleanUpDatabase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean up the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $newusers = user::
        where('created_at', '>=', Carbon::now()->subDays(1)->toDateTimeString())
        ->get();
        foreach ($newusers as $thisUser) 
        {
            $message = "Hi {$thisUser->name}. Thank you so much for trying Fawour. It means a lot to me. I'm the creator and you can contact me here if you have any questions or ideas. Welcome to the site!";
                 messages::create([
                'to_user' => $thisUser->id,
                'body' => $message,
                'user_id' => 1,
            ]);
                $notification = new Activity;
                $notification->user_id = $thisUser;
                //increment notifyication
                $userIncrementNotifcation = User::where("id", $thisUser->id)->first();
                $userIncrementNotifcation->increment('notifications');
                $notification->from_id =1;
                $notification->user_id = $thisUser->id;
                $notification->notifyThis = true;
                $notification->type = 'MessageSent';
                $notification->route = "Mmrene";
                $notification->save();
        }
        // gets old unseen notifications
        $deleteNotifications = Activity::
            where('notifyThis', 0)
            ->where('created_at', '<=', Carbon::now()->subDays(30)->toDateTimeString())
            ->get();

            if(!$deleteNotifications->isEmpty()){
                foreach ($deleteNotifications as $deleteNotificationsdelete){ 
                    $deleteNotificationsdelete->delete();
                }
                
            } 

        // 30 days chat commments delete
        $xeleteChat = Chat::
            where('created_at', '<=', Carbon::now()->subDays(30)->toDateTimeString())
            ->get();
            
            if(!$xeleteChat->isEmpty()){
                foreach ($xeleteChat as $xeleteChatdeletethis){ 
                    $xeleteChatdeletethis->delete();
                }
        } 

    }
}
