<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Url;
use App\Activity;
use App\analytics;
use Carbon\Carbon;
use App\Chat;
use Auth;

class logDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:logDaily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'logDaily database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
   $allUsers = User::where("verified", 1)->get();
        // loop 
        foreach ($allUsers as $instance){
            $userToUpdateDailyPoints = (User::where("id",$instance->id)->first());

            $newValueToStoreGiven = (($userToUpdateDailyPoints->pointsGiven) - ($userToUpdateDailyPoints->GivenThisDay));
            $newValueToStoreGotten = (($userToUpdateDailyPoints->pointsGotten) - ($userToUpdateDailyPoints->GottenThisday));
                
            // store the difference.
            $extraData = analytics::create([
                'user_id' => $userToUpdateDailyPoints->id, 
                'GottenThisday' => $newValueToStoreGotten,
                'GivenThisDay' => $newValueToStoreGiven,
            ]);
              // reset the difference 
            $userToUpdateDailyPoints->update(['GivenThisDay' => $userToUpdateDailyPoints->pointsGiven]);
            $userToUpdateDailyPoints->update(['GottenThisday' => $userToUpdateDailyPoints->pointsGotten]);
        }
        // delete old data for each user
         $deleteAnalyticsData = analytics::where('created_at', '<=', Carbon::now()->subDays(31)->toDateTimeString())->get();
            
        if(!$deleteAnalyticsData->isEmpty()){
            foreach ($deleteAnalyticsData as $deletethis){ 
                $deletethis->delete();
            }
        } 
    }
}
