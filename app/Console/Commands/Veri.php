<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\User;

class Veri extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Commands:Veri';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Help';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $getusersToEmail = User::where("verified", 0)
        ->whereNotIn('emailCode', ['DELETED'])
        ->get();
            foreach ($getusersToEmail as $mailuser) 
            {
                
                $data = array( 'email' => $mailuser->email, 'name' => $mailuser->name, 'code' => $mailuser->emailCode );
                Mail::send('email.verifyy', $data, function($message) use ($data){
                    $message->to($data['email'], $data['name'], $data['code'] )
                    ->subject('We never saw you on Fawour');
                });
            }
    }
}
