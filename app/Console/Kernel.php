<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SendEmailNoti::class,
        Commands\NotOnline::class,
        Commands\CleanUpDatabase::class,
        Commands\logDaily::class,
        Commands\MailNews::class,
        Commands\suggestpartners::class,
        Commands\Veri::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->command('SendEmailNoti')
                 ->hourly();*/
    }
}
