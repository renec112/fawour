<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;

class analytics extends Model
{
    protected $table = "analytics";

    protected $fillable = [
    "GottenThisday",
    "GivenThisDay",
    "user_id",
    ]; 

    public function user()
    {
        return $this->belongsTo("App\user", "user_id");
    }
}