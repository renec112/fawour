<?php

namespace App;

use Auth;
use App\Url;
use App\url_to_partner;
use Cache;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'description',
        'GivenThisDay',
        'GottenThisday',
        'field',
        'lang',
        'ratio',
        'token',
        'emailCode',
        'last_login',
        'settings',
        'Avatarpath',

        'notifications',
        'EmailEach2dayNoti',
        'EmailifNotActiveIn7Days',

        'hasFacebook',
        'facebookUrl',

        'hasVine',
        'vineUrl',

        'hasPinterest',
        'pinterestUrl',

        'hasTwitter',
        'twitterUrl',

        'hasyoutube',
        'YoutubeUrl',

        'hasTumblr',
        'tumblrUrl',

        'hasBlog',
        'BlogUrl',

        'linkedinUrl',
        'googleplusUrl'.

        'hasInstagram',
        'instagramUrl',
    ];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = [
        'image', 'smiley',
    ];
    protected $casts = [
        'settings' => 'json',
    ];
    public function settings(){
        return new Settings($this);
    }
    protected static function boot()
   {
       parent::boot();
       self::saving(function ($model) {
           $model->email = strtolower($model->email);
       });
   }
    /**
     * A User owns many Flyers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function flyers()
    {
        return $this->hasMany('App\Url');
    }
    public function chat(){
        return $this->hasMany('App\Chat');
    }
    public function activity()
    {
        return $this->hasMany("App\Activity");
    }

    public function generateCode($current_id){
        // generate short url 
        $num = $current_id;
        $codeset = "0123456789abcdfghjklmnpqrstvwxyzABCDFGHJKLMNPQRSTVWXYZ";
        $base = strlen($codeset);
        $converted = "";
        while ($num > 0) {
          $converted = substr($codeset, ($num % $base), 1) . $converted;
          $num = floor($num/$base);
        }
        return $converted;
    }
    /**
     * A User can publish a Flyer.
     *
     * @param Flyer $flyer
     */

    public function publish(url $url)
    {
        
        //store the url normal
       $this->flyers()->save($url);
         // Notfy and log activity

        $storeThisUrl = $url->url;
        // Gets all partners id
        $myPartnerstoGetUrl = Auth::user()->partners();
   
        // create new column for each partner
        foreach ($myPartnerstoGetUrl as $instance){
            $extraData = url_to_partner::create([
                'trafic_from_user' => $instance->id,
                'code' => 1,
                'url' => $storeThisUrl,
                'trafic_to_user' => Auth::user()->id,
                'url_id' => $url->id,
                ]);
            $extraData->code = $this->generateCode($extraData->id); 
            $extraData->save(); 

            // notify each user on url being crated
            $notification = new Activity;
            $notification->user_id = $instance->id;
            //increment notifyication
            $userIncrementNotifcation = User::where("id", $instance->id)->first();
            $userIncrementNotifcation->increment('notifications');
            $notification->from_id = $url->user_id;
            $notification->type = 'created_url';
            $notification->route = Auth::user()->name;
            $notification->notifyThis = true;
            $notification->data = $url->description;
            $notification->save();
            // notify end
        }
    }
    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public function owns($relation)
    {

        return $relation->user_id == $this->id;
    }
    public static function returnSmiley($ratio){
      switch (true) {
        case ($ratio == 0) :
            return "baby";
        break;
        case ($ratio < 0.1) :
            return "rage";
        break;
        case ($ratio >= 0.1 && $ratio <= 0.2) :
            return "angry";
        break;

         case ($ratio >= 0.2 && $ratio <= 0.4) :
            return "unamused";
        break;

         case ($ratio >= 0.4 && $ratio <= 0.7) :
            return "expressionless";
        break;

         case ($ratio >= 0.7 && $ratio <= 0.9) :
            return "relaxed";
        break;

         case ($ratio >= 0.9 && $ratio <= 1.2) :
            return "smile";
        break;

         case ($ratio >= 1.2 && $ratio <= 1.5) :
            return "smiley";
        break;

         case ($ratio > 1.5) :
            return "innocent";
        break;
    }
}
    public static function returnColor($field){
      switch (true) {
        case ($field == "Not set") :
            return "grey";
        break;
        case ($field == "ha") :
            return "red";
        break;
        case ($field == "gam") :
            return "orange";
        break;
         case ($field == "bf") :
            return "yellow";
        break;

         case ($field == "mu") :
            return "olive";
        break;

         case ($field == "te") :
            return "green";
        break;

         case ($field == "de") :
            return "blue";
        break;

         case ($field == "cr") :
            return "violet";
        break;

         case ($field == "li") :
            return "purple";
        break;
        case ($field == "fo") :
            return "pink";
        break;
        case ($field == "tr") :
            return "brown";
        break;
        case ($field == "ot") :
            return "black";
        break;
    }
    }
    public function getSmileyAttribute()
    {
        return $this->returnSmiley($this->ratio);
    }
    public function getName()
    {
        return ucfirst($this->name);
    }
    public function getImage()
    {
        if($this->Avatarpath == null){
                $first = "https://www.gravatar.com/avatar/";
                $second = md5($this->email);
                $last = "?d=mm&s=35";
                $result = $first . $second .$last;
            return $result;
        } else{
             $first = "/uploads/avatars/35";
             $second = $this->Avatarpath;
             $result = $first . $second;

            return $result;
        }
    }
    public function getImageAttribute()
    {
        return $this->getImage();
    }
    public function getpofilepic()
    {
        if($this->Avatarpath == null){
                $first = "https://www.gravatar.com/avatar/";
                $second = md5($this->email);
                $last = "?d=mm&s=300";
                $result = $first . $second .$last;
            return $result;
        } else{
             $first = "/uploads/avatars/";
             $second = $this->Avatarpath;
             $result = $first . $second;
            return $result;
        }
    }
    public function getpofilepic20()
    {
        if($this->Avatarpath == null){
                $first = "https://www.gravatar.com/avatar/";
                $second = md5($this->email);
                $last = "?d=mm&s=20";
                $result = $first . $second .$last;
            return $result;
        } else{
             $first = "/uploads/avatars/20";
             $second = $this->Avatarpath;
             $result = $first . $second;

            return $result;
        }
    }
    public function getpofilepic120()
    {
        if($this->Avatarpath == null){
                $first = "https://www.gravatar.com/avatar/";
                $second = md5($this->email);
                $last = "?d=mm&s=120";
                $result = $first . $second .$last;
            return $result;
        } else{
             $first = "/uploads/avatars/120";
             $second = $this->Avatarpath;
             $result = $first . $second;

            return $result;
        }
    }
        public function getpofilepic80()
    {
        if($this->Avatarpath == null){
                $first = "https://www.gravatar.com/avatar/";
                $second = md5($this->email);
                $last = "?d=mm&s=80";
                $result = $first . $second .$last;
            return $result;
        } else{
             $first = "/uploads/avatars/80";
             $second = $this->Avatarpath;
             $result = $first . $second;

            return $result;
        }
    }
            public function getpofilepic35()
    {
        if($this->Avatarpath == null){
                $first = "https://www.gravatar.com/avatar/";
                $second = md5($this->email);
                $last = "?d=mm&s=35";
                $result = $first . $second .$last;
            return $result;
        } else{
             $first = "/uploads/avatars/35";
             $second = $this->Avatarpath;
             $result = $first . $second;

            return $result;
        }
    }
    public function getAvatar() 
    {   
        $first = "https://www.gravatar.com/avatar/";
        $second = md5($this->email);
        $last = "?d=mm&s=20";
        $result = $first . $second .$last;
        return $result;

    }
        public function getAvatarmed()
    {   
        $first = "https://www.gravatar.com/avatar/";
        $second = md5($this->email);
        $last = "?d=mm&s=35";
        $result = $first . $second .$last;
        return $result;

    }
     public function getAvatarbig()
    {   
        $first = "https://www.gravatar.com/avatar/";
        $second = md5($this->email);
        $last = "?d=mm&s=80";
        $result = $first . $second .$last;
        return $result;
    }
         public function getAvatarverybig()
    {   
        $first = "https://www.gravatar.com/avatar/";
        $second = md5($this->email);
        $last = "?d=mm&s=120";
        $result = $first . $second .$last;
        return $result;
    }

    public function messages()
    {
        return $this->hasMany("App\messages", "user_id");

    }
    public function analytics()
    {
        return $this->hasMany("App\analytics", "user_id");

    }

    public function partnersOfMine()
    {
        return $this->belongsToMany("App\User", "partners", "user_id", "partner_id");
    }

    public function partnersOf()
    {
        return $this->belongsToMany("App\User", "partners", "partner_id", "user_id");
    }

    public function partners()
    {
        return $this->partnersOfMine()->wherePivot("accepted", true)->get()
        ->merge($this->partnersOf()
        ->where("accepted", true)
        ->where("verified", 1)
        ->get());
    }


    public function partnerRequest()
    {
        return $this->partnersOfMine()->wherePivot("accepted", false)->get();
    }

    public function partnerRequestsPending()
    {
        return $this->partnersOf()->wherePivot("accepted", false)->get();
    }

    public function hasPartnerRequestspending(User $user)
    {
        return(bool) $this->partnerRequestsPending()->where("id", $user->id)->count();
    }

    public function hasPartnerRequestReceived(User $user)
    {
        return(bool) $this->partnerRequest()->where("id", $user->id)->count();
    }

    public function addPartner(User $user)
    {
        $this->partnersOf()->attach($user->id);
    }

    public function deletePartner(User $user)
    {
        $this->partnersOf()->detach($user->id);
        $this->partnersOfMine()->detach($user->id);
    }

    public function acceptPartnerRequest(User $user)
    {

        //Get all urls, friend doens't have

        $missingUrlsAccept = Url::where("user_id", $user->id)->get();
        // create new column for each partner
        foreach ($missingUrlsAccept as $instance){
            $extraData = url_to_partner::create([
                'trafic_from_user' => Auth::user()->id,
                'code' => 1,
                'url' => $instance->url,
                'trafic_to_user' => $user->id,
                'url_id' => $instance->id,
                ]);
            $extraData->code = $this->generateCode($extraData->id); 
            $extraData->save(); 
        }


        $missingUrlsUserGetAccepted = Url::where("user_id", Auth::user()->id)->get();
        // create new column for each partner
        foreach ($missingUrlsUserGetAccepted as $instance){

            $extraData = url_to_partner::create([
                'trafic_from_user' => $user->id,
                'code' => 1,
                'url' => $instance->url,
                'trafic_to_user' => Auth::user()->id,
                'url_id' => $instance->id,
                ]);
            $extraData->code = $this->generateCode($extraData->id); 
            $extraData->save(); 
        }

        //Give all urls

        // Accept request
        $this->partnerRequest()->where("id", $user->id)->first()->pivot->update([
            "accepted" =>true,
            ]);
    }
      public function declinePartnerRequest(User $user)
    {
        $this->partnerRequest()->where("id", $user->id)->first()->pivot->delete();
    }

    public function isPartnerWith(User $user)
    {
        return (bool) $this->partners()->where("id", $user->id)->count();
    }
}
