<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Url;


class Activity extends Model
{
    protected $table = "activities";

    protected $fillable = [
    "user_id",
    "from_id",
    "type",
    "route",
    "notifyThis",
    ];

    public function user()
    {
     return $this->belongsTo("App\User");   
    }

    public function createNotification($user_id, $from_id, $type, $route, $notifyThis){
         Activity::create([
            "user_id" => $user->id,
            "from_id" => Auth::User()->id,
            "type" => "partnerRequestSend",
            "route" => Auth::User()->name,
            "notifyThis" => 1,
        ]);
    }

}