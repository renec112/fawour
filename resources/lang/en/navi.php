<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation lang
    |--------------------------------------------------------------------------
    |
    | navi
    |
    */

    'more' => 'More',
    'growtog' => 'Grow together with other creators',
    'getmore' => 'Get more valuable & legitimate visitors by sharing visitors with other creators',
    'toggle' => 'Toggle navigation',
    'udpateprofile' => 'Update profile',
    'help' => ' Help',
    'Notifications' => 'Notifications',
    'faq' => 'F.A.Q. ',
    'Usersettings' => 'User settings',
    'updatelog' => 'Update log',
    'Logout' => 'Logout',
    'Login' => 'Login',
    'Signup' => 'Sign up',
    'Home' => 'Home',
    'Messages' => 'Messages',
    'Partners' => 'Partners',
    'myprofile' => 'Profile',
    'Links' => 'Links',
    'Analytics' => 'Analytics',
    'theTour' => 'tour',
    'hello' => 'Hello',
    'whatshouldwe' => 'What should we',
    'improve' => 'improve?',

];

