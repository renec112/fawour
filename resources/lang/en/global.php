<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global langauge
    |--------------------------------------------------------------------------
    |
    | Global langauge
    |
    */

    'Hello' => 'Hello',
    'h1lead' => 'Grow by helping each other',
    'plead' => "Fawour is a network of creators that helps each other grow. Share your partners content and they'll share yours. Reach more and valuable visitors through collaboration",

    'madeBy' => 'Made by creators for creators.',
    'how' => 'How?',
    'section1' => 'Team up with creators in your field.',
    'section2' => 'Start sharing each others content.',
    'section3' => 'Increase visitors.',

    'whyFawour' => 'Why Fawour then?',
    'partnerupwith' => 'Partner up with',
    'creatorsinyourfild' => 'creators in your field.',
    'sameniche' => 'Find creators in the same niché as you and send them a partner request. For example, fashion bloggers would partner up with other fashion bloggers. If they accept your request, you are both ready to increase your visitors.',
    'valuableleg' => 'valuable & legitimate visitors.',
    'submitlinks' => 'Submit links to your content: articles, videos, etc. Each link you submit generates a special link for your partners. This special link counts how many visitors you got from each partner.',
    'youpartner' => 'Your partner have a lot of interesting links, as well - so return the fawour. For your partners content there is a special link for you to share as well, you can post this link anywhere: on an article, Youtube description, Facebook post and so on. Your own visitors will love you for providing them with extra relevant material.',
    'ncreaseisitors' => 'Start exchanging ',
    'Checkmate' => 'Checkmate.',
    'Increasevisitors' => 'Increase visitors. ',
    'win-win' => ' Now we have created a win-win situation for you and your partner. You willll help each other by sharing your visitors. The visitors will be less likely to end up at a 3rd rival - who does not benefit either of you. ',
    'anhareyour' => 'You can share your partners content when you don not have anything else to post that day. Or just add an extra link in your blogpost/youtube description. Your visitors will love you for providing them with great content. Do not worry about losing the visitors, they were going to leave you some time anyway. Thanks to your partners, you will ll get a fresh visitor back, for each you lost. Win win.',
    'togetheryouare' => 'Together, you are stronger',
    'tryyitout' => 'Try it out!',
    'Signup' => 'Sign up',
    'latestusers' => 'Latest users',
    'cheaat' => 'Is this cheating or spam?',
    'cheaat2' => 'No. It is valuable & legitimate  visitors exchange. Because your partner sends you interested and relevant visitors. 
You are in a mall and there is a lot of competition. It would be beneficial for two stores to help each other grow. lets say two clothing shops. In the first clothing shop, you can not find what you were looking for and the store assistant says "If you cannot find what you need here, try the one next-door!". The shop next-door makes sure to return the favour and redirect their customer as well. The result is you willl exchange real valuable & legitimate users that is actually interested in the content. In this case: cloth. The only difference with Fawour is we use links.',

    'grow222' => "Turn your competitors into colleagues ",
    'yousharetheirs' => "Boost your channel  by sharing  your followers
with other creator's followers. You share theirs, they'll share yours. Reach a new and larger audiences through cross channel collaboration with creators",
    'wemakeit' => 'We make it easy for you to grow by sharing your followers with other creators followers',
    'readmoroor' => 'Read more',
    'createanaccount' => 'Create an account',
    'accesthetool' => 'Sign up to get access to the tool. ',
    'startup' => 'We&#39;re a startup - everything is free. ',
    'Username' => 'Username',
    'email' => 'E-Mail Address',
    'Password' => 'Password',
    'conpassword' => 'Confirm Password',
    'Register22' => ' Register',
    'readmorrre' => ' Read more',

    'here' => 'here.',
    'Basics' => 'Basics',
    'countworkss' => 'How do does the count work?',
    'countworkanser' => 'The link you share have an ID. With that ID we can see who created the link and who shared it. That is how we can count "Visitors gotten" and "Visitors given". There is also a of bit security going on to avoid spam. When a user clicks on the link we store their IP address so they cannot give you 100 visitors by clicking on the link themselves. If the link does not count when you visit it, you have probably already visited it once. We also check for crawling bots, too. Off course we do not count for those.',
    'caniremove' => 'Can I remove my links?',
    'caniremoveans' => ' - just click on the big X. We are only soft-deleting your links. that is because if someone already shared your link the user should still get redirected and get counted. However, your partner will not be able to share the link, after you have soft-deleted it.',
    'howbigisthe' => 'How big is the site?',
    'howbigistheans' => 'Keep in mind we are a startup and are constantly growing. But you can view a few stats about us ',
    'howdoiuse' => 'How do I use the site?',
    'howdoiuseans' => 'There is a guided tour of the app after you log in.',
    'wjhatdoesitcost' => 'What does it cost?',

    'wjhatdoesitcostans' => 'it is free.',
    'Account' => 'Account',
    'howdoichange' => 'How do I change my password?',
    'howdoichangeans' => 'You log out and reset it like you have forgot the password. That is done',
    'hwopdoidelete' => 'How do I delete my account?',
    'hwopdoideleteans' => 'Go to account settings and select "delete account".',
    'hodoichangeset' => 'How do I change my account settings?',
    'hodoichangesetans' => 'Go my my profile, then press "information"',
    'iforgotpass' => 'I forgot my password. How do I reset it?',
    'iforgotpassans' => 'You can do it here',
    'xxrivacy' => 'Privacy',
    'whyneedemail' => 'Why do you need my email?',
    'answhyneedemail' => 'To avoid bots, spam, and bad behavior on our site. With an email, we can ban people. Also, confirming an email is very hard for bots. Also if you have "Received email" on, we will send you emails about old and unseen notifications. This can be undone on your settings',
    'cookiespriva' => 'What about cookies and privacy?',
    'cookiesprivabsa' => 'We only use cookies to make your experience better. One cookie for security, another to remember your login, and two cookies to use with google analytics. We use Google Analytics to get stats from our site - how many is online, demographics and such, nothing else. ',
    'dateaas' => 'Can anyone access data my data?',
    'dateaasanss' => 'Everything you enter is secured by Digital Ocean and Laravel 5.2. It is a secure system. Also, most information is hidden behind a login on Fawour. Most of your information is hidden - like your partners and ratio. But your name, profile text and platforms is available. This boost your SEO.',
    'mis' => 'Did we miss anything? Contact us',
    'moreFaw' => 'More fawour',
    'updateLog' => 'Update log',
    'About22' => 'About',
    'fawismadegettoach' => 'Fawour is made by creators for creators. Need to get in touch?',
    'contactushere' => 'Contact us here',


    'GetStarted' => 'Get Started',
    'howitworks' => 'How it works',
    'whatotherssay' => 'What others say',

    'youshouldchoose' => 'Why we rock',
    'itsfree' => "It's free",
    'itsfreedes' => "That's right. We just released our service and we invite everyone to try it out. Free of charge.",
    'Count' => "You share theirs they share yours",
    'Countdes' => 'Our uniqe short-link sharing system is both fun and makes sure you get as many visitors as you give. ',
    'easy' => "Easy",
    'easydes' => "Signup, find partners, submit links and go. That's all you need to start benefiting from with site",
    'Independent' => 'Independent',
    'Independentdes' => 'Fawour is made by a single blogger who wanted to make growing much easier. We just write "we" to sound more professional.',
    'Cumminity' => 'Community',
    'Cumminitydes' => 'Our site is for creators. Youtubers, bloggers and so on. Have fun, learn and grow together with them.',
    'Feature' => 'Feature rich',
    'Featuredes' => "Not only do we have a unique short-link sharing system, we also have other cool features like analytics, chat, messaging and so on.",

    'contToSite' => 'Continue to the site',
    'withLogin' => ' With',

    'FirstSubmitLink' => 'First submit links to your content',
    'nextPartner' => 'Next partner up with creators',
    'lastlyboostyourpartners' => 'Lastly, boost your partners by sharing their content. They will return the fawour!',
    'TheresNeat' => "There's neat stuff like analytics",
    'givenandgotten111' => 'See how many visitors each user has given and gotten',
    'tryItfree' => 'Try it for free',

    'fawhist' => 'Fawour history',
    'inependentwebapp' => "Fawour is an independent web application, made by a single Blogger / Youtuber - Rene Czepluch.
The idea behind Fawour started, because there's so much competition out there as a Blogger / Youtuber. Rene thought it would be super fun and very useful to be able to utilise your current amount of followers, to gain new followers. A way to do this is by using short-links. Short links allows for cross platform promotion and analytics. Fawour is kinda like Bit.ly but multiplayer.",
    'Reneisastudent' => 'Rene is a student from Denmark. Who studies physics, but used to work part-time with web-development and Photoshop. Rene Also has a Youtube channel and a Blog, where he shares photoshop articles and videos.',
    'suppthis' => 'Support',
    'costmoney' => 'Running an application like Fawour costs more than you would expect. If you want to help me cover some expenses, donate whatever you would like here:',
    'questt' => 'Questions?',
    'thanksTyler' => 'Thank you Tyler Arbon',
    'tylerhlped' => "Rene had a lot of issues creating Fawour. Not only did Tyler help solve all programming issues and server issues,  he also contributed with great ideas for a better user experience. Fawour thanks Tyler Arbon - it wouldn't exist  without him. ",
    'checkhisproject' => 'Check out his projects here.',
    'Contactme11' => 'Contact me',


];
