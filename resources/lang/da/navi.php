<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation lang
    |--------------------------------------------------------------------------
    |
    | navi
    |
    */

    'more' => 'Mere',
    'growtog' => 'Gro sammen med andre indholdsskabere',
    'getmore' => 'Få flere og værdifulde besøgende ved at dele besøende med andre indholdsskabere',
    'toggle' => 'Udvid',
    'udpateprofile' => 'Opdater profil',
    'help' => ' Hjælp',
    'Notifications' => 'Notifikationer',
    'faq' => 'F.A.Q. ',
    'Usersettings' => 'Bruger indstilling',
    'updatelog' => 'Updateringer',
    'Logout' => 'Log ud',
    'Login' => 'Log ind',
    'Signup' => 'Tilmeld',
    'Home' => 'Hjem',
    'Messages' => 'Beskeder',
    'Partners' => 'Partnere',
    'myprofile' => 'Profil',
    'Links' => 'Links',
    'Analytics' => 'Statistik',
    'theTour' => 'tourda',
    'hello' => 'Hej',
    'whatshouldwe' => 'Hvad kan vi',
    'improve' => 'forbedre?',

];

