<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global langauge
    |--------------------------------------------------------------------------
    |
    | Global langauge
    |
    */

  'Hello' => 'Hej',
    'h1lead' => 'Gro ved at hjælpe hinanden',
    'plead' => "Fawour er et netværk er indholdsskabere der hjælper hinanden med at gro. Del dine partners indhold og de vil dele dit. Nå flere og relevante besøgende gennem et samarbejde.",
    'madeBy' => 'Lavet af indholdsskabere til indholdsskabere.',
    'how' => 'Hvordan?',
    'section1' => 'Find indholdsskabere i dit felt.',
    'section2' => 'Del hinandens indhold.',
    'section3' => 'Øg mængden af besøgende.',
    'whyFawour' => 'Hvorfor så Fawour?',
    'partnerupwith' => 'Find partnere',
    'creatorsinyourfild' => ' i dit felt.',
    'sameniche' => 'Find indholdsskabere i samme niche som dig og send dem en anmodning til at arbejde sammen. Eksempelvis finder Fashion bloggers sammen med andre fashion bloggere. Hvis de acceptere din anmidning er i begge klar til at øge mængden af besøgende.',
    'valuableleg' => 'relevante besøgende.',
    'submitlinks' => 'Gem links til dit indhold: Artikler, videoer osv. Hvert link du gemmer generer et specielt link til dine partnere. Dette link tæller hvor mange besøgende hver af dine partnere har givet dig.',
    'youpartner' => 'Din partnere har også interessante links. Så giv da også dem en tjeneste (Fawour). For hver af dine partneres link er der også et link du kan dele. Del dette linket på din Youtube video beskrivelse, Facebook post osv. Dine følgere vil elske dig for at vise dem andet relevant materiale.',
    'ncreaseisitors' => 'Start med at bytte ',
    'Checkmate' => 'Checkmate.',
    'Increasevisitors' => 'Få flere besøgende. ',
    'win-win' => 'Nu har vi lavet en win-win siutation for dig og din partner. I vil begge hjælpe hinanden med at gro, ved at dele besøgende. Disse besøgende vil derfor befinde sig i midnre grad hos en anden Konkurrent der ikke hjælper nogle af jer.',
    'anhareyour' => 'Du kan dele dine partners indhold når du alligevel ikke har noget at poste den dag. Eller du kan tilføje disse links omkring din blog eller youtube. Dine følgere vil elske dig for at vise dem nyt materiale. Du sender måske dine følgere væk, men de ville jo klikke ved på et tidspunkt. På denne måde hjælper du dem videre og du får jo en ny besøgende tilbage fra personen du sender dine følgere videre til.',
    'togetheryouare' => 'Sammen er du stærkere',
    'tryyitout' => 'Prøv det!',
    'Signup' => 'Tilmeld',
    'latestusers' => 'Seneste brugere',
    'cheaat' => 'Er det her ikke snyd eller spam?',
    'cheaat2' => 'Nej. Det er relevant og værdifuld udveksling af besøgende. Forestil dig følgende scenarie: I et storcenter med mange konkurrenter, vil det være en fordel for to tøjbutikker at arbejde sammen - Altså de aftaler at dele om deres kunder.  Du er inde i den første tøjbutik H&M og kan ikke finde hvad du leder efter, så ekspedienten siger “Prøv inde hos Vero-moda”. I Vero-moda gør de noget i samme stil “Hey har du set det nye tøj hos H&M?”. Der er nu mindre chance for, at kunden går over til en tredje konkurrent, der ikke hjælper nogle af de to butikker. Fawour gør det samme. Vi sender kunden/besøgende videre på samme måde. Vi bruger bare links i stedet. I eksempelet giver det selvfølgelig ikke mening, hvis en fiskebutik sendte kunden videre til en tøjbutik. Derfor kan man finder indholdsskabere i samme felt som sig selv på Fawour.
',
    'grow222' => 'Gør dine konkurrenter til kollegaer',
    'yousharetheirs' => 'Du deler deres og de deler dit. Opnå flere og relevante besøgende gennem samarbejde med andre indholdsskabere.',
    'wemakeit' => 'Vi gør det nemt for dig at vokse, ved simplificere samarbejde mellem indholdsskabere.',
    'readmoroor' => 'Læs mere',
    'createanaccount' => 'Lav en bruger',
    'accesthetool' => 'Tilmeld dig og få adgang til værktøjet. ',
    'startup' => 'Vi er i opstarts fasen. Alt er gratis. ',
    'Username' => 'Brugernavn',
    'email' => 'E-Mail',
    'Password' => 'Kodeord',
    'conpassword' => 'Gentag kodeord',
    'Register22' => ' Tilmeld',
    'readmorrre' => ' Læs mere',

    'here' => 'her.',
    'Basics' => 'Basics',
    'countworkss' => 'Hvordan tæller i?',
    'countworkanser' => '** ikke oversat endnu ** The link you share have an ID. With that ID we can see who created the link and who shared it. That is how we can count "Visitors gotten" and "Visitors given". There is also a of bit security going on to avoid spam. When a user clicks on the link we store their IP address so they cannot give you 100 visitors by clicking on the link themselves. If the link does not count when you visit it, you have probably already visited it once. We also check for crawling bots, too. Off course we do not count for those.',
    'caniremove' => 'Kan jeg slette mine links?',
    'caniremoveans' => '** ikke oversat endnu **  - just click on the big X. We are only soft-deleting your links. that is because if someone already shared your link the user should still get redirected and get counted. However, your partner will not be able to share the link, after you have soft-deleted it.',
    'howbigisthe' => 'Hvor stor er siden?',
    'howbigistheans' => '** ikke oversat endnu ** Keep in mind we are a startup and are constantly growing. But you can view a few stats about us ',
    'howdoiuse' => 'Hvordan bruger jeg siden?',
    'howdoiuseans' => '** ikke oversat endnu ** There is a guided tour of the app after you log in.',
    'wjhatdoesitcost' => 'Hvad koster det?',

    'wjhatdoesitcostans' => 'Det er gratis',
    'Account' => 'Bruger',
    'howdoichange' => 'Hvordan ændrer jeg mit kodeord?',
    'howdoichangeans' => '** ikke oversat endnu ** You log out and reset it like you have forgot the password. That is done',
    'hwopdoidelete' => 'Hvordan sletter jeg min bruger?',
    'hwopdoideleteans' => '** ikke oversat endnu ** Go to account settings and select "delete account".',
    'hodoichangeset' => 'hvordan ændrer jeg mine indstillinger?',
    'hodoichangesetans' => '** ikke oversat endnu ** Go my my profile, then press "information"',
    'iforgotpass' => 'Hvordan ændrer jeg mit kodeord?',
    'iforgotpassans' => '** ikke oversat endnu ** You can do it here',
    'xxrivacy' => 'Sikkerhed',
    'whyneedemail' => 'Hvad skal I bruge min email till?',
    'answhyneedemail' => '** ikke oversat endnu ** To avoid bots, spam, and bad behavior on our site. With an email, we can ban people. Also, confirming an email is very hard for bots. Also if you have "Received email" on, we will send you emails about old and unseen notifications. This can be undone on your settings',
    'cookiespriva' => 'Hvad med cookies?',
    'cookiesprivabsa' => '** ikke oversat endnu ** We only use cookies to make your experience better. One cookie for security, another to remember your login, and two cookies to use with google analytics. We use Google Analytics to get stats from our site - how many is online, demographics and such, nothing else. ',
    'dateaas' => 'Kan alle se mine data?',
    'dateaasanss' => '** ikke oversat endnu ** Everything you enter is secured by Digital Ocean and Laravel 5.2. It is a secure system. Also, most information is hidden behind a login on Fawour. Most of your information is hidden - like your partners and ratio. But your name, profile text and platforms is available. This boost your SEO.',
    'mis' => 'Missede vi noget? Kontakt os',
        'moreFaw' => 'Mere Fawour',
    'updateLog' => 'Update log',
    'About22' => 'Om',
    'fawismadegettoach' => 'Fawour er lavet af indholdsskabere til indholdsskabere. Skal vi skrives ved?',
    'contactushere' => 'Kontakt os her.',

      'GetStarted' => 'Kom i gang',
    'howitworks' => 'Sådan virker det',
    'whatotherssay' => 'Det siger andre',


    'youshouldchoose' => 'Derfor skal du prøve os',
    'itsfree' => "Det er gratis",
    'itsfreedes' => "Vi har lige udgivet denne service og invitere derfor alle til at prøve det. Helt gratis.",
    'Count' => 'Du deler deres de deler dit',
    'Countdes' => 'Vores unikke short-link dele system er både sjov, og sikrer samtidig man får så mange besøgende, som man giver.',
    'easy' => "Nemt",
    'easydes' => "Tilmeld, find partnere, indsend links. Det er alt der skal til, for at starte med at vokse via denne service.",
    'Independent' => 'Uafhængig',
    'Independentdes' => 'Fawour er lavet af en enkelt blogger, der ville gøre det nemmere at vokse. Vi skriver bare "vi" for at lyde lidt mere professionel. ',
    'Cumminity' => 'Fællesskab',
    'Cumminitydes' => 'Siden er for indholdsskabere. Youtubere, bloggere osv. Hyg dig, lær, og gro sammen med dem.',
    'Feature' => 'Features',
    'Featuredes' => "Ikke nok med vi har et helt unikt shourt-link dele system, har vi også smarte features som statisitk, chat, beskeder, osv.",


    'contToSite' => 'Fortsæt til siden',
    'withLogin' => ' Med',

    'FirstSubmitLink' => 'Først, indsend linsk til dit indhold',
    'nextPartner' => 'Dernæst, bliv partnere med nogle creators',
    'lastlyboostyourpartners' => 'Til slut, boost dine partnere ved at dele deres indhold. De vil gøre det samme for dig',
    'TheresNeat' => "Der er lækre ting på siden som statistik",
    'givenandgotten111' => 'Se hvor mnage besøgende en bruger har fået og givet',
    'tryItfree' => 'Prøv det gratis',

      'fawhist' => 'Historien om Fawour',
    'inependentwebapp' => "Fawour er en uafhængig web applikation, lavet af en blogger / youtuber - Rene Czepluch.
Ideen bag Fawour opstod, fordi der er så meget konkurrence som blogger/youtube. Rene Tænkte, det ville være sjovt og yderst brugbart at kunne anvende ens nuværende mængde af følgere, til at finde nye og relevante. En måde at kunne gøre det, er med Fawours short-links. Det muliggører, at dele indhold på tværs af sociale platforme. Det er lidt ligesom Bit.ly - bare multiplayer.",
    'Reneisastudent' => 'Rene er en studerende fra Aarhus. Han studere Fysik, men laver lidt blogs og Youtube i fritiden. Rene plejede at arbejde med web udvikling, hvilket muliggjorde produktionen af dette site. 
',
    'suppthis' => 'Støt',
    'costmoney' => 'Det koster meget at holde Fawour kørende. Hvis du gerne vil hjælpe med nogle udgifter, vil jeg sætte stor pris på en donation her:',
    'questt' => 'Spørgsmål?',
    'thanksTyler' => 'Tak Tyler Arbon',
    'tylerhlped' => "Tyler har været en kæmpe hjælp til udivklingen af Fawour. Han hjalp med at fikse programemrs og server fejl, og havde mange gode forslag til en bedre bruger oplevelse. Fawour takker Tyler Arbon. Det ville ikke eksistere uden ham.",
    'checkhisproject' => 'Tjek hans projekter her.',
    'Contactme11' => 'Kontakt mig',


];
