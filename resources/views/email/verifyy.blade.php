<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Dang it!</h2>

        <div>
           Hello {{$name}}.
            <br> sadly, we never get to know you on Fawour.com. Did the verification code not show up?  Please follow the link below to verify your email address <br>
            {{ URL::to('register/verify/' . $code) }}.<br/>
            <br>
            Thank you so much for being a part of fawour!

        </div>

    </body>
</html>