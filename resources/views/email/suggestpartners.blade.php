<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Hello {{$name}} </h2>

        <div>
            Here is a few people you could partner up with and grow :)<br>
            @foreach($theseuser as $user1)
            <br>
            <h3><a href="fawour.com/partner/{{$user1->name}}">{{$user1->name}}</a></h3>
            <br>
            {{$user1->description}}
            <br>
            <img src="https://fawour.com/{{ $user1->getpofilepic120() }}"/>
            <br>
            <br>          
            @endforeach
            
            <br><br>
            - - - 
            <br> 
            Have a great day and see you on <a href="https://fawour.com">Fawour</a>.
            <br>

            Don't wanna receive these emails? Update your settings <a href="https://fawour.com/profile/updateEmailSettings/{{$email}}/{{$name}}">here</a>.



        </div>

    </body>
</html>