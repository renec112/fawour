<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>New notifications on Fawour</h2>

        <div>
            Hello,<br/>

            You have new notifications on<a href="http://fawour.com/">Fawour</a>.<br>
            We can't wait to see you on the site.
            <br> Thank you!
            <br><br>

            Don't wanna receive these emails? Update your settings here:
            <a href="https://fawour.com/profile/updateEmailSettings/{{$email}}/{{$name}}">here</a>.



        </div>

    </body>
</html>