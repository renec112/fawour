<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
            <div>        
            
            <h3>Hello,</h3>
            There's been a little confusion about what Fawour is actually about and how to use the site.<br>
            I just released 2 new videos explaining what Fawour is about and how you can use the site. I hope you want to check them out!
            <br>
            <a href="https://www.youtube.com/watch?v=_RMLPWP8Uco">Video: Why Fawour can boost your channel</a>
            <br>
            <a href="https://www.youtube.com/watch?v=oU4E48gWOt0">Video: How to use Fawour</a>
            <br><br>
            <br> See you on the site.<br> - Rene 

            <br>  <br>   <br>  Thank you so very much for being a part of Fawour. And stay cool :)
            <br> Need to get in touch? Reply <a href="https://docs.google.com/forms/d/e/1FAIpQLSez07_zXZHcpJoGlLaJ_QAtXNr2gxyKks5qWHXs3NrK6s_0qQ/viewform">here.</a>
            <br>
            Don't wanna receive these emails? Update your settings on the site.

        </div>

    </body>
</html>