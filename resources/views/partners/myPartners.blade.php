@extends('layout')

@section('content')

<?php
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url) and $url != '') {
        $url = "http://" . $url;
    }
    return $url;
}
$user->BlogUrl = addhttp($user->BlogUrl);
$user->vineUrl = addhttp($user->vineUrl);
$user->pinterestUr = addhttp($user->pinterestUr);
$user->facebookUrl = addhttp($user->facebookUrl);
$user->instagramUrl = addhttp($user->instagramUrl);
$user->twitterUrl = addhttp($user->twitterUrl);
$user->tumblrUrl = addhttp($user->tumblrUrl);
$user->YoutubeUrl = addhttp($user->YoutubeUrl);
?>
	<style>
	.statsstyle div{
		text-align: center;
	}

	.platformEntry{
		display:inline-block;
		margin-right:5px;
	}
	 .image.label.large{
	 	margin-bottom:5px;
	 }
	   .icononProfile{
        font-size:1em !important;
    }
	</style>
		 @include('pages.partials.menuprofile')

	 <div class="row">
		
	 	<div class="col-lg-8 col-md-8 col-sm-12 getWhite">
				<div class="row frontprofile getWhite">
				<div class="col-lg-8 col-md-8 col-sm-8">
					<h2>{{$user->Getname()}} </h2>
					
					<p><span class="getitalic smalldetail">{{$user->description}}</span></p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<h1><a href="/profile/avatar">

					<div id="myimageofme" class="circle_the_text"><img class="media-object circle_the_text" alt="" style="margin:0 auto;" class="" src="{{ $user->getpofilepic120() }}"></div></a></h1>
				
				</div>

				

			</div>
<hr>
	 		<div class="row statsstyle getWhite">
					<div id="Visitorsgiven" class="col-lg-3 col-md-6 col-sm-6">
					<h1>{{$user->pointsGiven}}</h1>
						<p >{{ trans('app.Visitorsgiven') }} </p>
					
						

					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<h1 id="Visitorsgotten">{{$user->pointsGotten}}</h1>
						<p>{{ trans('app.Visitorsgotten') }} </p>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
							<h1 id="Visitorsratio">{{$user->ratio * 100}}<i class="twa  twa-{{$user::returnSmiley($user->ratio)}}"></i></h1>
							<p >{{ trans('app.givegetratio') }} </p>
					</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
							<h1><a href="{{route("myUrls")}}">{{$urls}}</a></h1>
							<p>{{ trans('app.linkscreated') }}</p>
					</div>
					
					</div>
					<hr>
	
	 				<div class="row getWhite">
							<div class="col-lg-3 col-md-6 col-sm-6">
							<h3 >{{ trans('app.field') }}</h3>
							 <a class="ui basic {{ $user->returnColor($user->field) }} label">{{App\Http\Utilities\Country::getFieldName($user->field)}}</a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<h3 >{{ trans('app.Langauge') }}</h3>
							<a class="ui grey basic label">{{App\Http\Utilities\Country::getLanguageName($user->lang)}}</a>
						</div>				
						</div>

					</div>


	 	<div class="col-lg-4  col-md-4 col-sm-12 partnerplace">

		 	<h3>Partners ({{$partners->count()}})</h3>
			@if ($partners->count() > 30)
			<h3>{{ trans('app.youarelimit') }}</h3>
			<p>{{ trans('app.over30part') }}</p>
			@endif

			@if (!$partners->count())
				<p><span class="getitalic">{{ trans('app.nopartyet') }}</span></p>

			@else
				@foreach($partners->sortByDesc("ratio") as $user2)
					@include("user/partials/userblockfreinds")
				@endforeach
			@endif
	
			<hr>
		@if (!$requests->count())
			<p><span class="getitalic"> {{ trans('app.noreq') }} <a href="{{route("partners.explore")}}">{{ trans('app.here') }}</a></span></p>

		@else
		<h3>{{ trans('app.newreq') }}</h3>
			@foreach($requests as $user2)
				@include("user/partials/userblock")
			@endforeach
		@endif
	<hr>
	 	<h3>{{ trans('app.platforms') }}</h3>
	 		@if (isset($user->BlogUrl))
	 		<a class="ui label"  href="{{$user->BlogUrl}}"><i class="fa fa-home" aria-hidden="true"></i> Website</a>
	
			@endif

			@if (isset($user->vineUrl))
			<a class="ui label"  href="{{$user->vineUrl}}"><i class="fa fa-twitch" aria-hidden="true"></i> Twitch</a>
			@endif
			@if (isset($user->pinterestUrl))
	
	
			<a class="ui label"  href="{{$user->pinterestUrl}}"><i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest</a>

			@endif
				@if (isset($user->facebookUrl))		
		<a class="ui label"  href="{{$user->facebookUrl}}"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>
			@endif
				@if (isset($user->instagramUrl))
				<a class="ui label"  href="{{$user->instagramUrl}}"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>		
			@endif
				@if (isset($user->twitterUrl))
				<a class="ui label"  href="{{$user->twitterUrl}}"><i class="fa fa-twitter" aria-hidden="true"></i> twitter</a>			
			@endif
				@if (isset($user->YoutubeUrl))
				<a class="ui label"  href="{{$user->YoutubeUrl}}"><i class="fa fa-youtube" aria-hidden="true"></i> Youtube</a>					
			@endif
				@if (isset($user->tumblrUrl))
				<a class="ui label"  href="{{$user->tumblrUrl}}"><i class="fa fa-tumblr" aria-hidden="true"></i> Tumblr</a>				
			@endif
			<hr>
			<p><span class="getitalic"> {{ trans('app.Updateplatforms') }} <a href="{{route("user.edit")}}">{{ trans('app.here') }}</a></span></p>
	 	</div>

	
	 </div>
	<span class ="helperrr" onClick="RunTourHere(1);" data-position="top left" data-tooltip="{{ trans('app.tourmyprofile') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>
	 <script>
	 document.addEventListener("DOMContentLoaded", function() {
		 $(".platformLink").on('click', function(event){
		 	event.preventDefault();
		    var url = $(this).attr("href");
	
		    if(url.startsWith("www")){
		    	url = 'http://' + url;
		    	window.open(url,'_blank');
		    } else{
		    	window.open(url,'_blank');
		    }
		});	
		var divList = $(".listing-item");
		divList.sort(function(a, b){ return $(a).data("listing-price")-$(b).data("listing-price")});

		$("#list").html(divList);
   	});
	 </script>
@stop