@extends('layout')
@section('title')

   {{$user1->Getname()}} on Fawour
@stop

@section('description')
    {{$user1->description}}
@stop
@section('Keywords')
    User profile,  {{$user1->Getname()}}, creator, blogger, Youtuber
@stop
@section('Image')
    https://www.fawour.com{{ $user1->getpofilepic120() }}
@stop
@section('content')
@if ($signedIn)
	 @include('pages.partials.aprtnersmenu')
	 @endif
	<style>
	.statsstyle div{
		text-align: center;
	}
	.partnerplace, .platformplace, .getWhite{background-color:#ffffff;}

	.platformEntry{
		display:inline-block;
		margin-right:5px;
	}
	.partnerplace, .platformplace{
		float:right;
	}
	.freindzone{
		padding-top:10px;
	}
	.hide, .hide2{
		display:none;
	}
	.btn-primary6{
		background-color: #b9b9b9 !important;
		color:#ffffff;
 	}
 	.fakebuttonremove{
 		margin-bottom:10px;
 	}
 	 .image.label.large{
	 	margin-bottom:5px;
	 	margin-right:41%;

	 }
	</style>
<?php
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url) and $url != '') {
        $url = "http://" . $url;
    }
    return $url;
}
$user1->BlogUrl = addhttp($user1->BlogUrl);
$user1->vineUrl = addhttp($user1->vineUrl);
$user1->pinterestUr = addhttp($user1->pinterestUr);
$user1->facebookUrl = addhttp($user1->facebookUrl);
$user1->instagramUrl = addhttp($user1->instagramUrl);
$user1->twitterUrl = addhttp($user1->twitterUrl);
$user1->tumblrUrl = addhttp($user1->tumblrUrl);
$user1->YoutubeUrl = addhttp($user1->YoutubeUrl);
?>
	 <div class="row">
	 	<div class="col-lg-8 col-md-8 col-sm-12 getWhite">
				<div class="row frontprofile getWhite">
				<div class="col-lg-8 col-md-8 col-sm-8">

					<h1>{{$user1->Getname()}}</h1>

					<p><span class="getitalic smalldetail">{{$user1->description}}</span></p>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">

					<h1>
					@if($user1->isOnline())
			    		<div class="userIsOnline userIsOnlineProfile"></div>
					@endif
					<div class="circle_the_text"><img class="media-object circle_the_text" alt="" class="" src="{{ $user1->getpofilepic120() }}"></div></h1>
				</div>

				

			</div>
<hr>
			@if ($signedIn)
	 		<div class="row statsstyle getWhite">
					<div class="col-lg-3 col-md-6 col-sm-6">
					<h1>{{$user1->pointsGiven}}</h1>
						<p>{{ trans('app.Visitorsgiven') }}</p>

					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<h1>{{$user1->pointsGotten}}</h1>
						<p>{{ trans('app.Visitorsgotten') }}</p>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
							<h1>{{$user1->ratio * 100}}<i class="twa twa-{{$user::returnSmiley($user1->ratio)}}"></i></h1>
							<p>{{ trans('app.givegetratio') }}</p>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
							<h1><a href="../partner/{{$user1->name}}/links">{{$partnerLinks}}</a></h1>
							<p>{{ trans('app.linkscreated') }}</p>
					</div>
					
					</div>
					<hr>
				@endif
	
	 				<div class="row getWhite">
							<div class="col-lg-3 col-md-6 col-sm-6">
							<h3>{{ trans('app.field') }}</h3>
							<a class="ui basic {{ App\User::returnColor($user1->field) }} label">{{App\Http\Utilities\Country::getFieldName($user1->field)}}</a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<h3>{{ trans('app.Langauge') }}</h3>
								<a class="ui grey basic label">{{App\Http\Utilities\Country::getLanguageName($user1->lang)}}</a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<h3>Last active</h3>
								<div class="ui grey basic label">{{ Carbon\Carbon::parse($user1->last_login)->diffForHumans() }}</div>
						</div>
					</div>


		
	 	</div>

	 	<div class="col-lg-4  col-md-4 col-sm-12 partnerplace">
	 	@if ($signedIn)
	 	<div class="freindzone">
			@if (Auth::user()->hasPartnerRequestspending($user1))
				<p>{{ trans('app.waitingfor') }} {{ $user1->getName()}} {{ trans('app.toaccepteq') }}</p>
			@elseif (Auth::user()->hasPartnerRequestReceived($user1))
			<p>{{ $user1->getName()}} {{ trans('app.wantstopartnerup') }} {{ $user->getName()}}?</p>
				@if(Auth::user()->field == $user1->field)
				<a href="{{route('partner.accept',['name' => $user1->name]) }}" class="btn btn-primary1"><i class="fa fa-user-plus" aria-hidden="true"></i> {{ trans('app.Accept') }}</a>
				<a href="{{route('partner.decline',['name' => $user1->name]) }}" class="btn btn-default"><i class="fa fa-user-times" aria-hidden="true"></i> {{ trans('app.Decline') }}</a>
				@else
				<a href="#/" class="btn btn-primary1 notsamefield2"><i class="fa fa-user-plus" aria-hidden="true"></i> {{ trans('app.Accept') }}</a>
				<a href="{{route('partner.accept',['name' => $user1->name]) }}" class="btn btn-primary1 hide "><p class="acceptpartner"></p></a>
				<a href="{{route('partner.decline',['name' => $user1->name]) }}" class="btn btn-default declinepartner"><i class="fa fa-user-times declinepartner" aria-hidden="true"></i> {{ trans('app.Decline') }}</a>
				@endif

			@elseif (Auth::user()->isPartnerWith($user1))
				<p>{{ trans('app.Youand') }} {{$user1->getName()}} {{ trans('app.arepartners') }}</p>
				<form action="{{ route('partner.delete',['name' => $user1->name])}}" method="POST">
					<button type="submit" value="Delete partner " class="btn btn-default hide2 truebuttonremove">
					</button>
					<a href="#/" class="btn btn-default fakebuttonremove"><i class="fa fa-user-times" aria-hidden="true"></i> {{ trans('app.removepartner') }}</a>
					{{ csrf_field() }}
				</form>
			@else
				@if(Auth::user()->field == $user1->field)
					<a href="{{route('partner.add',['name' => $user1->name]) }}" class="btn btn-primary1"><i class="fa fa-user-plus" aria-hidden="true"></i> {{ trans('app.addpartner') }}</a>
					@else
					<a href="#/" class="btn btn-primary1 notsamefield"><i class="fa fa-user-plus" aria-hidden="true"></i> {{ trans('app.addpartner') }}</a>
					<a href="{{route('partner.add',['name' => $user1->name]) }}" class="hide "><p class="butclickthis"></p></a>
				@endif
			@endif
			<a href="/messages?from={{ $user1->id}}" class="btn btn-primary6"><i class="fa fa-envelope" aria-hidden="true"></i> {{ trans('app.message') }}</a>
		</div>
			<h3>{{ $user1->getName()}}'s {{ trans('app.partnersss') }} ({{$user1->partners()->count()}})</h3>

			@if (!$user1->partners()->count())
				<p>{{ trans('app.Nopartnerye') }}</p>

			@else
				@foreach($user1->partners()->sortByDesc("ratio") as $user2)
					@include("user/partials/userblock")
				@endforeach
			@endif
			<hr>
			<h3>You and {{$user1->name}} </h3>
			You gave {{$user1->name}} <div class="ui label">{{$dataYougive}}</div> visitors, while {{$user1->name}} gave you <div class="ui label">{{$dataYougot}}</div>

			<hr>
		@endif

				 	<h3>{{ trans('app.platforms') }}</h3>
	 		 			 		@if (isset($user1->BlogUrl))
	 		<a class="ui label" target="_blank"  href="{{$user1->BlogUrl}}"><i class="fa fa-home" aria-hidden="true"></i> Website</a>
	
			@endif

			@if (isset($user1->vineUrl))
			<a class="ui label" target="_blank"  href="{{$user1->vineUrl}}"><i class="fa fa-twitch" aria-hidden="true"></i> Twitch</a>
			@endif
			@if (isset($user1->pinterestUrl))
	
	
			<a class="ui label" target="_blank"  href="{{$user1->pinterestUrl}}"><i class="fa fa-pinterest" aria-hidden="true"></i> Pinterest</a>

			@endif
				@if (isset($user1->facebookUrl))		
		<a class="ui label" target="_blank" href="{{$user1->facebookUrl}}"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>
			@endif
				@if (isset($user1->instagramUrl))
				<a class="ui label"  target="_blank" href="{{$user1->instagramUrl}}"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>		
			@endif
				@if (isset($user1->twitterUrl))
				<a class="ui label" target="_blank"  href="{{$user1->twitterUrl}}"><i class="fa fa-twitter" aria-hidden="true"></i> twitter</a>			
			@endif
				@if (isset($user1->YoutubeUrl))
				<a class="ui label" target="_blank" href="{{$user1->YoutubeUrl}}"><i class="fa fa-youtube" aria-hidden="true"></i> Youtube</a>					
			@endif
				@if (isset($user1->tumblrUrl))
				<a class="ui label" target="_blank" href="{{$user1->tumblrUrl}}"><i class="fa fa-tumblr" aria-hidden="true"></i> Tumblr</a>				
			@endif
	 		</div>
	 	</div>
	 	@if (!$signedIn)
	 	<style>
	 		body{
	 			background-color:#ffffff !important;
		 		}
		 	.contentholder{
		 		margin:0 auto;

		 	}
		 	.partnerplace, .platformplace{
		 		float:left !important;
		 	}
	 	</style>
	 	@endif
 	 <script>
		 document.addEventListener("DOMContentLoaded", function() {
			 $(".platformLink").on('click', function(event){
			 	event.preventDefault();
			    var url = $(this).attr("href");
		
			    if(url.startsWith("www")){
			    	url = 'http://' + url;
			    	window.open(url,'_blank');
			    } else{
			    	window.open(url,'_blank');
			    }
			});	
			 $(".notsamefield").on('click', function(event){
			 swal({   title: "Are you sure?",   text: "You are not in the same field with this partner. This seems like a bad idea.",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#19bd9a",   confirmButtonText: "Add partner",   cancelButtonText: "Cancel",   closeOnConfirm: false,   closeOnCancel: false }, 
			 	function(isConfirm){   if (isConfirm) {     
			 		$(".butclickthis").click();
			 		} else {     
			 		swal("Cancelled", "", "error");   } 
			 	});
			 });	
 			 $(".notsamefield2").on('click', function(event){
			 swal({   title: "Are you sure?",   text: "You are not in the same field with this partner. This seems like a bad idea.",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#19bd9a",   confirmButtonText: "Accept partner",   cancelButtonText: "Decline partner",   closeOnConfirm: false,   closeOnCancel: false }, 
			 	function(isConfirm){   if (isConfirm) {     
			 		$(".acceptpartner").click();
			 		} else {     
			 		$(".declinepartner").click();   } });
			 });	
 			 // Delete partner
		 	 $(".fakebuttonremove").on('click', function(event){
			 swal({   title: "Are you sure?",   text: "You are about to remove this partner.",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#19bd9a",   confirmButtonText: "Remove partner",   cancelButtonText: "Keep partnership",   closeOnConfirm: false,   closeOnCancel: false }, 
			 	function(isConfirm){   if (isConfirm) {     
			 		$(".truebuttonremove").click();
			 		} else {     
			 		swal("Partnership saved", "", "info");  
			 	} });
			 });	
	   	});
 	</script>
@stop