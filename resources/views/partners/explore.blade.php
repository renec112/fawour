@extends('layout')
<style>
    .userresults{
        background-color:#ffffff;
        padding:10px;
        min-height: 165px !important;
    }
    .userresultsimage{
        width:80px;
    }
    .spanclass{
        color:#19bd9a;
    }
    .advancedform{
        margin:2px 0;
    }
    .navbar-form{
        padding:0px !important;
    }
</style>
@section('content')
   @include('pages.partials.aprtnersmenu')
    <div class="row">
    <div class="col-lg-8 getWhite">
    <h2>{{ trans('app.settings') }}</h2>
    <form class="ui form" role="search" action"{{route('partners.explore')}}">
        <div class="form-group">

                    
            <div class="three fields">
                <div class="field">
            <label>{{ trans('app.field') }}</label>
           	<select  name="field" class="ui fluid dropdown" id="field" onchange="validate()">
            @foreach (App\Http\Utilities\Country::allfield() as $name => $code)
            	@if (Request::input("field") == $code)
				<option value="{{ $code }}" selected>{{ $name }}</option>
            	@else
					<option value="{{ $code }}">{{ $name }}</option>
            	@endif
           	
           	@endforeach
           		@if (Request::input("field") == "any")
					<option value="any" selected>{{ trans('app.anything') }}</option>
                @else
                    <option value="any">{{ trans('app.anything') }}</option>
            	@endif
			</select>
            </div>
          
            <div class="field">
         <label>Last online</label>
         <select name="online" class="ui fluid dropdown" id="online"  onchange="validate()">
        <option value="14"  selected>2 weeks ago</option>
            @if (Request::input("online") == "1000")<option value="1000" selected>Any day</option>@else<option value="1000" >Any day</option>@endif
            @if (Request::input("online") == "2")<option value="2" selected >2 days ago</option>@else<option value="2" >2 days ago</option>@endif
            @if (Request::input("online") == "4")<option value="4" selected>4 days ago</option>@else<option value="4" >4 days ago</option>@endif
            @if (Request::input("online") == "7")<option value="7" selected>A week ago</option>@else<option value="7" >A week ago</option>@endif
            @if (Request::input("online") == "14")<option value="14" selected>2 weeks ago</option>@else<option value="14" >2 weeks ago</option>@endif
            @if (Request::input("online") == "30")<option value="30" selected>A month ago</option>@else<option value="30" >A month ago</option>@endif
        </select>
        </div>
            <div class="field">
                 <label>{{ trans('app.platforms') }}</label>
			<select name="medium" class="ui fluid dropdown" id="medium"  onchange="validate()">
			     @if (Request::input("medium") == "facebookUrl")
					<option value="facebookUrl" selected>Facebook</option>
            	@else
					<option value="facebookUrl">Facebook</option>
            	@endif
				
            		@if (Request::input("medium") == "BlogUrl")
					<option value="BlogUrl" selected>Website/Blog</option>
            	@else
					<option value="BlogUrl">Website/Blog</option>
            	@endif
            		

            		@if (Request::input("medium") == "twitterUrl")
					<option value="twitterUrl" selected>Twitter</option>
            	@else
					<option value="twitterUrl">Twitter</option>
            	@endif

            		@if (Request::input("medium") == "YoutubeUrl")
					<option value="YoutubeUrl" selected>Youtube</option>
            	@else
					<option value="YoutubeUrl">Youtube</option>
            	@endif

            		@if (Request::input("medium") == "hasInstagram")
					<option value="instagramUrl" selected>Instagram</option>
            	@else
					<option value="instagramUrl">Instagram</option>
            	@endif

            		@if (Request::input("medium") == "vineUrl")
					<option value="vineUrl" selected>Vine</option>
            	@else
					<option value="vineUrl">Vine</option>
            	@endif

            		@if (Request::input("medium") == "pinterestUrl")
					<option value="pinterestUrl" selected>Pinterest</option>
            	@else
					<option value="pinterestUrl">Pinterest</option>
            	@endif

            		@if (Request::input("medium") == "tumblrUrl")
					<option value="tumblrUrl" selected>Tumblr</option>
            	@else
					<option value="tumblrUrl">Tumblr</option>
            	@endif
                @if (Request::input("medium") == "any")
                    <option value="any" selected>{{ trans('app.anything') }}</option>
                @elseif(Request::input("medium") == null)
                    <option value="any" selected>{{ trans('app.anything') }}</option>
                @else
                    <option value="any">{{ trans('app.anything') }}</option>
                @endif
            </select>
            </div>
            </div>

        </div>
        @if(Request::input("given") == null && Request::input("online") == null)
        <style>    .advancedform{
        display:none;
     }</style>
        @else
        <style>
        .advancedsearch{
        display:none;
        }
        </style>
        @endif
        <div>
  
        
        </div>
        <div class="advancedform">
        <div class="three fields">
        <div class="field">
        <label>{{ trans('app.Visitorsgiven') }}</label>
        <select name="given" class="ui fluid dropdown" id="given"   onchange="validate()">
            <option value="0"  selected>{{ trans('app.Visitorsgiven') }}</option>
             @if (Request::input("given") == "0")<option value="0" selected>0 {{ trans('app.ormore') }}</option>@else<option value="0" >0 {{ trans('app.ormore') }}</option>@endif
             @if (Request::input("given") == "25")<option value="25" selected>25 {{ trans('app.ormore') }}</option>@else<option value="25" >25 {{ trans('app.ormore') }}</option>@endif
             @if (Request::input("given") == "50")<option value="50" selected>50 {{ trans('app.ormore') }}</option>@else<option value="50" >50 {{ trans('app.ormore') }}</option>@endif
             @if (Request::input("given") == "100")<option value="100" selected>100 {{ trans('app.ormore') }}</option>@else<option value="100" >100 {{ trans('app.ormore') }}</option>@endif
             @if (Request::input("given") == "250")<option value="250" selected>250 {{ trans('app.ormore') }}</option>@else<option value="250" >250 {{ trans('app.ormore') }}</option>@endif
             @if (Request::input("given") == "500")<option value="500" selected>500 {{ trans('app.ormore') }}</option>@else<option value="500" >500 {{ trans('app.ormore') }}</option>@endif
             @if (Request::input("given") == "1000")<option value="1000" selected>1000 {{ trans('app.ormore') }}</option>@else<option value="1000" >1000 {{ trans('app.ormore') }}</option>@endif
        </select>
        </div>
          <div class="field">
            <label>{{ trans('app.Langauge') }}</label>
            <select  name="lang" class="ui fluid dropdown" id="lang"  onchange="validate()">
            @foreach (App\Http\Utilities\Country::alllang() as $name => $code)
                    @if (Request::input("lang") == $code)
                <option value="{{ $code }}" selected>{{ $name }}</option>
                @else
                    <option value="{{ $code }}">{{ $name }}</option>
                @endif
            @endforeach
                @if (Request::input("lang") == "any")
                    <option value="any" selected>{{ trans('app.anything') }}</option>
                @else
                    <option value="any">{{ trans('app.anything') }}</option>
                @endif
            
            
            </select>
            </div>
        
        </div>
        </div>
        <a class="advancedsearch" href="#/"><span class="ui grey basic button">{{ trans('app.advancedse') }}</span></a><button type="submit" class="ui button">{{ trans('app.Search') }} <i class="fa fa-search" aria-hidden="true"></i> </button>
    
    </form>
    </div>
    <div class="col-lg-4 getWhite">
     <h2>{{ trans('app.query') }}</h2>
           <span>
                <p id="helpBlockloength">{{ trans('app.showuserswith') }} <span id="fieldishere" class="spanclass"></span> {{ trans('app.contentlangis') }} <span id="langishere" class="spanclass"></span> {{ trans('app.whoison') }} <span id="platformishere" class="spanclass"></span><span class="advancedform">
                {{ trans('app.givenmorethan') }} <span id="visitorsgiven" class="spanclass"> </span> {{ trans('app.anndhwolastolnie') }}<span id="lastonline" class="spanclass"></span>
                </span></p>
            </span>
        <script>
        function validate() {
            var valuefield = document.getElementById('field').options[document.getElementById('field').selectedIndex].text;
            document.getElementById("fieldishere").innerHTML = valuefield;

             var valuefield = document.getElementById('lang').options[document.getElementById('lang').selectedIndex].text;
            document.getElementById("langishere").innerHTML = valuefield;

            var valuefield = document.getElementById('medium').options[document.getElementById('medium').selectedIndex].text;
            document.getElementById("platformishere").innerHTML = valuefield;

            var valuefield = document.getElementById('online').options[document.getElementById('online').selectedIndex].text;
            
               if (valuefield == "Was online"){
                valuefield = "Any day";
            }
            document.getElementById("lastonline").innerHTML = valuefield;
            var valuefield = document.getElementById('given').options[document.getElementById('given').selectedIndex].value;

            document.getElementById("visitorsgiven").innerHTML = valuefield;

        }
        validate();
        </script>
        </div></div>

    @if(!isset($userslisted))
    
    @elseif (!$userslisted->count())
        <p>{{ trans('app.nores') }}</p>
    @elseif (empty(Request::input("field")))
    @else
      <h2>{{ trans('app.userswhomeet') }}</h2>
    <div class="ui six doubling cards">
        @foreach ($userslisted as $user1)
            @if($user1->verified == 1)
            @include("user/partials/userblocksearch")
            @endif
        @endforeach
    		
    	</div>
      {!! $userslisted->appends(['field' => Request::input("field"), 'lang' => Request::input("lang"), 'medium' => Request::input("medium"), 'given' => Request::input("given"), 'online' => Request::input("online")])->render() !!}


    @endif
      <span class ="helperrr" onClick="RunTourHere(5);" data-tooltip="{{ trans('app.tourexplore') }}" id="tour5" ><i class="fa fa-question-circle-o"></i></span>
   @if(Request::input("field") == null)
        <script>
        var dataforexplore1 = "<?php echo $user->field; ?>"; //Don't forget the extra semicolon!
        var element1 = document.getElementById('field');
        element1.value = dataforexplore1;
        validate();

        var dataforexplore2 = "<?php echo $user->lang; ?>"; //Don't forget the extra semicolon!
        var element2 = document.getElementById('lang');
        element2.value = dataforexplore2;
        </script>
    @endif
    <style>
@media only screen and (max-width: 1400px) {

     .userresults{
        min-height: 220px !important;
    }
  
}
@media only screen and (max-width: 1000px) {

     .userresults{
        min-height: 260px !important;
    }
  
}
</style>
    <script>
            document.addEventListener("DOMContentLoaded", function() {

                $(".advancedsearch").on('click', function(event){
                    $(".advancedsearch").hide();
                    $(".advancedform").show();
                 });
        

            });
        </script>
@stop