@extends('layout')
<style>
    .userresults{
        background-color:#ffffff;
        padding:10px;
        min-height: 165px;
    }
        .userresultsimage{
        width:80px;

    }
</style>
@section('content')
   @include('pages.partials.aprtnersmenu')
    <form class="navbar-form" role="search" action"{{route('search')}}">
        <div class="form-group">
            <input type="text" name="query" class="form-control" placeholder="Search partner by name">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>

  
    @if (!$userslisted->count())
        <p>No results found.</p>
    @elseif (empty(Request::input("query")))
    @else
      <h2>Results for {{Request::input("query")}}</h2>
    <div class="ui six doubling cards">
        @foreach ($userslisted as $user1)
            @if($user1->verified == 1)
            @include("user/partials/userblocksearch")
            @endif
        @endforeach
        </div>

        {!! $userslisted->appends(['query' => Request::input("query")])->render() !!}

    @endif
@stop