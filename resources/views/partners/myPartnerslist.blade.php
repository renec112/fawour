@extends('layout')

@section('content')
	@include('pages.partials.aprtnersmenu')

		 	<h1>Partners ({{$partners->count()}})</h1>
			@if ($partners->count() > 30)
			<h3>{{ trans('app.youarelimit') }}</h3>
			<p>{{ trans('app.over30part') }}</p>
			@endif

			@if (!$partners->count())
				<p><span class="getitalic">{{ trans('app.nopartyet') }}</span></p>

			@else
			<div class="ui six doubling cards">
				@foreach($partners->sortByDesc("ratio") as $user2)
				  <div class="ui card">
					  <div class="image">
					    <img src="{{ $user2->getpofilepic120() }}">
					  </div>
					  <div class="content">
					    <a class="header" href="{{ route('partner.profile', ['name' => $user2->name])}}">{{ $user2->name }}</a>
					    <div class="meta">
					      <span class="date">Given: {{ $user2->pointsGiven }}, Gotten: {{ $user2->pointsGotten }}</span>
					    </div>
					    <div class="description">
					      {{ str_limit($user2->description, $limit = 70, $end = '...') }}
					    </div>
					  </div>
					  <div class="extra content">
					    <a>
					   
					      <a class="ui basic {{ $user2->returnColor($user2->field) }} label">{{App\Http\Utilities\Country::getFieldName($user2->field)}}</a>
					      	<a class="ui grey basic label">{{App\Http\Utilities\Country::getLanguageName($user2->lang)}}</a>
					    </a>
					  </div>
					</div>
				@endforeach
			</div>
			@endif

@stop