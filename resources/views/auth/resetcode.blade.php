@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h1 class="text-center">Reset code</h1>
         @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <form class="form-horizontal" role="form" method="POST" action="{{ url('register/resetemail') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="">E-Mail Address</label>

                <div class="">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-primary1 btn-block">
                        <i class=""></i>reset code
                    </button>
                </div>
                <div class="col-md-12 text-center">
                    <i class="">Remember to check spam folder.</i>
                </div>
            </div>
        </form>
</div>
@endsection
