@extends('layout')
@section('title')
    Login on Fawour to find and grow with creators
@stop

@section('description')
    Here you can login and access the tool
@stop
@section('Keywords')
    Login, connect, join, become memember, membership, share, youtubers
@stop
@section('content')
<style>
    .form-control{
        border:none !important;
    }
    .alert-info{
        background-color: #19bd9a !important;
        border-color: #19bd9a !important;
        color:#ffffff !important;
    }   
    .marginthishere{
        margin-top:5px !important;
    }
</style> 
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h1 class="text-center">Login</h1>

        <form class="ui form" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}

            <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="">{{ trans('app.Email') }}</label>

                <div class="">
                    <input type="email"  name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="field {{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="">{{ trans('app.Password') }}</label>

                <div class="">
                    <input type="password"  name="password">


                @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="ui toggle  checkbox">
                <input type="checkbox" checked tabindex="1" name="remember" class="hidden"> 
                    <label>
                    {{ trans('app.rememberbe') }} 
                        
                    </label>
                </div>
            </div>

                    <button type="submit" class="ui button">
                        <i class="fa fa-btn fa-sign-in"></i> Login
                    </button>
                    <br> <br>
                <div class="ui labeled button " tabindex="0">
                   <a class="" href="{{ url('/password/reset') }}"><div class="ui standard button">
                   {{ trans('app.forgotpass') }}
                  </div></a>
                  <a class="" href="{{ url('register/resetemail') }}"><div class="ui standard button">
                   {{ trans('app.confrim') }}
                  </div></a>
                </div>      
        </form>
<h1 class="text-center">Social login</h1>
 <a href="../auth/facebook"><div class="ui labeled button marginthishere" tabindex="0">
                  <div class="ui blue button">
                     Facebook
                  </div>
                  <div class="ui basic blue left pointing label">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </div>
                </div>
                </a>
                <br>
                 <a href="../auth/twitter"><div class="ui labeled button marginthishere" tabindex="0">
                  <div class="ui teal button">
                     Twitter
                  </div>
                  <div class="ui basic teal left pointing label">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </div>
                </div>
                </a>
                <br>

                <a href="../auth/google"><div class="ui labeled button marginthishere" tabindex="0">
                  <div class="ui red button">
                     Google
                  </div>
                  <div class="ui basic red left pointing label">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                  </div>
                </div>
                </a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {

    $('.ui.checkbox')
  .checkbox()
;
});
</script>
@endsection
