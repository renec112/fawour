@extends('layout')
@section('content')
<style>
.event .label{
  padding:0.5em 0 0 0  !important;
  margin-bottom:15px;
}
.hasReadThis{
  opacity:0.5;
}
.fa{
  color:#383838;
    font-size: 3em;
}
</style>
<div class="smallmenudiv"><h1 id="overskrift1">{{ trans('app.welcome') }} {{$user->name}}</h1></div>
@if ( $user->pointsGiven != 0 && $user->partners()->count() && $user->flyers()->count() && Auth::user()->description != null)
<script type="text/javascript">

var dates = [
@foreach ($data as $data1)
     "{{ $data1->created_at->format('m-d') }}", 
@endforeach
];

var gotten = [
@foreach ($data as $data1)
     "{{ $data1->GottenThisday }}", 
@endforeach
];
var given = [
@foreach ($data as $data1)
     "{{ $data1->GivenThisDay }}", 
@endforeach
];
while (dates.length < 7) {
    dates.push("{{ trans('app.nodata') }}");
}
while (gotten.length < 7) {
    gotten.push("0");
}
while (given.length < 7) {
    given.push("0");
}
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js"></script>
  <div class="row">
   <div class="col-xs-12 col-sm-12 analytics">

  <h2>{{ trans('app.latest7') }}</h2>
  <canvas id="myChart" height="100"></canvas>

  <script>
var canvas = document.getElementById('myChart');
var color1 = "rgba(137, 160, 176, 1)";
var data = {
    labels: dates,
    datasets: [
        {
            label: "Visitors",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(25,189,154,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(25,189,154,1)",
            pointHoverBorderColor: "rgba(25,189,154,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            data: gotten,
        }, 
          
    ]
};

var option = {
  maintainAspectRatio: true,
  responsive: true,
  showLines: true,
    scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }],
            scales: {
            xAxes: [{
                ticks: {
                    display: false
                }
            }]
        }
        }

};
var myLineChart = Chart.Line(canvas,{
  data:data,
  options:option
});

  </script>
  </div>
 <div class="ui stackable two column very relaxed grid container" style="position:relative;">
    <div class="column">
            <h2 class="getMargin"><a href="/profile/notifications">{{ trans('app.latestactivity') }}</a></h2>
        <div class="ui feed">
@foreach ($activity as $event)
    <div class="event">
    @include("pages.partials.2.{$event->type}")
    </div>
@endforeach

</div>
    </div>

    <div class="column">
      <h2><a href="/chat">{{ trans('app.latestchats') }}</a></h2>
           <div class="ui comments">
        @foreach ($chat as $chatmessage)

          <div class="comment">
            <a class="avatar"> @if($chatmessage->user->isOnline())
              <div class="userIsOnline userIsOnlinefront"></div>
          @endif
              <img src="{{ $chatmessage->user->getpofilepic35()}}">
            </a>
            <div class="content">
              <a class="author" href="partner/{{ $chatmessage->user->name}}">{{ $chatmessage->user->name}}</a><i class="twa  twa-{{$chatmessage->user->getSmileyAttribute()}}"></i></a>
              <div class="metadata">
                <div class="date">{{ $chatmessage->humanCreatedAt}}</div>
                <div class="rating">
                  <i class=""></i>
              @if($chatmessage->forum == "ev")
              <span class="getitalic">{{ trans('app.anything') }}</span>
              @else 
              <span class="getitalic">{{App\Http\Utilities\Country::getFieldName($chatmessage->forum)}}</span>
              @endif
                </div>
              </div>
              <div class="text">
               <p>{{ $chatmessage->body}}</p>
              </div>
            </div>
          </div>
        @endforeach
       </div>
    </div>
  </div>   
       </div>
        @else
  <h2 class="text2">{{ trans('app.upandrun') }}</h2>
  <div class="ui steps ui fluid vertical ">
    <!-- new to do instance -->
      @if (Auth::user()->description == null)
      <a class="active step" href="{{route("user.edit")}}">
      @else
      <a class="disabled step" >
      @endif
       <i class="fa fa-pencil" aria-hidden="true"></i>
        <div class="content">
          <div class="title">{{ trans('app.description') }}</div>
          <div class="description">{{ trans('app.addinfo') }}</div>
        </div>
      </a>

            <!-- new to do instance -->
      @if (!$user->flyers()->count())
      <a class="active step" href="{{route("create_url")}}">
      @else
       <a class="disabled step" > 
      @endif
      <i class="fa fa-share-alt" aria-hidden="true"></i>
        <div class="content">
          <div class="title">Links</div>
          <div class="description">{{ trans('app.submitsomelinks') }}</div>
        </div>
      </a>

        <!-- new to do instance -->
      @if (!$user->partners()->count())
      <a class="active step" href="{{route("partners.explore")}}">
      @else
       <a class="disabled step" >
      @endif
      <i class="fa fa-users" aria-hidden="true"></i>
        <div class="content">
          <div class="title">{{ trans('app.partnersss') }}</div>
          <div class="description">{{ trans('app.findparnter') }}</div>
        </div>
      </a>
     <!-- new to do instance -->
      @if ($user->pointsGiven == 0)
      <a class="active step" href="/links/share">
      @else
       <a class="disabled step" >
      @endif
      <i class="fa fa-line-chart aria-hidden="true"></i>
        <div class="content">
          <div class="title">{{ trans('app.Visitorsgiven') }}</div>
          <div class="description">{{ trans('app.notshared') }}</div>
        </div>
      </a>
      </div>
    

     <style>

     .step i{
      padding-right:10px;
     }
     .ui.steps .step.active .title{
      color:#19bd9a;
     }

     </style>
      <div class="column">
      <h2><a href="/chat">{{ trans('app.latestchats') }}</a></h2>
           <div class="ui comments">
        @foreach ($chat as $chatmessage)

          <div class="comment">
            <a class="avatar"> @if($chatmessage->user->isOnline())
              <div class="userIsOnline userIsOnlinefront"></div>
          @endif
              <img src="{{ $chatmessage->user->getpofilepic35()}}">
            </a>
            <div class="content">
              <a class="author" href="partner/{{ $chatmessage->user->name}}">{{ $chatmessage->user->name}}</a><i class="twa  twa-{{$chatmessage->user->getSmileyAttribute()}}"></i></a>
              <div class="metadata">
                <div class="date">{{ $chatmessage->humanCreatedAt}}</div>
                <div class="rating">
                  <i class=""></i>
              @if($chatmessage->forum == "ev")
              <span class="getitalic">{{ trans('app.anything') }}</span>
              @else 
              <span class="getitalic">{{App\Http\Utilities\Country::getFieldName($chatmessage->forum)}}</span>
              @endif
                </div>
              </div>
              <div class="text">
               <p>{{ $chatmessage->body}}</p>
              </div>
            </div>
          </div>
        @endforeach
       </div>

    </div>
  </div>
  
    @endif
    <h1>{{ trans('app.needhelp') }}</h1>
  <div class="segment">
  <a href="https://www.facebook.com/groups/1733682206850523/"><button class="ui green basic button">{{ trans('app.joinfacebookcomm') }}</button></a><br><br>
<a href="#/"><button class="ui blue basic button" id="taketour2">{{ trans('app.websitetutoria') }}</button></a><br><br>

<a href="https://www.youtube.com/watch?v=oU4E48gWOt0"><button class="ui violet basic button">{{ trans('app.youtubehowtousefawou') }}</button></a><br><br>
<a href="https://www.youtube.com/watch?v=_RMLPWP8Uco"><button class="ui purple basic button">{{ trans('app.youtubehowfawourcangro') }}</button></a><br><br>
<a href="https://docs.google.com/forms/d/e/1FAIpQLSez07_zXZHcpJoGlLaJ_QAtXNr2gxyKks5qWHXs3NrK6s_0qQ/viewform"><button class="ui pink basic button">{{ trans('app.contactcreator') }}</button></a><br><br>
<a href="https://docs.google.com/forms/d/e/1FAIpQLSex-51Ri8JdwexaT6lbGDXFMM60lCnO7Sfppdzq92ndxZR0YA/viewform?usp=send_form"><button class="ui grey basic button">{{ trans('app.bugsfeature') }}</button></a><br><br>

</div>
          <script>
     document.getElementById('taketour2').onclick = function()
   {
        startTour();
   }
     </script>
@stop