@extends('layout')
@section('title')
    Frequently asked questions
@stop

@section('description')
    Answering frequently asked questions on how to use the site and grow
@stop
@section('Keywords')
    FAQ, How to,  Frequently asked questions, questions, cross channel collaboration
@stop
@section('content')
    <div   class="basicss entry shown">
		<div class="ui accordion">
		 <h1>{{ trans('global.Basics') }}</h1>
			  <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.countworkss') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.countworkanser') }}</p>
			  </div>

			   <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.caniremove') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.caniremoveans') }}</p>
			  </div>

			     <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.howbigisthe') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.howbigistheans') }}<a href="/stats">{{ trans('global.here') }}</a></p>
			  </div>

			     <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.howdoiuse') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.howdoiuseans') }}</p>
			  </div>

			     <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.wjhatdoesitcost') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.wjhatdoesitcostans') }}</p>
			  </div>
			  <h1>{{ trans('global.Account') }}</h1>

			      <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.howdoichange') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.howdoichangeans') }} <a href="/password/reset"> {{ trans('global.here') }}</a></p>
			  </div>

			      <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.hwopdoidelete') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.hwopdoideleteans') }}</p>
			  </div>
			
			    <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.hodoichangeset') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.hodoichangesetans') }}</p>
			  </div>
			 <h1>{{ trans('global.xxrivacy') }}</h1>
			      <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.whyneedemail') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.answhyneedemail') }}</p>
			  </div>

			      <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.cookiespriva') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.cookiesprivabsa') }}</p>
			  </div>

			      <div class="title">
			    <i class="dropdown icon"></i>
			    {{ trans('global.dateaas') }}
			  </div>
			  <div class="content">
			    <p class="transition hidden">{{ trans('global.dateaasanss') }}</p>
			  </div> 
			  <br><br>
			  <p>{{ trans('global.mis') }} <a href="https://docs.google.com/forms/d/e/1FAIpQLSez07_zXZHcpJoGlLaJ_QAtXNr2gxyKks5qWHXs3NrK6s_0qQ/viewform">{{ trans('global.here') }}</a></p>
			   <br><br>

  </div>

</div>

<script>
document.addEventListener("DOMContentLoaded", function() {
       $(document).ready(function(){
                $('.ui.accordion').accordion();
             });
});
</script>

</div>
</div>
</div>
</div>
@include("pages/partials/footer")

@stop
