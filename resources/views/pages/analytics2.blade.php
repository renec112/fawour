@extends('layout')
<style>
#myChart{
	width:90%;
	height:100px;
}
</style>
@section('content')
@include('pages.partials.menulinks')
<div class="ui two column stackable grid">
  <div class="four wide column">
    <div class="ui">@include('pages.partials.menuanalitcs')</div>
  </div>
  <div class="twelve wide column">
  <h2>{{ trans('app.visitorsgottenby') }}</h2>
    <div class="ui"><canvas id="myChart"></canvas></div>
  </div>
</div>
<script type="text/javascript">
@if ($to == false)
var namesgotten = ["No visitors gotten"];
var visitorsgotten = [1];
var count = 0;
@else

var namesgotten = [
@foreach ($to as $to1)
     "{{ $to1->name }}", 
@endforeach
];


var visitorsgotten = [
@foreach ($to as $to1)
     {{ $to1->points }}, 
@endforeach
];
// sum the vistors gotten
var count=0;
for (var i=visitorsgotten.length; i--;) {
 count+=visitorsgotten[i];
}
// check for array legth. Slice if too big
if(namesgotten.length > 8){

	namesgotten = namesgotten.slice(0, 8);
	namesgotten.push("others");

	var lengthToSlice = visitorsgotten.length - 8;
	visitorsgottensum = visitorsgotten.slice(8, lengthToSlice);
	var count2=0;
	for (var i=visitorsgottensum.length; i--;) {
 		count2+=visitorsgottensum[i];
	}
	visitorsgotten = visitorsgotten.slice(0, 8);
	visitorsgotten.push(count2);

}
@endif
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js"></script>



  <script>
var data = {
  labels: namesgotten,
  datasets: [
    {
      data: visitorsgotten,
      backgroundColor: [
        "#3e4a58",
        "#19bd9a",
        "#89a0b0",
        "#b2b3b8",
        "#3e4a58",
        "#19bd9a",
        "#89a0b0",
        "#b2b3b8",
      ],
      hoverBackgroundColor: [
        "#3e4a58",
        "#19bd9a",
        "#89a0b0",
        "#b2b3b8",
        "#3e4a58",
        "#19bd9a",
        "#89a0b0",
        "#b2b3b8",
      ]
    }]
};

var promisedDeliveryChart = new Chart(document.getElementById('myChart'), {
  type: 'doughnut',
  data: data,
  options: {
  	responsive: true,
    legend: {
      display: true
    }
  }
});
Chart.pluginService.register({
  beforeDraw: function(chart) {
    var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;

    ctx.restore();
    var fontSize = (height / 114).toFixed(2);
    ctx.font = fontSize + "em sans-serif";
    ctx.textBaseline = "middle";

    var text = count,
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = height / 1.8;

    ctx.fillText(text, textX, textY);
    ctx.save();
  }
});

  </script>
   <span class ="helperrr" onClick="RunTourHere(9);" data-tooltip="{{ trans('app.analyfawor') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>


@stop