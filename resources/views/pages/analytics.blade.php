@extends('layout')
<style>
#myChart{
	width:90%;
	height:100px;
}
</style>
@section('content')
@include('pages.partials.menulinks')
<div class="ui two column stackable grid">
  <div class="four wide column">

    <div class="ui">@include('pages.partials.menuanalitcs')</div>
  </div>
  <div class="twelve wide column">
  <h2>{{ trans('app.givenandgotten') }}</h2>
    <div class="ui"><canvas id="myChart"></canvas></div>
  </div>
</div>


<script type="text/javascript">

var dates = [
@foreach ($data as $data1)
     "{{ $data1->created_at->format('m-d') }}", 
@endforeach
];

var gotten = [
@foreach ($data as $data1)
     "{{ $data1->GottenThisday }}", 
@endforeach
];
var given = [
@foreach ($data as $data1)
     "{{ $data1->GivenThisDay }}", 
@endforeach
];
while (dates.length < 10) {
    dates.push("{{ trans('app.nodata') }}");
}
while (gotten.length < 10) {
    gotten.push("0");
}
while (given.length < 10) {
    given.push("0");
}
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js"></script>
	

  <script>
var canvas = document.getElementById('myChart');
var color1 = "rgba(137, 160, 176, 1)";
var data = {
    labels: dates,
    datasets: [
        {
            label: "Visitors gotten",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(25,189,154,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(25,189,154,1)",
            pointHoverBorderColor: "rgba(25,189,154,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            data: gotten,
        }, 
           {
            label: "Visitors given",
            fill: false,
            lineTension: 0,
            backgroundColor: color1,
            borderColor: color1,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: color1,
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: color1,
            pointHoverBorderColor: color1,
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            data: given,
        }
    ]
};

var option = {
      maintainAspectRatio: true,
  responsive: true,
	showLines: true,
    scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }

};
var myLineChart = Chart.Line(canvas,{
	data:data,
  options:option
});

  </script>
     <span class ="helperrr" data-position="top left"  onClick="RunTourHere(9);" data-tooltip="{{ trans('app.analyfawor') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>

@stop