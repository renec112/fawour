@extends('layout')

@section('content')

<h1>{{ trans('global.Hello') }} Peter</h1>
    <form action="{{URL::route('langset')}}" method="post">
    {!! csrf_field() !!}
      <select class="form-control" name="locale" onchange="this.form.submit()">
          <option value="en">English</option>
          <option value="da" {{Lang::locale () == 'da' ? ' selected' : ''}}>Dansk</option>
      </select>
    </form>
    





@stop
