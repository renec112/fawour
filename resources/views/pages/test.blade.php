@extends('layout')
@section('title')
    Register here to find and grow with creators
@stop

@section('description')
    Here you can register and access the tool
@stop
@section('Keywords')
    connect, Register, join, followers, share, cross channel collaboration
@stop
@section('content')
<style>
    .help-block strong{
        color:#a94442;
    }
    .form-control{
        border:none !important;
    }
    .ui.labeled.button{
        margin-top:10px;
    }
    .thisemailform{
        margin-top:30px;
    }
    .thisemailform{
       opacity: 0.5;
    }
</style> 
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h1 class="text-center">{{ trans('global.Signup') }}</h1>
                <a href="../auth/facebook"><div class="ui labeled button" tabindex="0">
                  <div class="ui blue button">
                     With Facebook
                  </div>
                  <div class="ui basic blue left pointing label">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </div>
                </div>
                </a>
                 <a href="../auth/twitter"><div class="ui labeled button" tabindex="0">
                  <div class="ui teal button">
                     With Twitter
                  </div>
                  <div class="ui basic teal left pointing label">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </div>
                </div>
                </a>

                <a href="../auth/google"><div class="ui labeled button" tabindex="0">
                  <div class="ui red button">
                     With google
                  </div>
                  <div class="ui basic red left pointing label">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                  </div>
                </div>
                </a>

                <a href="#/" class="signupbyemal"><div class="ui labeled button " tabindex="0">
                  <div class="ui standard button">
                     By Email
                  </div>
                  <div class="ui basic standard left pointing label">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                </div>
                </a>


        <form class="ui form thisemailform thisemailformhide" role="form" method="POST" action="{{ url('/register') }}">
            {!! csrf_field() !!}

            <div class="field {{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="">{{ trans('global.Username') }}</label>

                <input type="text"  name="name" value="{{ old('name') }}">

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="">{{ trans('global.email') }}</label>

                <input type="email" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="field {{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="">{{ trans('global.Password') }}</label>

                <input type="password"  name="password">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="field {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="">{{ trans('global.conpassword') }}</label>

                <input type="password" name="password_confirmation">

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                {!! app('captcha')->display() !!}

                @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                @endif
            </div>

     
                    <button type="submit" class="ui button">
                        <i class="fa fa-btn fa-user"></i>{{ trans('global.Register22') }}
                    </button>
           
        

        </form>

    </div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    $( ".signupbyemal, .thisemailform" ).click(function() {
      $( ".thisemailform" ).fadeTo( "slow", 1 );
    });
});
</script>
@endsection
