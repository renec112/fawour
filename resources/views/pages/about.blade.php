@extends('layout')
@section('title')
    About Fawor 
@stop

@section('description')
    About Fawour
@stop
@section('Keywords')
    About Fawour, 
@stop

@section('content')
<div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">
      <div class="row">
        <div class="eight wide column">
          <h3 class="ui header">{{ trans('global.fawhist') }}</h3>
          <p>{{ trans('global.inependentwebapp') }}
 </p>
<h3 class="ui header">René Czepluch</h3>
<p>{{ trans('global.Reneisastudent') }}</p>
<h3 class="ui header">{{ trans('global.suppthis') }}</h3>
<p>{{ trans('global.costmoney') }}
</p>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="QWF5M6LHUS6ZQ" />
<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>

        </div>
        <div class="six wide right floated column">
          <img src="img/Rene-Czepluch.jpg" class="ui large bordered rounded image">
        </div>
      </div>
      <div class="row">
        <div class="center aligned column">
        <p>{{ trans('global.questt') }}</p>
          <a class="ui huge button" href="https://docs.google.com/forms/d/e/1FAIpQLSez07_zXZHcpJoGlLaJ_QAtXNr2gxyKks5qWHXs3NrK6s_0qQ/viewform">{{ trans('global.Contactme11') }}</a>
        </div>
      </div>
    </div>
  </div>


  <div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">
      <div class="row">
        <div class="six wide column">
         <img src="img/Tyler-arbon.jpg" class="ui large bordered rounded image">
    
        </div>
        <div class="eight wide right floated column">
               <h3 class="ui header">{{ trans('global.thanksTyler') }}</h3>
          <p>
          {{ trans('global.tylerhlped') }}<a href="https://github.com/tylercd100"> {{ trans('global.checkhisproject') }}</a>
      </p>
        </div>
      </div>
    </div>
  </div>

</div></div></div>
@include("pages/partials/footer")
@stop
