@extends('layout')
@section('title')
    Fawour - {{ trans('navi.growtog') }}
@stop

@section('description')
    {{ trans('navi.getmore') }}
@stop
@section('Keywords')
    Grow, how to, examp,e
@stop

@section('content')
  <div class="ui vertical stripe segment">

    <div class="ui middle aligned stackable grid container">
      <div class="row">
        <div class="eight wide column">
          <h3 class="ui header">It's simple</h3>
          <p>Share your partners stuff whenever on whatever platform. Just copy the url-link and share it. Yan add it anywhere. To a blog-post, Facebook-post, your website, youtube annotations and so on. That's because we just use normal links.</p>

          <h3 class="ui header">It's great for your followers</h3>
          <p>Don't have anything to show your follows today? Your new partners proberbly have great and interesting stuff. Why not share the stuff with your followers? Your followers follow you because they love what you do - they'll love to know about other people that does simluar stuff like you.</p>

          <h3 class="ui header">It's your turn now</h3>
          <p>Sit back and enjoy. You helped out your partners without loosing anything. Now your partners returns the favour and share your stuff as well and provide you with relevant visitors. It's a win win situation.</p>
        </div>
        <div class="six wide right floated column">
          <img src="assets/images/wireframe/white-image.png" class="ui large bordered rounded image">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@include("pages/partials/footer")
@stop
