@extends('layout')
@section('title')
    Fawour - {{ trans('navi.growtog') }}
@stop

@section('description')
    {{ trans('navi.getmore') }}
@stop
@section('Keywords')
    channel collaboration, Grow, creators, bloggers, followers, share, youtubers
@stop

@section('content')

<style>
.containeerrr h1{
  text-align:center;
  padding:60px 0 15px 0;
}
.containeerrr img{
  width:100%;

}
.containerrightr img{
}
.centerthis{
  text-align:center !important;
}
.centerthis img{
  margin: 0 auto !important;
}
.ui.primary.button, .ui.primary.buttons .button{
  margin-top:20px;
  background-color:#19bd9a;
  margin-bottom: 50px;
}
.primary.button a{
 color:#ffffff !important;
}
.ui.primary.button:hover, .ui.primary.buttons .button:hover{
   background-color:#455161;

}
</style>


<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="containeerrr modal1 containerrightr">
      <h1> {{ trans('global.FirstSubmitLink') }} </h1>
      <img src="/img/img2.png" alt="First slide">
    </div>
    <div class="containeerrr ">
    <h1>{{ trans('global.nextPartner') }}</h1>
      <img src="/img/img3.png" alt="Second slide">
    </div>
    <div class="containeerrr ">
    <h1>{{ trans('global.lastlyboostyourpartners') }}</h1>
      <img src="/img/img1.png" alt="Third slide">
    </div>
           <div class="containeerrr ">
    <h1>{{ trans('global.TheresNeat') }}</h1>
      <img src="/img/img5.png" alt="Third slide">
    </div>
        <div class="containeerrr ">
    <h1>{{ trans('global.givenandgotten111') }}</h1>
      <img src="/img/img4.png" alt="Third slide">
    </div>

  </div>
</div>

      <div class="ui  container centerthis">
      <h1 class="ui  header ">
       {{ trans('global.tryItfree') }}
      </h1>
      <h2>{{ trans('global.madeBy') }}</h2>
      <a href="/about"><div class="ui huge grey basic  button">{{ trans('global.About22') }} </div></a>
      <a href="/register"><div class="ui huge primary button">{{ trans('global.GetStarted') }} <i class="fa fa-arrow-right" aria-hidden="true"></i></div></a>
    </div>
    </div>
</div>
</div>
</div>
@include("pages/partials/footer")
@stop
