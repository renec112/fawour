@extends('layout')
@section('title')
    Fawour - {{ trans('navi.growtog') }}
@stop

@section('description')
    {{ trans('navi.getmore') }}
@stop
@section('Keywords')  
    Grow, creators, share followers, share, cross channel collaboration
@stop

@section('content')

<style>
.centerthis{
  text-align:center !important;
}
.centerthis img{
  margin: 0 auto !important;
}
.ui.primary.button, .ui.primary.buttons .button{
  margin-top:50px;
  background-color:#19bd9a;
}
.primary.button a{
 color:#ffffff !important;
}
.ui.primary.button:hover, .ui.primary.buttons .button:hover{
   background-color:#455161;

}
.image{
  width:100%;
  height:auto;
}
.signupthis{margin-top:5px !important;}
.getpadding{padding:50px !important;}
</style>
<!-- Turn your competitors into colleagues-->
    <div class="ui  container centerthis">
      <h1 class="ui  header ">
        {{ trans('global.grow222') }}
      </h1>
      <h2>{{ trans('global.yousharetheirs') }}</h2>
<!-- get started-->
<a href="/register"><div class="ui huge primary button">{{ trans('global.GetStarted') }} <i class="fa fa-arrow-right" aria-hidden="true"></i></div></a>
<!-- Why we rock-->
  <h2 class="ui horizontal header divider getpadding">
        <a href="#">{{ trans('global.youshouldchoose') }}</a>
      </h2>
<div class="ui two column stackable grid container">
  <div class="column">
    <div class="ui ">
<div class="ui items">
  <div class="item">
    <a class="tiny image">
      <i class="fa fa-money" aria-hidden="true"></i>
    </a>
    <div class="content">
      <h2 class="header">{{ trans('global.itsfree') }}</h2>
      <div class="description">
        <p>{{ trans('global.itsfreedes') }}</p>
      </div>
    </div>
  </div>
</div>
    </div>
  </div>
  <div class="column">
    <div class="ui ">
<div class="ui items">
  <div class="item">
    <a class=" tiny image">
      <i class="fa fa-refresh" aria-hidden="true"></i>
    </a>
    <div class="content">
      <h2 class="header">{{ trans('global.Count') }}</h2>
      <div class="description">
        <p>{{ trans('global.Countdes') }}</p>
      </div>
    </div>
  </div>
</div>
    </div>
  </div>
    <div class="column">
    <div class="ui ">
<div class="ui items">
  <div class="item">
    <a class="tiny image">
      <i class="fa fa-check" aria-hidden="true"></i>
    </a>
    <div class="content">
      <h2 class="header">{{ trans('global.easy') }}</h2>
      <div class="description">
        <p>{{ trans('global.easydes') }}</p>
      </div>
    </div>
  </div>
</div>
    </div>
  </div>
    <div class="column">
    <div class="ui ">
<div class="ui items">
  <div class="item">
    <a class="tiny image">
      <i class="fa fa-user" aria-hidden="true"></i>
    </a>
    <div class="content">
      <h2 class="header">{{ trans('global.Independent') }}</h2>
      <div class="description">
        <p>{{ trans('global.Independentdes') }}</p>
      </div>
    </div>
  </div>
</div>


    </div>
  </div>

    <div class="column">
    <div class="ui ">
<div class="ui items">
  <div class="item">
    <a class=" tiny image">
      <i class="fa fa-users" aria-hidden="true"></i>
    </a>
    <div class="content">
      <h2 class="header">{{ trans('global.Cumminity') }}</h2>
      <div class="description">
        <p>{{ trans('global.Cumminitydes') }}</p>
      </div>
    </div>
  </div>
</div>


    </div>
  </div>

    <div class="column">
    <div class="ui ">
<div class="ui items">
  <div class="item">
    <a class=" tiny image">
      <i class="fa fa-bar-chart" aria-hidden="true"></i>
    </a>
    <div class="content">
      <h2 class="header">{{ trans('global.Feature') }}</h2>
      <div class="description">
        <p>{{ trans('global.Featuredes') }}</p>
      </div>
    </div>
  </div>
</div>


    </div>
  </div>
</div>

<div class="ui vertical stripe">
      <h2 class="ui horizontal header divider">
        <a href="#">{{ trans('global.howitworks') }}</a>
      </h2>
<div class="ui three column stackable grid centerthis">
  <div class="column">
    <div class="ui ">
        <img class="ui small circular image" src="img/flyers/work-together.jpg">
        <h3 class="ui ">1. {{ trans('global.section1') }}</h3>
    </div>
  </div>
  <div class="column">
    <div class="ui ">
    <img class="ui small circular image" src="img/flyers/share-links.jpg">
      <h3 class="ui ">2. {{ trans('global.section2') }}</h3>
      
    </div>
  </div>
  <div class="column">
    <div class="ui ">
    <img class="ui small circular image" src="img/flyers/increase-visitors.jpg">
      <h3 class="ui ">3. {{ trans('global.section3') }}</h3>
      
    </div>
  </div>

</div>

</div>
  <div class="ui vertical stripe segment">
    <div class="ui text container">
      <img class="image"  src="img/flyers/exchange-visitors-fawour.gif" alt="exchange visitors">
      <h3 class="ui header">{{ trans('global.whyFawour') }}</h3>
      <p>{{ trans('global.wemakeit') }}</p>
      <a class="ui large button readmoreabutfaw">{{ trans('global.readmoroor') }}</a>
    </div>


  <div class="ui vertical stripe segment " style="display:none">
    <div class="ui middle aligned stackable grid container">
      <div class="row">
        <div class="eight wide column">
          <h3 class="ui header">{{ trans('global.partnerupwith') }} <span class="text-muted getitalic">{{ trans('global.creatorsinyourfild') }}</span></h3>
          <p>{{ trans('global.sameniche') }}</p>
        </div>
        <div class="six wide right floated column image">
          <img src="img/flyers/work-together.gif"  alt="Work togeter" class="ui large bordered rounded image">
        </div>
      </div>

            <div class="row">
        <div class="six wide left floated column image">
          <img src="img/flyers/share-links.gif" alt="Share links" class="ui large bordered rounded image">
        </div>
        <div class="eight wide column">
          <h3 class="ui header">{{ trans('global.ncreaseisitors') }}<span class="text-muted getitalic">{{ trans('global.valuableleg') }}</span></h3>
          <p>{{ trans('global.submitlinks') }}<br><br>{{ trans('global.youpartner') }}</p>
        </div>

      </div>

            <div class="row">
        <div class="eight wide column">
          <h3 class="ui header">{{ trans('global.Increasevisitors') }}<span class="text-muted getitalic">{{ trans('global.Checkmate') }}</span></h3>
          <p>{{ trans('global.win-win') }}<br><br>{{ trans('global.anhareyour') }}</p>
        </div>
        <div class="six wide right floated column image">
          <img src="img/flyers/increase-visitors.jpg" alt="increase visitors" class="ui large bordered rounded image">
        </div>
      </div>

    </div>
  </div>
  </div>

    <h2 class="ui horizontal header divider">
        <a href="#">{{ trans('global.whatotherssay') }}</a>
      </h2>
  <div class="ui vertical stripe quote segment">
    <div class="ui equal width stackable internally celled grid">
      <div class="center aligned row">
        <div class="column">
          <h3>"Always knowing Fawour has the power to spread the art of my creations is a blessing, it could be yours too"</h3>
           <p>
            <img src="https://www.fawour.com/uploads/avatars/120EliyasMannaa1468776345.jpg" class="ui avatar image"> EliyasMannaa's <a href="http://eliyasmannaa.dk/">Blog.</a>
          </p>
        </div>
        <div class="column">
          <h3>"Fawour is a great innovative site that allows creators to connect and help each other grow. It reminds us that the fundamentals of online interaction are not only receiving help but also giving some in return. The best thing is that you can see it happening in real time"</h3>
          <p>
            <img src="/img/styleshea.png" class="ui avatar image"> Estee from <a href="http://www.stylesteeya.com/">Stylesteeya.</a>
          </p>
        </div>
      </div>
    </div>
  </div>



      <div class="ui  container centerthis">
      <h1 class="ui  header ">
       {{ trans('global.togetheryouare') }}
      </h1>
      <h2>{{ trans('global.madeBy') }}</h2>
      <a href="/register"><div class="ui huge primary button">{{ trans('global.GetStarted') }} <i class="fa fa-arrow-right" aria-hidden="true"></i></div></a>
    </div>
    

<div class="ui vertical stripe">
      <h2 class="ui horizontal header divider">
        <a href="/register">{{ trans('global.contToSite') }}</a>
      </h2>
      <!--<a href="/register"><div class="ui huge primary button">{{ trans('global.GetStarted') }} <i class="fa fa-arrow-right" aria-hidden="true"></i></div></a>-->
             <a href="../auth/facebook"><div class="ui labeled button signupthis" tabindex="0">
                  <div class="ui blue button">
                     {{ trans('global.withLogin') }} Facebook
                  </div>
                  <div class="ui basic blue left pointing label">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </div>
                </div>
                </a>
                 <a href="../auth/twitter"><div class="ui labeled button signupthis" tabindex="0">
                  <div class="ui teal button">
                    {{ trans('global.withLogin') }} Twitter
                  </div>
                  <div class="ui basic teal left pointing label">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </div>
                </div>
                </a>

                <a href="../auth/google"><div class="ui labeled button signupthis" tabindex="0">
                  <div class="ui red button">
                     {{ trans('global.withLogin') }} Google
                  </div>
                  <div class="ui basic red left pointing label">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                  </div>
                </div>
                </a>

                <a href="/register" class="signupbyemal"><div class="ui labeled button  signupthis" tabindex="0">
                  <div class="ui standard button">
                     {{ trans('global.withLogin') }}  Email
                  </div>
                  <div class="ui basic standard left pointing label">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                </div>
                </a>
    </div>
<script>
document.addEventListener("DOMContentLoaded", function() {
  $(".readmoreabutfaw").click(function(){
    $(".ui.vertical.stripe.segment").fadeIn(1000);
  });
});
</script>
</div>
</div>
</div>
</div>
</div>
<style>
    .ui.items>.item>.image:not(.ui){
      width:70px;
    }
    .item .image i{
      font-size:5em !important;
      color:rgba(0,0,0,.8);
      float:right;
      margin-right:15px;
    }
    .ui.two.column.stackable.grid.container .column .ui{
      margin-bottom:5%;
    }
    @media only screen and (max-width: 767px) {
      .item .image i{
      float:left;
      margin-right:0px;
    }
    .ui.items>.item>.image+.content{
     text-align: center;
    }
    </style>
@include("pages/partials/footer")
@endsection
