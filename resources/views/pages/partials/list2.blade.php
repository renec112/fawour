<style>
.event .label{
	padding:0.5em 0 0 0  !important;
	margin-bottom:15px;
}
.hasReadThis{
	opacity:0.5;
}
.fa{
	color:#383838;
    font-size: 3em;
}
</style>
<div class="ui feed">
@foreach ($activity as $event)
	@if ($event->notifyThis == 0)
	  <div class="event hasReadThis">
		@include("pages.partials.2.{$event->type}")
		</div>
	@else 
	  <div class="event">
		@include("pages.partials.2.{$event->type}")
		</div>
	@endif
@endforeach
{!! $activity->links() !!}
</div>

