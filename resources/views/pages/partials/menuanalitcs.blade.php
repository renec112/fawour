<div class="ui secondary vertical pointing menu">
 <a id="analytics1" href="/analytics/visitors" class="{{ Request::is('analytics/visitors') ? ' active' : '' }} item">{{ trans('app.Visitors') }} </a>
  <a id="analytics2" href="/analytics/gottenby" class="{{ Request::is('analytics/gottenby') ? ' active' : '' }} item " >{{ trans('app.Gottenby') }}</a>
 <a id="analytics3" href="/analytics/givento" class="{{ Request::is('analytics/givento') ? ' active' : '' }} item">{{ trans('app.givenTo') }}</a>
</div>