<div class="smallmenudiv">
<h1><i class="fa fa-user" aria-hidden="true"></i> {{ trans('app.Profile') }}</h1>

<div class="ui secondary pointing menu">
  <a class="{{ Request::is('profile') ? ' active' : '' }} item" href="/profile">
    {{ trans('app.yourporfile') }}
  </a>


  <a class="item {{ Request::is('profile/edit') ? ' active' : '' }}" href="/profile/edit">
    {{ trans('app.yourinformaton') }}
  </a>
  <a class="item {{ Request::is('profile/avatar') ? ' active' : '' }}" href="/profile/avatar">
    {{ trans('app.Profilepicture') }}
  </a>
    <a class="{{ Request::is('profile/settings') ? ' active' : '' }} item"  href="/profile/settings">
      {{ trans('app.Settings') }}
    </a>
     <a class="{{ Request::is('profile/notifications') ? ' active' : '' }} item" href="{{ route('notifications') }}">{{ trans('navi.Notifications') }}
    </a>
</div>
<div class="ui hidden divider"></div>
</div>
