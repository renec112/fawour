@foreach ($activity as $event)
	@if ($event->notifyThis == 0)
	<li class="list-group-item hasRead">
		@include("pages.partials.types.{$event->type}")
	</li>
	@else 
	<li class="list-group-item">
		@include("pages.partials.types.{$event->type}")
	</li>
	@endif
@endforeach
{!! $activity->links() !!}