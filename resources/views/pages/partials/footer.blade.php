  <div class="ui inverted vertical footer segment">
    <div class="ui container">
      <div class="ui stackable inverted divided equal height stackable grid">
        <div class="three wide column">
          <h4 class="ui inverted header">{{ trans('global.moreFaw') }}</h4>
          <div class="ui inverted link list">
          <a href="/about" class="item">{{ trans('global.About22') }}</a>
          <a href="/more" class="item">Screenshots</a>
            <a href="/updates" class="item">{{ trans('global.updateLog') }}</a>
             <a href="/faq" class="item">F.A.Q.</a>
              <a href="/stats" class="item">{{ trans('app.Analytics') }}</a>

          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">Social</h4>
          <div class="ui inverted link list">
          <a href="https://twitter.com/FawourApp" class="item">Twitter</a>
          <a href="https://www.facebook.com/fawourApp/" class="item">Facebook</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">{{ trans('app.Langauge') }}</h4>
          <div class="ui inverted link list">
              <a href="/la/en" class="item">English</a>
              <a href="/la/da" class="item">Danish</a>
          </div>
        </div>
        <div class="four wide column">
          <h4 class="ui inverted header">{{ trans('global.About22') }}</h4>
          {{ trans('global.fawismadegettoach') }}
           <a href="https://docs.google.com/forms/d/1btx_ls0ZdfEsb5MCKel4pWWVLaGx14H0CYxBArQAq90/viewform">{{ trans('global.contactushere') }}</a>
        </div>
      </div>
    </div>
  </div>