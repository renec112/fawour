<div class="smallmenudiv">
<h1><i class="fa fa-share-alt" aria-hidden="true"></i> Links</h1>
<div class="ui secondary pointing menu">
  <a class="{{ Request::is('links/myLinks') ? ' active' : '' }} item" href="/links/myLinks">
    {{ trans('app.mylinksxx') }}
  </a>
  <a class="item {{ Request::is('links/share') ? ' active' : '' }}" href="/links/share">
    {{ trans('app.shareparnterslnisk') }}
  </a>
  <a class="item {{ Request::is('links/explore') ? ' active' : '' }}" href="/links/explore">
    Explore links
  </a>
      <a class="{{ Request::is('analytics*') ? ' active' : '' }} item" href="/analytics/visitors">
    {{ trans('app.Analytics') }}
  </a>
</div>
<div class="ui hidden divider"></div>
</div>
