<div class="smallmenudiv">
<h1><i class="fa fa-users" aria-hidden="true"></i>{{ trans('app.partnerssss') }}</h1>
<div class="ui secondary pointing menu">
  <a class="{{ Request::is('partners/myPartners') ? ' active' : '' }} item" href="/partners/myPartners">
    {{ trans('app.partnerssss') }}
  </a>
  <a class="{{ Request::is('partners/explore') ? ' active' : '' }} item" href="/partners/explore">
    Find new
  </a>
  <a class="item {{ Request::is('partners/search') ? ' active' : '' }}" href="/partners/search">
    {{ trans('app.Search') }} by name
  </a>
    <a class="item {{ Request::is('messages') ? ' active' : '' }}" href="/messages">
    {{ trans('app.message') }}
  </a>
    <a class="item {{ Request::is('chat') ? ' active' : '' }}" href="/chat">
    Chat
  </a>
</div>
<div class="ui hidden divider"></div>
</div>
