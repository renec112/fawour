      <div class="label">
        <?php 
$articles = \App\user::where("id", $event->from_id)->first();
?>
      <img src="{{$articles->getpofilepic120()}}">
    </div>
    <div class="content">

      <div class="summary">
         {{ trans('app.newmessagefrom') }}  <a href="../messages?from={{$event->from_id}}">{{ucfirst($event->route)}}</a>, 
        <div class="date">
          {{$event->created_at->diffForHumans()}}. 
        </div>
      </div>
    </div>
