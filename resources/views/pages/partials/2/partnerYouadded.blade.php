<div class="label">
     <i class="fa fa-users" ></i>
    </div>
    <div class="content">
    You sent <a href="/partner/{{$event->data}}">{{$event->data}}</a> a request for partnership.
      <div class="summary">
        <div class="date"> {{$event->created_at->diffForHumans()}}. </div>
      </div>
    </div>