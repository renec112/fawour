<div class="label">
     <i class="fa fa-external-link" ></i>
    </div>
    <div class="content">
    You created a new <a href="/links/myLinks">link</a>.
      <div class="summary">
        Described as "{{$event->data}}".
        <div class="date"> {{$event->created_at->diffForHumans()}}. </div>
      </div>
    </div>