@include('pages.partials.menulinks')
<style>
.hidethis{display:none !important;}

  .thiscard{
    color:#19bd9a !important;
    box-shadow:0 0 0 1px #19bd9a inset !important;
  }
</style>
<div class="ui teal icon button"><a href="/links/create"><i class="fa fa-plus" aria-hidden="true"></i> {{ trans('app.Createlink') }}</a></div>
<br><br>
	 	@if (!$url_database->count())
	    <p>{{ trans('app.nolinksyet') }} <a href="{{route('create_url')}}">{{ trans('app.createsameee') }} </a></p>
    @else
     
            <div class="ui three stackable cards">
            @foreach($url_database as $entry)
 
            <div class="card">
              <div class="image thisImageRightHere">
                    <div class="ui active inverted dimmer">
                      <div class="ui text loader"></div>
                    </div>
                    <img class="ui medium image " src="/img/wireframe.png"/>
                  </div>
              <div id="linkAbout" class="floating ui {{ $user->returnColor($entry->field) }} label">{{ $user->returnColor($entry->field) }}</div>
                <div class="content" id="linkAbout">
                  <div class="header"  id="linkdes"> 
                       {{ str_limit($entry->description, $limit = 50, $end = '...') }}
                  </div>
                  <div class="meta">
                              <a class="inspectThisLink" href="{{$entry->url}}" >
                          <?php
                  $showtheurl = $entry->url;
                  $showtheurl = str_replace('www.', '', $showtheurl);
                  $showtheurl = str_replace('https://', '', $showtheurl);
                  $showtheurl = str_replace('http://', '', $showtheurl);

                  $showtheurl = mb_strimwidth($showtheurl, 0, 30,  "....");
                  echo $showtheurl; ?></a>
                  </div>
                  <div class="ui label" id="linkgotten"> {{ trans('app.Visitorsgotten') }}: {{$entry->count}}</div>
   
                </div>
                <div  class="extra content">
                                   <a href="{{route("showfull", array("id" => $entry->id))}}" ><button class="ui basic button">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </button></a>
                      <a href="{{route("edit_url", array("id" => $entry->id))}}"><button class="ui basic button">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </button></a>
                      <button  id="linkmore" class="ui basic red button" onclick="copyToClipboard('{{$entry->id}}')" >
                       <i class="fa fa-times" aria-hidden="true"></i>
                        <li class="hidethis"> <a href="{{route("delete_url", array("id" => $entry->id))}}" id="link{{$entry->id}}"></a></li>
                      </button>
                </div>
                <div class="geturl hidethis">
                  <a href="{{$showtheurl = $entry->url}}">link</a>
                </div>
              </div>              

      @endforeach
           {!! $url_database->render() !!}
      </div>
	@endif

      
<script>
function copyToClipboard(element) {
swal({   title: "{{ trans('app.Abouttodeletelink') }}",   text: "{{ trans('app.Abouttodeletelink') }}",   type: "warning",   cancelButtonText: "{{ trans('app.cancel') }}", showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "{{ trans('app.yesdeleteit') }}",   closeOnConfirm: false }, function(){  

document.getElementById("link"+element).click();
});
} 
document.addEventListener("DOMContentLoaded", function() {
   $cards = $('.card');
    $cards.each(function() {
    thisDiv = $( this ).find( '.thisImageRightHere' )
      var checkThisLink = $( this ).find( '.inspectThisLink' ).attr("href"); 
 var finalCheck = getYahoo(checkThisLink, thisDiv);
});
  $('.card').each(function() {
    var link = jQuery(this).find(".geturl a").attr("href");
});
});

</script>
<script src="/js/link.js"></script>
<span class ="helperrr"  data-position="top left" onClick="RunTourHere(4);" data-tooltip="{{ trans('app.touronhowtousepage') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>