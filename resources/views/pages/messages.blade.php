@extends('layout')
<style>
.list-inline li{
	display:inline;
}
.notAurl{
	cursor:default !important;
}
	.senderbutton{
		border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#19bd9a;
  padding:0;
  text-decoration:none;
  font-family:inherit;
  font-size:inherit;
  display:inline;
	}
	.messagesbody{
	 background-color: #f7f7f7 !important;
	 padding:15px 15px 5px 15px;
	border-radius: 20px;
	 width:80%;
	}
	.messagesmine{
		background-color: #19bd9a !important;
		margin-left:20%;
		color:#ffffff;
	}
	.thiscard{
		white-space: inherit !important;
		border: 1px solid  #19bd9a !important;
	}
	.notthiscard{
		white-space: inherit !important;
	}
	.circle_the_text.notAurl{
		margin-top:9px;
	}
	.messagesmine a{
		color:#ffffff;
	}
		.messagesmine a:hover{
		color:#ffffff;
	}
	.formmessage{
		margin-top:10px;
	}
	.pagination > .active > a, .pagination > .active > a:hover, .pagination > .active > a:focus, .pagination > .active > span, .pagination > .active > span:hover, .pagination > .active > span:focus{
		background-color:#19bd9a !important;
		border-color:#19bd9a !important;
	}
	.pagination > li > a{color:#354a5f !important;}
	.usercontainer{
		background-color: #ffffff;
		padding:15px;
	}
	.currentUser{background-color: #19bd9a;}
	.currentUser div form .senderbutton{color:#ffffff;}
	.messagecontainermessages{
		max-height: 700px;
		overflow-y:scroll;
	}
	.inlinethis{
		display: inherit;
	}
	.media{
		padding:5px;
	}
	.usercontainer:first-child{
		margin-top:10px;
	}
	.media-body{
		padding:10px;
	}
	.circleinmessage{
		position: relative;
		top:4px;
		right:2px;
	}
	.media.usercontainer  {
		margin-top:0px !important;
	}
	.ui.comments .comment .avatar img, .ui.comments .comment img.avatar{
		height:auto !important;
	}
	.messagelabel{
		background-color:#ffffff !important;
		white-space: inherit !important;
	}
	.messagelabel div{
		white-space: inherit !important;
		line-height: 1.5 !important;
	}
	.mycomment{
		float:right;
	}
	.dividermess{
		width:100%;
		display: inline-block;
	}
	.ui.pointing.below{
		font-size:1.2em !important;
	}
</style>

@section('content')
@include('pages.partials.aprtnersmenu')


	<div class="row">
	    	<div class="col-lg-4 getWhite" id="byContainer">
				@if (!$user->partners()->count())
				<p>No partners yet.</p>
			@else
				@foreach($user->partners()->sortByDesc("ratio") as $user1)

					@include("user/partials/userblockmessage")
				
				@endforeach
			@endif

		</div>
	    <div class="col-lg-8 messagecontainer" id="messsContainer">
	    @if (\Request::input('from') != 0)
	    <h1> {{ trans('app.message') }} <a href="/partner/{{$writingTo->getName()}}">{{$writingTo->getName()}}</a></h1>
	    @endif
	    	<div class="messagecontainermessages">
	     @if (\Request::input('from') == 0)



	      <form role="form" id="form"   class="formmessage hide">
		            <div class="form-group">
		                <textarea  id ="messages" name="messages" class="form-control" rows="2" required>
		             
		                </textarea>
		                <input class=hidden name='formFrom' value='{{ \Request::input('from')}}'>
		            </div>
		            @if ($errors->has("messages"))
					<span class="help-block">{{ $errors->first("messages")}}</span>
		            @endif
		            <strong id="helpBlockloength"></strong>
		            <button type="submit" class="btn btn-primary1 pull-right">{{ trans('app.sendmessage') }} <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
		            {{ csrf_field() }}

		        </form>




	        @else 
	        
	        @if (!$messages->count())
	        	<p>No messages yet.</p>
	        @else
				@foreach($messages as $message)
					<div class="messageentry @if($message->user->name == $user->name) mycomment @else othercomment @endif">
						<span class="ui label messagelabel">
						 	@if($message->user->name == $user->name)
										 <div class="ui pointing below thiscard basic label">
									@else
									 <div class="ui pointing below notthiscard grey basic label">
									@endif
						     {{ $message->body }}
						    </div><br><br>
						    <img src="{{ $message->user->getpofilepic35() }}">  {{ $message->user->getName()}}, {{ $message->created_at->diffForHumans()}}

						</span>
					</div>
					<div class="dividermess"></div>
				@endforeach
				{!! $messages->appends(['from' => Request::input("from")])->render() !!}
	        @endif
	       </div>
		        <form role="form" id="form" action="{{ route('messages.post')}}" method="post" class="formmessage">
		            <div class="form-group{{ $errors->has('messages') ? ' has-error' : '' }}">
		                <textarea onkeyup="validate()" id ="messages" name="messages" class="form-control" rows="2" required>
		             
		                </textarea>
		                <input class=hidden name='formFrom' value='{{ \Request::input('from')}}'>
		            </div>
		            @if ($errors->has("messages"))
					<span class="help-block">{{ $errors->first("messages")}}</span>
		            @endif
		            <strong id="helpBlockloength"></strong>
		            <button type="submit" class="btn btn-primary1 pull-right">{{ trans('app.sendmessage') }} <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
		            @if($lastMessage != null)
		            	<p>{{ trans('app.1meshour') }}{{$lastMessage}}. {{ trans('app.2meshour') }}</p>
		            @endif
		            {{ csrf_field() }}

		        </form>
	        @endif

	    </div>

 <script>
function validate() {
 var value = document.getElementById('messages').value;
 if (value.length < 1000) {
    document.getElementById("helpBlockloength").innerHTML = value.length + " / 1000";
    document.getElementById("helpBlockloength").style.color = "#19bd9a";
 } else{
    document.getElementById("helpBlockloength").innerHTML = value.length + " / 1000";
    document.getElementById("helpBlockloength").style.color = "red";
   
 }
}
document.addEventListener("DOMContentLoaded", function() {
	 document.getElementById('messages').value = "";
});
</script>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var initial_form_state = $('#form').serialize();
    $('#form').submit(function(){
      initial_form_state = $('#form').serialize();
    });
    $(window).bind('beforeunload', function(e) {
      var form_state = $('#form').serialize();
      if(initial_form_state != form_state){
        var message = "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        e.returnValue = message; // Cross-browser compatibility (src: MDN)
        return message;
      }
    });
});
</script>

	</div>
	

	</div>
	 <span class ="helperrr" onClick="RunTourHere(7);" data-tooltip="{{ trans('app.tourmess') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>
@stop