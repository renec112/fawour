@extends('layout')
@section('content')
<div class="row">
<div class="col-md-12 col-sm-12 paddthis">
<h1>Fawour's stats</h1>
<h2>Overall</h2>
<div class="ui statistics">
  <div class="statistic">
    <div class="value">
      {{$linksCreated}}
    </div>
    <div class="label">
      Links created
    </div>
  </div>
  <div class="statistic">
    <div class="value">
      {{$userstraffic}}
    </div>
    <div class="label">
      Visitors exhanged
    </div>
  </div>
  <div class="statistic">
    <div class="value">
      {{$userscount}}
    </div>
    <div class="label">
      Members
    </div>
  </div>
</div>
<h2>Members statistics</h2>
<div class="ui statistics">
   @foreach ($user_info as $field => $number)
     <div class="{{ App\User::returnColor($field) }} statistic">
    <div class="value">
      {{$number}} 
    </div>
    <div class="label">
      {{App\Http\Utilities\Country::getFieldName($field)}} 
    </div>
  </div>
   @endforeach
</div>
</div>
</div>
</div></div></div>
</div>
@include("pages/partials/footer")
@stop