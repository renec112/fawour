@extends('layout')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
.outer{
	display:none !important;
}
.media-body{
	width:0% !important;
}
.expand-transition{
	transition: all 0.3s ease !important;
	height:auto !important;
	overflow:hidden !important;
}
.expand-enter, .expand-leave{
	height:0 !important;
	padding:0 20px !important;
	opacity:0 !important;
}
.media.expand-transition{
	margin: 0 0;
}
.posts{
	height:500px;
	overflow-y:scroll;
}
.CommingSoon{
	color:grey;
	width:100%;
}
.media .media-left .media-object{
	min-width:35px;
}
.expand-transition div img{
	border-radius: 50%;
}
.expand-transition{
	padding:10px;
}
hr{
	margin: 10px 0 0 0 !important;
}
	/* Variables
 ------------------------------------------------------------- */
/* Animation from Animate.css
 ------------------------------------------------------------- */


/* Container
 ------------------------------------------------------------- */


/* Individual Controls
 ------------------------------------------------------------- */
.radio {
  display: inline-block;
  padding-right: 20px;

  line-height: 40px;
  cursor: pointer;
}
.radio:hover .inner {
  -webkit-transform: scale(0.5);
  transform: scale(0.5);
  opacity: .5;
}
.radio input {
  height: 10px;
  width: 10px;
  opacity: 0;
}
.radio input:checked + .outer .inner {
  -webkit-transform: scale(1);
  transform: scale(1);
  opacity: 1;
}
.radio input:checked + .outer {
  border: 3px solid #19bd9a;
}
.radio input:focus + .outer .inner {
  -webkit-transform: scale(1);
  transform: scale(1);
  opacity: 1;
  background-color: #19bd9a;
}
.radio .outer {
  height: 20px;
  width: 20px;
  display: block;
  float: left;
  margin: 10px 9px 10px 10px;
  border: 3px solid #b2b3b8;
  border-radius: 50%;
  background-color: #fff;
}
.radio .inner {
  -webkit-transition: all 0.25s ease-in-out;
  transition: all 0.25s ease-in-out;
  height: 14px;
  width: 14px;
  -webkit-transform: scale(0);
  transform: scale(0);
  display: block;
  margin: 0px;
  border-radius: 50%;
  background-color: #19bd9a;
  opacity: 0;
}
.selectedForum{
	display:none;
}

</style>
@section('content')
@include('pages.partials.aprtnersmenu')

	<div class="row" id="chat">
	<form role="form" action="/#/" method="post" class="formmessage" v-on:submit="postMessage">
    	<div class="col-lg-3 getWhite">
            <div class="form-group" id="sectioonndcah">
  
				<label  class="radio selectorLabel CommingSoon" for="ev"><input type="radio" class ="idHide" checked="checked" id="ev" value="ev" name="forum" v-model="forum" v-on:click="getPosts($event)"><span class="outer"><span class="inner"></span></span>
				<span class="ui basic label">Anything</span></label>
                @foreach (App\Http\Utilities\Country::allfield() as $name => $code)
               
                <label  class="radio selectorLabel CommingSoon" for="{{$code}}"><input type="radio" class ="idHide" id="{{$code}}" value="{{$code}}" name="forum" v-model="forum" v-on:click="getPosts($event)"><span class="outer"><span class="inner"></span></span>
				<span class="ui {{ $user->returnColor($code) }} basic label">{{$name}}</span></label>
						
                @endforeach 
            @if ($errors->has('field'))
                <span class="help-block">
                    <strong>{{ $errors->first('field') }}</strong>
                </span>
            @endif
        </div>
		</div>

	    <div class="col-lg-9 messagecontainer getWhite" id="messcoun">
	    	  <h4 class="ui horizontal divider header">
                            Chats from <span id="helptextForum">Anything</span>
                        </h4>
			<div class="posts">
				<p v-if="!posts.length">Nothing yet..</p>
				
				<div class="media" v-for="post in posts" track-by="id" transition="expand">
					<div class="media-left">
				
						<img class="media-object" v-bind:src="post.user.image">
					</div>
					<div class="media-body">
						<div class="user">
	
							<a href="partner/@{{ post.user.name}}"><strong>@{{ post.user.name}}</strong> <i class="twa  twa-@{{ post.user.smiley}}"></i></a> - @{{ post.humanCreatedAt}}
							<p>@{{ post.body}}</p>
						</div>
					</div>
					<hr>
				</div>

				<!-- <a v-if="total < limit" href="#" v-on:click="getMorePosts($event)">Show more (there might not be)</a> -->
			</div>
			

	        <div class="form-group{{ $errors->has('messages') ? ' has-error' : '' }}">
	            <textarea placeholder="Type message in anything here" onkeyup="validate()" id="xxxxxxxx" required maxlength="200" class="form-control messageTextarea" rows="1" required v-model="post"></textarea>
	            <input class=hidden name='formFrom' value='{{ \Request::input('from')}}'>
	        </div>
	        @if ($errors->has("messages"))
				<span class="help-block">{{ $errors->first("messages")}}</span>
	        @endif
	        <button type="submit" class="btn btn-primary1 pull-right" id="sendMessage">Send <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
	        <p id="helpBlock"></p>
	        <strong id="helpBlockloength"></strong>
	        {{ csrf_field() }}
	       
	    </div>
 	</form>
		
 	<script>
        function validate() {
         var value = document.getElementById('xxxxxxxx').value;
         if (value.length < 200) {
            document.getElementById("helpBlockloength").innerHTML = value.length + " / 200";
            document.getElementById("helpBlockloength").style.color = "#19bd9a";
         } else{
            document.getElementById("helpBlockloength").innerHTML = value.length + " / 200";
            document.getElementById("helpBlockloength").style.color = "red";
         }
        }
	</script>
	</div>
	
 	 <span class ="helperrr" data-position="top left" onClick="RunTourHere(8);" data-tooltip="Take a tour to see how to use this chat function" id="tour1" ><i class="fa fa-question-circle-o"></i></span>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>

	<script>
	document.addEventListener("DOMContentLoaded", function() {

		$('input[name="forum"]').on('click', function(e) {
		activeForum =  document.querySelector('input[name="forum"]:checked').value;
		styleThis = document.querySelector('input[name="forum"]:checked').parentNode;
		console.log(this.parentNode);
		if(activeForum == "ev"){
			activeForum="Anything"
		} else if (activeForum == "ha"){
			activeForum="Health & fitness"
		} else if (activeForum == "gam"){
			activeForum="Gaming"
		} else if (activeForum == "bf"){
			activeForum="Beauty and fashion"
		} else if (activeForum == "mu"){
			activeForum="Music"
		} else if (activeForum == "ha"){
			activeForum="Health & fitness"
		} else if (activeForum == "te"){
			activeForum="Tech"
		} else if (activeForum == "de"){
			activeForum="Design & photography"
		} else if (activeForum == "cr"){
			activeForum="Comedy & reality"
		} else if (activeForum == "li"){
			activeForum="Lifestyle"
		} else if (activeForum == "fo"){
			activeForum="Food"
		} else if (activeForum == "tr"){
			activeForum="Traveling"
		} else if (activeForum == "ot"){
			activeForum="Other"
		} 
		document.getElementById("xxxxxxxx").placeholder = "Type message in " + activeForum + " here";
		document.getElementById("helptextForum").innerHTML=activeForum;
		});
		$('.messageTextarea').on('keyup', function(e) {
			var val = this.value;
		  	/*if ( val.indexOf('www') !== -1 || val.indexOf('.com') !== -1 || val.indexOf('http') !== -1) {
			 	 document.getElementById("helpBlock").innerHTML = "We are experiencing a lot of spam on the chat. Therefore links are temporarily not allowed. We hope you understand.";
			 	 document.querySelector(".messageTextarea").value = "";
		  	}*/
		    if (e.which == 13 && ! e.shiftKey) {
		        	$('#sendMessage').click();
		    }else if (e.which == 13){
		    	e.preventDefault();
		    }
		});
		// click fix
	
		new Vue({
			el: "#chat",
			data: {
				post: '',
				posts:[],
				limit: 20,
				forum: '',
				total: 0,

			},
			methods:{
				postMessage: function(e){
					e.preventDefault();
					// Ajax request
					$.ajax({
						url: '/chat',
						type: 'post',
						dataType: 'json',
						headers: {
					            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
						data:{
							'body': this.post,
							'forum': this.forum,
						}
					}).success(function(data){
						this.post = "";
						this.posts.unshift(data);
						
					}.bind(this)).error(function(){
						swal({   title: "Sory, something went wrong",   text: "We are experiencing a lot of spam on the chat. Therefore links are temporarily not allowed. If your comment didn't contain a link, check your internet or contact us.",   type: "error", showConfirmButton: true });
					});
				},
				getPosts: function(e){
					$.ajax({

						url: '/chatGet',
						dataType: 'json',
						type: 'get',
						data: {
							limit: this.limit,
							forum: document.querySelector('input[name="forum"]:checked').value

						}
					})
					.success(function(data){
						this.posts = data.posts;
						this.total = data.total;
					}.bind(this));
				},
				getMorePosts: function(e){
					e.preventDefault();
					this.limit = this.limit + this.limit;
					this.getPosts();
				},


			},
			ready: function(){
				this.getPosts();
				setInterval(function(){
					this.getPosts();
				}.bind(this), 30000);
			}

		});
	
	});
	</script>


@stop