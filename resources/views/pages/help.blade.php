@extends('layout')

@section('content')
<style>
.articlehelp{
  line-height: 2.5em;
}
.articlehelp p{
  font-size:1.2em;
}
.articlehelp li{
  font-size:1.2em;
}
</style>
  <div class="row articlehelp">
    <div class="col-lg-12 col-md-12  col-sm-12 ">
    <h1>Help</h1>
    <p>In this guide i’ll show you how to use the app. First, i’ll guide you through how to setup your profile correctly. Then how you submit links and how that all works. Then i’ll show you how to find partners to share visitors with. At last, I will show you how to actually share visitors. 
Did we miss anything, or do you have a question? Read the <a href="http://fawour.com/faq">F.A.Q.</a> or feel free to <a href="https://docs.google.com/forms/d/1btx_ls0ZdfEsb5MCKel4pWWVLaGx14H0CYxBArQAq90/viewform">contact me.</a> </p>
<p>You can also watch a video tutorial instead <a href="https://www.youtube.com/watch?v=sE2dPr9yjao">here.</a></p>
<h2>Content</h2>
  <ul>
    <li><a href="#Setup-profile">Setup profile</a></li>
    <li><a href="#Submitting-links">Submitting links</a></li>
    <li><a href="#Finding-partners">Finding-partners</a></li>
    <li><a href="#Sharing-visitors">Sharing visitors</a></li>
  </ul>
<a name="Setup-profile"></a> 
    <h2>Setup your profile</h2>

    <p>The first thing we should do is to setup your profile. Having a good profile will help you future partners to work with you or not. 
All the information needed for your profile is available <a href="http://fawour.com/profile/edit">here.</a><br> </p>
<div class="helpImg"><img src="/img/profilesettings.jpg"></div>
<p>First write something about yourself in the description – who are you and what do you do?<br>
Next select your content niche. I myself do photoshop tutorials so my content is within the niche “Design and photography”.<br>
You can also select what language your content is in. Remember it’s the content. I’m a Danish guy but my Photoshop tips In English. So I would choose English.<br>
At last you can submit links to your social media sites and website. Do you have a facebook page, youtube channel or blog? Past the links in there.
Now submit “update” to save the settings. When you are done, you can see your profile and the information you submitted on <a href="http://fawour.com/profile">your profile.</a></p>
<div class="helpImg"><img src="/img/myprofile.jpg"></div>
<p>There’s a bit additional information on your profile. It’s Visitors given, visitors gotten and the ratio. Here we show how many people have visited a link you have created. That’s visitors gotten. Also we show how many visitors who has visited a link you shared. Lastly, “ratio” is just the numbers divided by each other – So you can see how many visitors you give versus gets. </p>
<a name="Submitting-links"></a> 
 <h2>Submitting links</h2>
 <p>Submitting links is very easy. Go to “links”, and then “create links”. Or click <a href="http://fawour.com/links/create">here.</a>
</p>
<div class="helpImg"><img src="/img/create-links.jpg"></div>
<p>You will see to input fields. Link and link description. Select the contet  you would like more visitors on. Maybe a youtube video or an article? Whatever you want, get the link. It’s easitest just to go there and copy it from the browser.<br>
Next, write something about the link. What the content about? This will help your partners to decide if they should share it or not. 
I think you should add at least 3 links. Here is an example:

<div class="helpImg"><img src="/img/create-link-example.jpg"></div>
Each link you submit creates a shortcode for your partners. With this short code, we can see what link it is, and what partner it belongs to. So we can count if how many visitors clicks on it from who. </p>
<a name="Finding-partners"></a> 
 <h2>Finding partners</h2>
<p>Now that your profile is ready let’s find some new partners. Go to partners. Here you can select to options. Either search by name <a href="http://fawour.com/partners/search">here.</a> That’s straightforward. That's done here and. below is an example</p>
<div class="helpImg"><img src="/img/search.jpg"></div>
<p>You can also choose "explore" <a href="http://fawour.com/partners/explore">here.</a> there’s a few parameters. If we keep advanced search disabled there’s 3 options:</p>
<ul>
  <li>Field. Here you choose what field you want to see users in. Maybe gaming or fashion? It’s the same as yours as default.</li>
  <li>Next choose the desired content language. I suggest you select the same as yours content language.</li>
  <li>At last you can select what social platforms the user should be on. If this doesn’t matter to you, you can select anything.</li>
</ul>
<p>As shown below:</p>
<div class="helpImg"><img src="/img/explore.jpg"></div>
<p>Now there’s a bunch of users you can choose to partner up with. Do you like their content? Do you think your visitors would like their content, and their visitors would like your content? Then team up by clicking the “add partner”</p>
<a name="Sharing-visitors"></a> 
 <h2>Sharing visitors</h2>
 <p>When your partner has accepted your request, both of you are ready to start sharing visitors.<br>
 Each link your partner has submitted has generated a special link for you. The same goes for your links as well.<br>
 You can find these specials links, by going to links, then “Share links” or click <a href="http://fawour.com/links/share">here.</a> You can also go to a partner's profile and click "Links created”. This it what you will see:
 </p>
 <div class="helpImg"><img src="/img/links-by-partner.jpg"></div>
<p>Here you can see who the links are from, a deception about the link, the link itself, and a option to view the link. At last you can also get your special link by clicking “Copy share link” or copy it manually, at the right where the page says “You share link is fawour.com/l/….”.</p>
<p>With this link you can put it in a facebook status, a blog post, youtube annotation – wherever you want. When a user clicks on it the following happens:
</p>
<ol>
  <li>The visitor get redirected to the link.</li>
  <li>Meanwhile server increments your “Visitors given”</li>
  <li>Meanwhile server increments your partners “Visitors gotten”</li>
  <li>A new give/get ratio is being calculated.</li>
</ol>
<p>
Off course, the opposite happens if your partners visitors clicks on the link. <br>
By the way, we store the IP address of who clicks on the link, to prevent false/spammy points being generated.<br><br>
Now you are ready to start growing with other creators. By sharing each others content with relevant followers/users you can start growing together! 

</p>
<h2>More</h2>
<p>Did we miss anything, or do you have a question? Read the F.A.Q. or feel free to contact me <a href="https://docs.google.com/forms/d/1btx_ls0ZdfEsb5MCKel4pWWVLaGx14H0CYxBArQAq90/viewform"> here.</a></p>
    </div>
  </div>
@stop