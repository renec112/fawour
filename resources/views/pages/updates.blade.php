@extends('layout')
<style>
	.hasRead{
		background-color:#e8e8e8 !important;
		color:#ADADAD !important;
	}
	.hasRead a, .hasRead a:hover{
		color:#91BBB2 !important;
	}
	.list-group-item ul{
		 list-style: none;
		    padding:0;
		    margin:0;
	}
	.list-group-item ul li{

	padding-left: 2em; 
    text-indent: -.7em;
	}
	.list-group-item ul li:before{
		    content: "• ";
    color: #19bd9a;
	}
	.list-group-item{
		border:none !important;
		list-style:none;
	}
}

</style>
@section('content')
<h1>Update news</h1>
	<div class="row">
	 <div class="col-xs-12 col-sm-12">
		<ul class="list-group" id="myList">
				<li class="list-group-item">
				<h4>07/10/2016 Version 0,99</h4>
				<ul>
					<li>Almost ready for version 1. A new help article has been written <a href="http://fawour.com/help">here</a></li>
					<li>Green dot will appear if user is online</li>
					<li>New video tutorial on how to use the app <a href="https://www.youtube.com/watch?v=sE2dPr9yjao">here</a></li>

				</ul>
			</li>
			<li class="list-group-item">
				<h4>07/06/2016 Version 0,95</h4>
				<ul>
					<li>We are currently developing and collecting data for analytics!</li>
					<li>Sub menus on the sidebar has changed style.</li>
					<li>Chat has gotten a visual upgrade.</li>

				</ul>
			</li>
		<li class="list-group-item">
				<h4>06/9/2016 Version 0,8</h4>
				<ul>
					<li>You asked for it and we made it. Now you can message users you aren't partners with.</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>06/9/2016 Version 0,8</h4>
				<ul>
					<li>You can now upload a profile picture. - We no longer use Gravatar.</li>
					<li>"Explore" now suggest you to search on users on the same field as you.</li>
					<li>Adding partners gives a small warning if you aren't in the same field with the new partner.</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/29/2016 Version 0,7</h4>
				<ul>
					<li>Partners list now sorts by highest ratio</li>
					<li>Small help box pops up after 30 sec for everyone that haven't completed the "to do" list</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/19/2016 Version 0,65</h4>
				<ul>
					<li>You can now delete your profile</li>
					<li>We cleaned up on the dashboard</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/14/2016 Version 0,6</h4>
				<ul>
					<li>We have added a F.A.Q.</li>
					<li>There's now a few help links around the app</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/12/2016 Version 0,55</h4>
				<ul>
					<li>Cool update! Now with chat rooms for each categories</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/10/2016 Version 0,5</h4>
				<ul>
					<li>We updated the niche categories! Added lifestyle, food, traveling. Removed "pets". Changed "design" to "design and photography" </li>
					<li>For fun we show latest created user on the front page and latest logged in users on the dashboard.</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/07/2016 Version 0,4</h4>
				<ul>
					<li>We created a few more "what to do" on the home site.</li>
					<li>We created a burger bar for people on the mobile!</li>
					<li>Fixed a bug where visitors where counted wrong.</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/05/2016 Version 0,35</h4>
				<ul>
					<li>Fixed a bug - you can now delete links</li>
					<li>Fixed a bug where myLinks data "total count" where counted wrong.</li>
				</ul>
		</li>
		<li class="list-group-item">
				<h4>05/04/2016 Version 0,3</h4>
				<ul>
					<li>Added a small chat site function </li>
				</ul>
		</li>
		
			<li class="list-group-item">
				<h4>05/03/2016 Version 0,2</h4>
				<ul>
					<li>Added this update log.</li>
					<li>You can now view a users links without being partner with them.</li>
					<li>"Watch link" now opens in a new browser, as suggested.</li>
					<li>"Explore partners" now orders users by ratio from high to low! :) </li>
				</ul>
			</li>
		</ul>

	</div>
	</div>
	<p>We are listening. Suggest updates or features <a href="https://docs.google.com/forms/d/1RgqHG4U8C_5wu0CcKXZ_EwZZM5Fzs5Is676JyG3-nsc/viewform">here </a>, or join in our Facebook group <a href="https://www.facebook.com/groups/1733682206850523/">here</a> a place for creators to connect. Ask questions, post ideas and help each other grow. </p>

@stop