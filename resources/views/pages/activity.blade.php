@extends('layout')

<style>
	.hasRead{
		background-color:#e8e8e8 !important;
		color:#ADADAD !important;
	}
	.hasRead a, .hasRead a:hover{
		color:#91BBB2 !important;
	}
</style>
@section('content')

@include('pages.partials.menuprofile')
@if ($activity->isEmpty())
<h3> - No notifications yet</h3>
@endif
	<ul class="list-group">
		@include ("pages.partials.list2")
	</ul>
@stop