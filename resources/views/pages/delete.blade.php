@extends('layout')
<style>

.btn-primarydelete{
  background-color: red;
  color:#ffffff;
}
.btn-primarydontdelete{
  background-color: #19bd9a;
  color:#ffffff;
}
.deleteReal{
  display:none;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.min.css">
@section('content')
    <h1>You are about to delete your profile</h1>
    <div class="row">
        <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                 <div class="form-group">
                   <button class="btn btn-primarydelete">Delete profile</button>
                   <button  class="btn btn-primarydontdelete">No don't delete it</button>

           <form method="POST" action="/profile/delete/{{$user->id}}">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button class="deleteReal" id="deleteForReal">Delete your profile?</button>
           </form>
                    </div>
                
                    </div>

                </div>

   

             </div>
             </div>





@stop
<script>
document.addEventListener("DOMContentLoaded", function() {
    $( ".btn-primarydelete" ).click(function() {
        swal({   title: "Warning",   text: "You are about to delete your profile",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, function(){  
      document.getElementById("deleteForReal").click();
    });
        });
    $( ".btn-primarydontdelete" ).click(function() {
        window.location.replace("http://fawour.com");
    });


    });
</script>