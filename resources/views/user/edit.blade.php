@extends('layout')

<style>

.socialsettings .col-lg-6 .form-group{
    background-color:#ffffff;

}

.form-control{
    border-radius: 0px !important;


}
.onlyLink::-webkit-input-placeholder {
    color: #ebebeb !important;
}
.onlyLink:-moz-placeholder {
    /* FF 4-18 */
    color: #ebebeb !important;
}
.onlyLink::-moz-placeholder {
    /* FF 19+ */
    color: #ebebeb !important;
}
.onlyLink:-ms-input-placeholder {
    /* IE 10+ */
    color: #ebebeb !important;
}
.help-block{
    color:red !important;
}
</style>
@section('content')
   @include('pages.partials.menuprofile')
    <div class="row">
        <div class="col-lg-12">
            <form class="form-vertical" id="form" role="form" method="post" action"{{ route ('user.edit') }}">
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="ui horizontal divider header">
                            About you
                        </h4>
                         <div class="form-group">
                         <label for="name" class="control-label{{ $errors->has('name') ? ' has-error' : '' }}">Name</label>
                            <textarea type="text" rows="1" required name="name" class="form-control" id="name" placeholder="{{ old('name', $user->name ?: 'name') }}">{{ old('name', $user->name) }}</textarea>
                            <span>
                            
                            </span>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>  

                    <!-- description -->
                    <div class="form-group">
                            <label for="description" class="control-label{{ $errors->has('description') ? ' has-error' : '' }}">{{ trans('app.description') }}</label>
                            <textarea type="text" rows="5" onkeyup="validate()" required name="description" class="form-control" id="description" placeholder="{{ old('description', $user->description ?: 'Write something cool about yourself..') }}">{{ old('description', $user->description) }}</textarea>
                            <span>
                                    <strong id="helpBlockloength"></strong>
                            </span>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                   
                    <script>
                    function validate() {
                     var value = document.getElementById('description').value;
                     if (value.length < 550) {
                        document.getElementById("helpBlockloength").innerHTML = value.length + " / 550";
                        document.getElementById("helpBlockloength").style.color = "#19bd9a";
                     } else{
                        document.getElementById("helpBlockloength").innerHTML = value.length + " / 550";
                        document.getElementById("helpBlockloength").style.color = "red";
                     }
                    }
                    </script>
                    <!-- field -->
                    <div class="form-group">
                            <label for="field" class="control-label{{ $errors->has('field') ? ' has-error' : '' }}">{{ trans('app.contentnich') }} </label>
                            <select  name="field" class="form-control" id="field">

                                @foreach (App\Http\Utilities\Country::allfield() as $name => $code)
                                    
                                @if (Request::old("field") ?: $user->field == $code)
                                              <option value="{{ $code }}" selected>{{ $name }}</option>
                                        @else
                                              <option value="{{ $code }}">{{ $name }}</option>
                                        @endif
                                @endforeach 
                            </select>
                            @if ($errors->has('field'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('field') }}</strong>
                                </span>
                            @endif
                        </div>

                    <!-- lang -->
                    <div class="form-group">
                            <label for="lang" class="control-label{{ $errors->has('lang') ? ' has-error' : '' }}">{{ trans('app.contentlang') }}</label>
                            <select  name="lang" class="form-control" id="lang">

                                @foreach (App\Http\Utilities\Country::alllang() as $name1 => $code1)
                                    
                                @if (Request::old("lang") ?: $user->lang == $code1)
                                              <option value="{{ $code1 }}" selected>{{ $name1 }}</option>
                                        @else
                                              <option value="{{ $code1 }}">{{ $name1 }}</option>
                                        @endif
                                @endforeach 
                            </select>
                            @if ($errors->has('lang'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lang') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                   
                    </div>

                    <div class="col-lg-6 socialsettings">
                     <h4 class="ui horizontal divider header">
                            Your platforms
                        </h4>
                       <div class="col-lg-6">
                            <div class="form-group">
                                <div class="hasBlog">
                                    <label for="BlogUrl" class="control-label {{ $errors->has('BlogUrl') ? ' has-error' : '' }}">  <i class="fa fa-home" aria-hidden="true"></i>{{ trans('app.your') }} website link?</label>
                                    <input type="text" name="BlogUrl" class="form-control onlyLink" id="BlogUrl" placeholder="{{ old('BlogUrl', $user->BlogUrl ?: 'http://yourWebsite.com/') }}" value="{{ old('BlogUrl', $user->BlogUrl) }}">
                                    @if ($errors->has('BlogUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('BlogUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            
                        </div>
                                           <!-- Youtube -->
                            <div class="form-group">
                                <div class="hasyoutube">
                                    <label for="YoutubeUrl" class="control-label {{ $errors->has('YoutubeUrl') ? ' has-error' : '' }}"><i class="fa fa-youtube" aria-hidden="true"></i> {{ trans('app.your') }} Youtube link?</label>
                                    <input type="text" name="YoutubeUrl" class="form-control onlyLink" id="YoutubeUrl" placeholder="{{ old('YoutubeUrl', $user->YoutubeUrl ?: 'https://www.youtube.com/c/yourName') }}" value="{{ old('YoutubeUrl', $user->YoutubeUrl) }}">
                                    @if ($errors->has('YoutubeUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('YoutubeUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <!-- facebook -->
                            <div class="form-group">
                                <div class="hasFacebook">
                                    <label for="facebookUrl" class="control-label {{ $errors->has('facebookUrl') ? ' has-error' : '' }}"> <i class="fa fa-facebook" aria-hidden="true"></i> {{ trans('app.your') }} Facebook link?</label>
                                    <input type="text" name="facebookUrl" class="form-control onlyLink" id="facebookUrl" placeholder="{{ old('facebookUrl', $user->facebookUrl ?: 'https://www.facebook.com/yourName') }}" value="{{ old('facebookUrl', $user->facebookUrl) }}">
                                    @if ($errors->has('facebookUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('facebookUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>

                        </div>


                           <!-- instagram -->
                            <div class="form-group">
                                
                                <div class="hasInstagram">
                                    <label for="instagramUrl" class="control-label {{ $errors->has('instagramUrl') ? ' has-error' : '' }}"><i class="fa fa-instagram" aria-hidden="true"></i> {{ trans('app.your') }} Instagram link?</label>
                                    <input type="text" name="instagramUrl" class="form-control onlyLink" id="instagramUrl" placeholder="{{ old('instagramUrl', $user->instagramUrl ?: 'https://www.instagram.com/yourName') }}" value="{{ old('instagramUrl', $user->instagramUrl) }}">
                                    @if ($errors->has('instagramUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('instagramUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                            </div>
                        <div class="col-lg-6">
                       <!-- twitter -->
                            <div class="form-group">

                                <div class="hasTwitter">
                                    <label for="twitterUrl" class="control-label {{ $errors->has('twitterUrl') ? ' has-error' : '' }}"><i class="fa fa-twitter" aria-hidden="true"></i> {{ trans('app.your') }} Twitter link?</label>
                                    <input type="text" name="twitterUrl" class="form-control onlyLink" id="twitterUrl" placeholder="{{ old('twitterUrl', $user->twitterUrl ?: 'https://twitter.com/yourName') }}" value="{{ old('twitterUrl', $user->twitterUrl) }}">
                                    @if ($errors->has('twitterUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('twitterUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                                               <!-- Vine -->
                            <div class="form-group">

                                <div class="hasVine">
                                    <label for="vineUrl" class="control-label {{ $errors->has('vineUrl') ? ' has-error' : '' }}"><i class="fa fa-twitch" aria-hidden="true"></i> {{ trans('app.your') }} Twitch link?</label>
                                    <input type="text" name="vineUrl" class="form-control onlyLink" id="vineUrl" placeholder="{{ old('vineUrl', $user->vineUrl ?: 'https://twitch.tv/yourName') }}" value="{{ old('vineUrl', $user->vineUrl) }}">
                                    @if ($errors->has('vineUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('vineUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                                               <!-- pinterest -->
                            <div class="form-group">
        
                                <div class="hasPinterest">
                                    <label for="pinterestUrl" class="control-label {{ $errors->has('pinterestUrl') ? ' has-error' : '' }}"><i class="fa fa-pinterest" aria-hidden="true"></i> {{ trans('app.your') }} Pinterest link?</label>
                                    <input type="text" name="pinterestUrl" class="form-control onlyLink" id="pinterestUrl" placeholder="{{ old('pinterestUrl', $user->pinterestUrl ?: 'pinterest.com/yourName') }}" value="{{ old('pinterestUrl', $user->pinterestUrl) }}">
                                    @if ($errors->has('pinterestUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pinterestUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                                               <!-- tumblr -->
                            <div class="form-group">
                                <div class="hasTumblr">
                                    <label for="tumblrUrl" class="control-label {{ $errors->has('tumblrUrl') ? ' has-error' : '' }}"><i class="fa fa-tumblr" aria-hidden="true"></i> {{ trans('app.your') }} Tumblr link?</label>
                                    <input type="text" name="tumblrUrl" class="form-control onlyLink" id="tumblrUrl" placeholder="{{ old('tumblrUrl', $user->tumblrUrl ?: 'http://yourName.tumblr.com/') }}" value="{{ old('tumblrUrl', $user->tumblrUrl) }}">
                                    @if ($errors->has('tumblrUrl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('tumblrUrl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>


                    </div>
                    </div>
              
             
                </div>
                  <div class="form-group">
                  {{ csrf_field() }}
                    <button type="submit" style="padding:10px 50px;" class="btn btn-primary1">{{ trans('app.Update') }}</button>
                </div>

            </form>
             </div>
             @if($user->emailCode == "facebook" or $user->emailCode == "twitter" or $user->emailCode == "facebook")
            <h4 class="ui horizontal divider header">
                Social login
            </h4>
            <table class="ui definition table">
            <p>
                Because you logged in with  Facebook, Twitter or Google+ here's some info
            </p>
              <tbody>
                <tr>
                  <td>Normal login</td>
                  <td>To enable normal login, log out and reset password with email. Then you will be able to login without Facebook, Twitter or Google+</td>
                </tr>
              </tbody>
            </table>
             @endif

        </div>
        <span class ="helperrr" onClick="RunTourHere(2);" data-tooltip="Click here to take a tour on your information" id="tour1" ><i class="fa fa-question-circle-o"></i></span>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var initial_form_state = $('#form').serialize();
    $('#form').submit(function(){
      initial_form_state = $('#form').serialize();
    });
    $(window).bind('beforeunload', function(e) {
      var form_state = $('#form').serialize();
      if(initial_form_state != form_state){
        var message = "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        e.returnValue = message; // Cross-browser compatibility (src: MDN)
        return message;
      }
    });
});
</script>


@stop
