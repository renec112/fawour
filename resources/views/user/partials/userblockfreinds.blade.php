<div class="ui {{ $user2->returnColor($user2->field) }} image basic label large">
  <img src="{{ $user2->getpofilepic20() }}">
 <a href="{{ route('partner.profile', ['name' => $user2->name])}}">{{ $user2->getName()}} </a> @if($user2->isOnline()) <div class="floating ui fawourlab label">Online</div> @endif
  <div class="detail"><a href="/messages?from={{ $user2->id}}">Message</a></div>
  <div class="detail"><a href="/partner/{{ $user2->name}}/links">Share link</a></div>

</div>