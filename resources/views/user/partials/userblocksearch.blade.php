			<div class="ui card">
					  <div class="image">
					    <img src="{{ $user1->getpofilepic120() }}" onError="this.onerror=null;this.src='https://www.fawour.com/img/flyers/increase-visitors.jpg';">
					  </div>
					  <div class="content">
					    <a class="header" href="{{ route('partner.profile', ['name' => $user1->name])}}">{{ $user1->name }}<i class="twa  twa-{{$user::returnSmiley($user1->ratio)}}"></i></a>
					    @if($user1->isOnline()) <div class="floating ui fawourlab label">Online</div> @endif
					    <div class="meta">
					      <span class="date">Given: {{ $user1->pointsGiven }}, Gotten: {{ $user1->pointsGotten }}</span>
					    </div>
					    <div class="description">
					      {{ str_limit($user1->description, $limit = 70, $end = '...') }}
					    </div>
					  </div>
					  <div class="extra content">
					    <a>
					      <i class="user "></i>
					      <a class="ui basic {{ $user1->returnColor($user1->field) }} label">{{App\Http\Utilities\Country::getFieldName($user1->field)}}</a>
					      <a class="ui grey basic label">{{App\Http\Utilities\Country::getLanguageName($user1->lang)}}</a>
					      <div class="ui grey basic label">{{ Carbon\Carbon::parse($user1->last_login)->diffForHumans() }}</div>
					    </a>
					  </div>
					</div>
