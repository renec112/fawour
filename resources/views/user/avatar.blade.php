@extends('layout')
@section('content')
<style>
.help-block{
    color:red !important;
}
#preview img{
  width:150px;
  height:150px;
  border-radius:50%;
  margin-right:5px;
}
#preview{
  width:100%;
}
</style>
    @include('pages.partials.menuprofile')
    <div class="row">
        <div class="col-lg-12">
            <form class="form-vertical" enctype="multipart/form-data" role="form"  method="post" action"{{ route ('user.avatar') }}">
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ $user->getpofilepic() }}" class="roundprofilepic" style="width:150px; height:150px; border-radius:50%;" />
            <div class="form-group">
                <input type="file" required="required" id="exampleInputFile" name="avatar">
                <p class="">{{ trans('app.imagesett') }}</p>
                  @if ($errors->has('image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif
              </div>
                <div class="form-group" style="margin-left:10px;">
                  {{ csrf_field() }}
                    <button type="submit" id="cekJpg" class="btn btn-primary1">{{ trans('app.updateimage') }}</button>
                </div>
                    </div>
                     <div class="col-md-6">
                        <div id="preview"></div>
                     </div>
                </div>


              
                
                </div>

            </form>
            
             </div>
        </div>
<script>

document.addEventListener("DOMContentLoaded", function() {
window.URL    = window.URL || window.webkitURL;
var elBrowse  = document.getElementById("exampleInputFile"),
    elPreview = document.getElementById("preview"),
    useBlob   = false && window.URL; // `true` to use Blob instead of Data-URL


// 2.
function readImage (file) {

  // 2.1
  // Create a new FileReader instance
  // https://developer.mozilla.org/en/docs/Web/API/FileReader
  var reader = new FileReader();

  // 2.3
  // Once a file is successfully readed:
  reader.addEventListener("load", function () {

    // At this point `reader.result` contains already the Base64 Data-URL
    // and we've could immediately show an image using
    // `elPreview.insertAdjacentHTML("beforeend", "<img src='"+ reader.result +"'>");`
    // But we want to get that image's width and height px values!
    // Since the File Object does not hold the size of an image
    // we need to create a new image and assign it's src, so when
    // the image is loaded we can calculate it's width and height:
    var image  = new Image();
    image.addEventListener("load", function () {

      // Concatenate our HTML image info 
      var imageInfo = file.name    +' '+ // get the value of `name` from the `file` Obj
                      image.width  +'×'+ // But get the width from our `image`
                      image.height +' '+
                      file.type    +' '+
                      Math.round(file.size/1024) +'KB';

      // Finally append our created image and the HTML info string to our `#preview`
      $('#preview').html('');
      elPreview.appendChild( this );
      elPreview.insertAdjacentHTML("beforeend", imageInfo +'<br>');
    });

    image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;

    // If we set the variable `useBlob` to true:
    // (Data-URLs can end up being really large
    // `src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAA...........etc`
    // Blobs are usually faster and the image src will hold a shorter blob name
    // src="blob:http%3A//example.com/2a303acf-c34c-4d0a-85d4-2136eef7d723"
    if (useBlob) {
      // Free some memory for optimal performance
      window.URL.revokeObjectURL(file);
    }
  });

  // 2.2
  // https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
  reader.readAsDataURL(file);  

}

// 1.
// Once the user selects all the files to upload
// that will trigger a `change` event on the `#browse` input
elBrowse.addEventListener("change", function() {

  // Let's store the FileList Array into a variable:
  // https://developer.mozilla.org/en-US/docs/Web/API/FileList
  var files  = this.files;
  // Let's create an empty `errors` String to collect eventual errors into:
  var errors = "";

  if (!files) {
    errors += "File upload not supported by your browser.";
  }

  // Check for `files` (FileList) support and if contains at least one file:
  if (files && files[0]) {

    // Iterate over every File object in the FileList array
    for(var i=0; i<files.length; i++) {

      // Let's refer to the current File as a `file` variable
      // https://developer.mozilla.org/en-US/docs/Web/API/File
      var file = files[i];

      // Test the `file.name` for a valid image extension:
      // (pipe `|` delimit more image extensions)
      // The regex can also be expressed like: /\.(png|jpe?g|gif)$/i
      if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
        // SUCCESS! It's an image!
        // Send our image `file` to our `readImage` function!
        readImage( file ); 
      } else {
        errors += file.name +" Unsupported Image extension\n";  
      }
    }
  }

  // Notify the user for any errors (i.e: try uploading a .txt file)
  if (errors) {
    $("#exampleInputFile").replaceWith($("#exampleInputFile").val('').clone(true));
    swal({   title: "Error",   text: errors,   timer: 2000,   showConfirmButton: false });
    $("#exampleInputFile").replaceWith($("#exampleInputFile").val('').clone(true));
  }

});
    });
</script>

@stop
