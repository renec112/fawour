@extends('layout')

@section('content')
     @include('pages.partials.menulinks')


<div class="ui grid">
  <div class="twelve wide column">
        <form method="post" action="/urlcreate" enctype="multipart/form-data">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('urls.form')
    </form>
  </div>
  <div class="four wide column">
         <div class="ui card">
                    <div class="image thisImageRightHere">
                    <div class="ui active inverted dimmer">
                      <div class="ui text "></div>
                    </div>
                    <img class="ui medium image " src="/img/wireframe.png"/>
                  </div>
      
                <div class="content">
                  <div class="header">
                    Link by <a href="https://www.fawour.com/partner/{{Auth::user()->name}}" >{{Auth::user()->name}}</a>
                  </div>
                  <div id="beksrivelse" class="meta">
                    
                  </div>                 
                </div>
                <a class="inspectThisLink" href="" target="_blank"><div class="ui bottom attached button">
              <i class="fa fa-external-link" aria-hidden="true"></i>
             See link
    </div></a>
              </div>  
    <!--<div>Meta Keyword: <div id="kw"></div></div>-->
  </div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
  $('#url').keyup(function(e){
    if (e.keyCode == 17) // Ctrl key, fires at Ctrl's keyup.
    return false;
    theUrl = $('#url').val();
    var theDiv = $( '.thisImageRightHere' );
    var finalCheck = getYahoo(theUrl,theDiv);
    });
    $('#description').keyup(function(){
      document.getElementById('beksrivelse').innerHTML = document.getElementById('description').value;
      
    });
});
</script>
<script src="/js/link.js"></script>
    <span class ="helperrr" data-position="top left" onClick="RunTourHere(3);" data-tooltip="{{ trans('app.taketourlinkss') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>
@stop