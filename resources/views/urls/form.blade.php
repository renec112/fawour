<div class="ui form">
{{ csrf_field() }}
<div class="field">
    <label for="url">Link</label>
    <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}" placeholder="{{ trans('app.copyandpsate') }}" required>
</div>


<div class="field">
    <label for="description">{{ trans('app.description') }}</label>
            <textarea type="text" name="description" id="description" class="form-control" rows="2" required placeholder="{{ trans('app.helpyoupart') }}"></textarea>
</div>
<!-- Form Submit -->
<div class="form-group">
    <button type="submit" class="btn btn-primary1">{{ trans('app.cratethelin') }}</button>
</div>
</div>

