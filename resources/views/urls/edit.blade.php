@extends('layout')
<style>

.socialsettings .col-lg-6 .form-group{
    padding:20px;
    background-color:#ffffff;

}
.socialsettings .col-lg-6{
    padding:0;

}
.form-control{
    border-radius: 0px !important;


}
</style>
@section('content')
    <h1>Update link description</h1>
    <p>For link: {{$link->url}}</p>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-vertical" role="form" method="post" action"{{ route ('edit_url', array("id" => $link->id)) }}">
                <div class="row">
                    <div class="col-lg-6">

                    <!-- description -->
                    <div class="form-group">
                            <label for="description" class="control-label{{ $errors->has('description') ? ' has-error' : '' }}">Description</label>
                            <textarea type="text" rows="5" required name="description" class="form-control" id="description" placeholder="{{$link->description}}">{{$link->description}}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
               <div class="form-group" style="margin-left:10px;">
                  {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary1">Update</button>
                </div>
                                      
                    </div>

         
              
                </div>

            </form>
            
             </div>
        </div


@stop
