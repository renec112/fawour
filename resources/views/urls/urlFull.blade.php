@extends('layout')

@section('content')
<style>
.getWhitebg{
	background-color:#ffffff;
}
</style>
<div class="row">
	<div class="col-md-6 getWhitebg">
	<h1>Data for <a href="{{ $urlfull->url }}"><?php
					$showtheurl = $urlfull->url;
					$showtheurl = str_replace('www.', '', $showtheurl);
					$showtheurl = str_replace('https://.', '', $showtheurl);
					$showtheurl = str_replace('http://.', '', $showtheurl);
					echo $showtheurl; ?></a></h1>
	<p><span class="smalldetail getitalic">{{ $urlfull->description }}</span></p>
	<h3>Total visitors: {{ $urlfull->count }}</h3>
	</div>
	<div class="col-md-6 getWhitebg">
		<h3>Visitors given</h3>
	 <form class="form-vertical" role="form" method="post" action"{{ route ('postFull', $urlfull->id) }}">
     <table class="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Visitors given </th>
                <th>Can share this link</th>
              </tr>
            </thead>
            <tbody>
		@foreach($partnerData as $user1)

              
			@include("user/partials/userblockUrlData")
      </label>
                

		
                   <input type="hidden" class="xxx{{$user1->trafic_from_user}}" name="user[{{$user1->trafic_from_user}}]" value="{{$user1->can_be_shared}}" multiple="multiple"/>
                    <input type="hidden" name="linkid" value="{{$urlfull->id}}"  multiple="multiple"/>
          
		@endforeach
    </td>

              </tr>
          
            </tbody>
          </table>
		    {{ csrf_field() }}
		    <br>
                    <button type="submit" class="btn btn-primary1">{{ trans('app.Update') }}</button>
	</form>
	</div>
</div>
<script>
	function convert(element){
		console.log(element);
		if(element.value == 1){
			element.value = "0";
			document.querySelector("." + element.id).value = "0";
		} else if(element.value == 0){
			element.value = "1";
			document.querySelector("." + element.id).value = "1";
		}


	}
</script>
<style>

.tgl {
  position: relative;
  outline: 0;
  display: inline-block;
  cursor: pointer;
  user-select: none;
  margin: 0 0 5px 0;

}
.tgl,
.tgl:after,
.tgl:before,
.tgl *,
.tgl *:after,
.tgl *:before,
.tgl + .tgl-btn {
  box-sizing: border-box;
}
.tgl::selection,
.tgl:after::selection,
.tgl:before::selection,
.tgl *::selection,
.tgl *:after::selection,
.tgl *:before::selection,
.tgl + .tgl-btn::selection {
  background: none;
}
.tgl span {
  position: relative;
  display: block;
  height: 1.8em;
  line-height: 1.2em;
  overflow: hidden;
  font-weight: normal;
  text-align: center;
  border-radius: 2em;
  padding: 0.2em 1em;
  border: 1px solid #fafafa;
  box-shadow: inset 0 2px 0 rgba(0, 0, 0, 0.2), 0 2px 0 rgba(255, 255, 255, 0.7);
  transition: color 0.3s ease, padding 0.3s ease-in-out, background 0.3s ease-in-out;
}
.tgl span:before {
  position: relative;
  display: block;
  line-height: 1.3em;
  padding: 0 0.2em;
  font-size: 1em;
}
.tgl span:after {
  position: absolute;
  display: block;
  content: '';
  border-radius: 2em;
  width: 1.3em;
  height: 1.3em;
  margin-left: -1.45em;
  top: 0.2em;
  background: #FFFFFF;
  transition: left 0.3s cubic-bezier(0.175, 0.885, 0.32, 0.97), background 0.3s ease-in-out;
}
.tgl input[type="checkbox"] {
  display: none !important;
}
.tgl input[type="checkbox"]:not(:checked) + span {
  background: #de474e;
  color: #FFFFFF;
  padding-left: 1.6em;
  padding-right: 0.4em;
}
.tgl input[type="checkbox"]:not(:checked) + span:before {
  content: attr(data-off);
  color: #FFFFFF;
}
.tgl input[type="checkbox"]:not(:checked) + span:after {
  background: #FFFFFF;
  left: 1.6em;
}
.tgl input[type="checkbox"]:checked + span {
  background: #19bd9a;
  color: #FFFFFF;
  padding-left: 0.4em;
  padding-right: 1.6em;
}
.tgl input[type="checkbox"]:checked + span:before {
  content: attr(data-on);
}
.tgl input[type="checkbox"]:checked + span:after {
  background: #FFFFFF;
  left: 100%;
}
.tgl input[type="checkbox"]:disabled,
.tgl input[type="checkbox"]:disabled + span,
.tgl input[type="checkbox"]:read-only,
.tgl input[type="checkbox"]:read-only + span {
  cursor: not-allowed;
}
.tgl-gray input[type="checkbox"]:not(:checked) + span {
  background: #e3e3e3;
  color: #999999;
}
.tgl-gray input[type="checkbox"]:not(:checked) + span:before {
  color: #999999;
}
.tgl-gray input[type="checkbox"]:not(:checked) + span:after {
  background: #ffffff;
}
.tgl-inline {
  display: inline-block !important;
  vertical-align: top;
}
.tgl-inline.tgl {
  font-size: 16px;
}
.tgl-inline.tgl span {
  min-width: 50px;
}
.tgl-inline.tgl span:before {
  line-height: 1.4em;
  padding-left: 0.4em;
  padding-right: 0.4em;
}
.tgl-inline-label {
  display: inline-block !important;
  vertical-align: top;
  line-height: 26px;
}

.simple-toggle {
  position: absolute;
  left: 0;
  right: 0;
  top: 30px;
  text-align: center;
  margin: auto;
}
.title,
.subtitle {
  display: block;
  -webkit-font-smoothing: antialiased !important;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.004);
}
.title {
  font-size: 30px;
  line-height: 40px;
  font-weight: bold;
  color: #262626;
}
.subtitle {
  margin-bottom: 20px;
  opacity: 0.4;
}
.code {
  display: inline-block;
  color: grey;
  border-radius: 5px;
  border: 1px solid #c9c9c9;
  background-color: #d6d6d6 ;
  margin: 10px;
  text-align: left;
  padding: 10px 10px 0 0;
  line-height: 0.6em;
  max-width: 300px;
}
.tgl span{
    border:none;
    box-shadow: none;
}
.btn-primarydelete{
  background-color: red;
  color:#ffffff;

  margin-top:650px;
}
</style>
@stop