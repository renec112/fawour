@extends('layout')
<style>
</style>
@section('content')
<div class="row">
	 <div class="col-md-12 getWhite">
   <h1>Links by <a href="{{URL::route('partner.profile', array('name' => $partner->name ))}}">{{$partner->name}}</a> </h1>
	 	@if (!$url_database->count())

    @else
      <div class="ui four stackable cards">
      @foreach($url_database as $url_single)
       <div class="card">
                     <div class="image thisImageRightHere">
                    <div class="ui active inverted dimmer">
                      <div class="ui text loader"></div>
                    </div>
                    <img class="ui medium image " src="/img/wireframe.png"/>
                  </div>
                <div class="content" id="linkAbout">
                  <div class="meta" id="linkLinks" >
                    <a href="{{$url_single->url}}" class="inspectThisLink" >
                <?php
        $showtheurl = $url_single->url;
        $showtheurl = str_replace('www.', '', $showtheurl);
        $showtheurl = str_replace('https://', '', $showtheurl);
        $showtheurl = str_replace('http://', '', $showtheurl);

        $showtheurl = mb_strimwidth($showtheurl, 0, 30,  "....");
        echo $showtheurl; ?>.  {{ str_limit($url_single->description, $limit = 50, $end = '...') }}</a>
                  </div>
                   <div class="description" id="desLinks">
                    
                  </div>
                  <div class="ui label basic" id="givenLinks"> {{ trans('app.Visitorsgotten') }}: {{$url_single->count}}</div>
   
                </div>
              </div>              

      @endforeach
      </div>
      {!! $url_database->render() !!}
	@endif
        </div>
      </div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    $(".card").each(function(){
      thisDiv = $( this ).find( '.thisImageRightHere' )
      var checkThisLink = $( this ).find( '.inspectThisLink' ).attr("href"); 
      var finalCheck = getYahoo(checkThisLink, thisDiv);      
  });
});
</script>
<script src="/js/link.js"></script>

@stop