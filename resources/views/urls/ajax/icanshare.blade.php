
@include('pages.partials.menulinks')
<style>

.copyHack{
  display:none;
}

.card{
  overflow:hidden;
}
.ui[class*="right labeled"].icon.button, .ui[class*="right labeled"].icon.button{
  padding:10px !important;
}

</style>
<div class="row">
	 

	<div class="col-md-12 elementsWrapper">
         <span class ="helperrr"  data-position="top left" onClick="RunTourHere(6);" data-tooltip="{{ trans('app.taketoursharelinks') }}" id="tour1" ><i class="fa fa-question-circle-o"></i></span>
 	@if (!$userCanShare->count())
	    <p>{{ trans('app.nolmiksforyou') }}</p>
    @else
      <div class="ui three stackable cards">
      @foreach($userCanShare as $url_single)
       <div class="card">
         <div class="image thisImageRightHere">
                    <div class="ui active inverted dimmer">
                      <div class="ui text loader"></div>
                    </div>
                    <img class="ui medium image " src="/img/wireframe.png"/>
                  </div>
           
                <div class="content" id="linkAbout">

                  <div class="header"  id="byLinks"> 
                       By <a href="../partner/{{ $url_single->name }}">{{ $url_single->name }}
                  </div>
                  <div class="meta" id="linkLinks">
                    <a class="inspectThisLink" href="{{$url_single->url}}" >                <?php
        $showtheurl = $url_single->url;
        $showtheurl = str_replace('www.', '', $showtheurl);
        $showtheurl = str_replace('https://', '', $showtheurl);
        $showtheurl = str_replace('http://', '', $showtheurl);

        $showtheurl = mb_strimwidth($showtheurl, 0, 30,  "....");
        echo $showtheurl; ?></a>. {{ str_limit($url_single->description, $limit = 50, $end = '...') }}
                  </div>
                   <div class="description" id="desLinks">
                     
                  </div>
                  <div class="ui label basic" id="givenLinks"> {{ trans('app.Visitorsgiven') }}: {{$url_single->countgiven}}</div>   
                </div>
                <div  class="extra content">


<p id="p{{$url_single->code}}" class="copyHack">https://fawour.com/l/{{$url_single->code}}</p>
  <div class="ui action input" id="shareLinks" >
  <input type="text" value="https://fawour.com/l/{{$url_single->code}}">
  <button class="ui teal right labeled icon button" onclick="copyToClipboard('#p{{$url_single->code}}')" >
    <i class="fa fa-files-o" aria-hidden="true"> </i>  Copy
  </button>
</div>
<br><br>
<div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="https://fawour.com/l/{{$url_single->code}}" data-a2a-title="{{$url_single->description}}" data-a2a-icon-color="#b2b3b8">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_button_tumblr"></a>
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
</div>


                </div>
              </div>              

      @endforeach
      </div>
      {!! $userCanShare->render() !!}
    @endif

</div>

<script>
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  swal({   title: "{{ trans('app.ShareLinkCopied') }}",  timer: 1500,   showConfirmButton: false });
}
document.addEventListener("DOMContentLoaded", function() {

    $(".card").each(function(){
      thisDiv = $( this ).find( '.thisImageRightHere' )
      var checkThisLink = $( this ).find( '.inspectThisLink' ).attr("href"); 
       var finalCheck = getYahoo(checkThisLink, thisDiv);
      
  });
});
</script>
<script src="/js/link.js"></script>
<!-- AddToAny BEGIN -->

<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->