@extends('layout')

@section('content')
<style>
  .thiscard{
    color:#19bd9a !important;
    box-shadow:0 0 0 1px #19bd9a inset !important;
  }
</style>
    @include('pages.partials.menulinks')
<div class="ui ignored warning message">Beta: Here you can watch links created by users on the site, in your field. Explore new partners by their content.</div>
 <div class="ui four stackable cards">
                @foreach($linkexplore as $entry)
            
             <div class="ui card">
                    <div class="image thisImageRightHere">
                    <div class="ui active inverted dimmer">
                      <div class="ui text loader"></div>
                    </div>
                    <img class="ui medium image " src="/img/wireframe.png"/>
                  </div>
            <div class="floating ui {{ $user->returnColor($entry->field) }} label">{{ $user->returnColor($entry->field) }}</div>
                <div class="content">
                  <div class="header">
                    Link by <a href="https://www.fawour.com/partner/{{$entry->name}}" >{{$entry->name}}</a>
                  </div>
                  <div class="meta">
                    {{$entry->description}}
                  </div>                 
                </div>
                <a class="inspectThisLink" href="{{$entry->url}}" target="_blank"><div class="ui bottom attached button">
              <i class="fa fa-external-link" aria-hidden="true"></i>
             See link
    </div></a>
              </div>                 

                @endforeach
                  </div>

<script>
document.addEventListener("DOMContentLoaded", function() {

    $(".card").each(function(){
      thisDiv = $( this ).find( '.thisImageRightHere' )
      var checkThisLink = $( this ).find( '.inspectThisLink' ).attr("href"); 
      var finalCheck = getYahoo(checkThisLink, thisDiv);

  });
});

</script>
<script src="/js/link.js"></script>
@stop