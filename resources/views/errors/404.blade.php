<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Rene Czepluch" />
    <meta name="keywords" content="@yield('keywords')" />
    <link rel="stylesheet" href="/css/libs.css">
   <link rel="stylesheet" href="/css/app.css">
   <meta name="csrf-token" content="<?= csrf_token() ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/mystyles.css">
    <link href="/css/emoji.css" rel="stylesheet">
    <link rel="icon" href="https://fawour.com/img/flyers/icon.png">
    <meta property="og:url" content="{{ str_replace('http://', 'https://', Request::url()) }}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@hasSection('Image')@yield('Image') @else http://fawour.com/img/flyers/exchange-visitors-fawour.gif @endif" />
    <meta name="google-site-verification" content="gV1maLMH4ds34A2a7iUQRgaw0eV_Ih4UolhXExvdV-A" />
    <script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '74e200f6a38466d60592292eaeedcf098d88a607');
</script>
</head>
<body>
 <style type="text/css">
 .pager{
  list-style:none;
  text-align: center;
  margin:30px 0;
 }.pager li{
    display:inline;
    padding:10px 14px;
    background-color:#f3f3f3;
 }
 .ui.loader:after, .ui.loader:before{
  top:50%;
 }
.ui.inverted.loader:after{
  border-top-color:#0c0c0c;
}
.ui.inverted.loader:before{
  border-color:rgba(19, 19, 19, 0.48);
}
 .ui.label{
  margin-bottom: 10px;
 }
 .fawourlab{
  background-color:#19bd9a !important;
  color:#ffffff !important;
 }
 .ui.toggle.checkbox input:checked~.box:before, .ui.toggle.checkbox input:checked~label:before{
     background-color:#19bd9a !important;
 }
 .ui.teal.button{
  background-color:#19bd9a !important;

}
.ui.teal.button a {
  color:#ffffff;
}
.button .fa{
  font-size:1em;
}

    .smallmenudiv h1{
      font-size:4em;
    }
    .ui.secondary.pointing.menu .active.item, .ui.secondary.vertical.pointing.menu .active.item{
    color:#19bd9a;
    border-color:#19bd9a;
    }
    .hidden.menu {
      display: none;
    }
    .masthead.segment {
      min-height: 700px;
      padding: 1em 0em;
    }
    .masthead .logo.item img {
      margin-right: 1em;
    }
    .masthead .ui.menu .ui.button {
      margin-left: 0.5em;
    }
    .masthead h1.ui.header {
      margin-top: 3em;
      margin-bottom: 0em;
      font-size: 4em;
      font-weight: normal;
    }
    .masthead h2 {
      font-size: 1.7em;
      font-weight: normal;
    }
    .ui.vertical.stripe {
      padding: 8em 0em;
    }
    .ui.vertical.stripe h3 {
      font-size: 2em;
    }
    .ui.vertical.stripe .button + h3,
    .ui.vertical.stripe p + h3 {
      margin-top: 3em;
    }
    .ui.vertical.stripe .floated.image {
      clear: both;
    }
    .ui.vertical.stripe p {
      font-size: 1.33em;
    }
    .ui.vertical.stripe .horizontal.divider {
      margin: 3em 0em;
    }

    .quote.stripe.segment {
      padding: 0em;
    }
    .quote.stripe.segment .grid .column {
      padding-top: 5em;
      padding-bottom: 5em;
    }

    .footer.segment {
      padding: 5em 0em;
    }

    .secondary.pointing.menu .toc.item {
      display: none;
    }
    /*  - - - - - -- my styles - - - - - -  - - -*/

    .main.ui.secondary.pointing.menu, .ui.container{
      border:none;
    }
    a{
      color:#19bd9a;
    }
    a:hover, .ui.card>.content>a.header:hover, .ui.cards>.card>.content>a.header:hover{
      transition:0.5s;
      color:#ddd;

    }
    .detail a{
      color:rgba(0,0,0,.6);
    }
    .ui.image.label .detail{
      background-color: #ffffff;
    }

    @media only screen and (max-width: 700px) {
      .ui.fixed.menu {
        display: none !important;
      }
      .secondary.pointing.menu .item,
      .secondary.pointing.menu .menu,
      .ui.inline.dropdown.namemenu {
        display: none;
      }
      .secondary.pointing.menu .toc.item {
        display: block;
      }
      .masthead.segment {
        min-height: 350px;
      }
      .masthead h1.ui.header {
        font-size: 2em;
        margin-top: 1.5em;
      }
      .masthead h2 {
        margin-top: 0.5em;
        font-size: 1.5em;
      }
    }


  </style>
<div class="ui vertical sidebar menu">
<a class="item" href="/"><b>{{ trans('navi.Home') }}</b></a>
</div>
<div class="pusher">
  <div class="ui  vertical masthead center aligned ">

    <div class="ui container">
      <div class="ui large secondary  main pointing menu">
        <a class="toc item">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </a>
        <a class="{{ Request::is('/') ? 'active' : '' }} item" href="/">{{ trans('navi.Home') }}</a>

        <div class="right item">
        </span>
        </div>
      </div>
      <style>
      .ui.primary.button, .ui.primary.buttons .button{
            background-color:#19bd9a !important;
      }
      </style>
    <div class="ui basic segment">
      <div id="theloader" class="ui loader  inverted dimmer"></div>
        <div id="ajaxfield">
        <h1>That's an error. </h1>
        <h3>Crap! - Sorry. Something went wrong. If this keeps happening, please contact us</h3>
<a href="https://docs.google.com/forms/d/e/1FAIpQLSez07_zXZHcpJoGlLaJ_QAtXNr2gxyKks5qWHXs3NrK6s_0qQ/viewform"><button class="ui primary button">
  Contact
</button></a>
<a href="/"><button class="ui button">
  Home
</button></a>
        </div>
      </div>
   </div>
</div>

<script src="/js/libs.js"></script>
<script src="/js/js.js"></script>
@include('flash')
<script>

      // create sidebar and attach to menu open
      $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
      ;

    $('.ui.dropdown')
    .dropdown()
  ;

function startTour(where){

    $.getScript( "/js/{{ trans('navi.theTour') }}.js" )
  .done(function( script, textStatus ) {
    if(where == null){

            if (tour.ended()) {
              tour.restart();
            } else {
              tour.init();
              tour.start();
        }
    }else{
    
        if (tour.ended()) {

              tour.restart();
              tour.goTo(where);
        } else {

              tour.init();
              tour.goTo(where);
        }
    }
    
  })
  .fail(function( jqxhr, settings, exception ) {
});
   
}
function RunTourHere(theId)
   {
    window.location.href.split('#')[0]
    history.pushState(null, null, '#tour' + theId);
    tjeckhash();
   }
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50238279-11', 'auto');
  ga('send', 'pageview');
</script>
</body>


</html>
