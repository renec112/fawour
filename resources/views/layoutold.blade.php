<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Rene Czepluch" />
    <meta name="keywords" content="@yield('keywords')" />
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/libs.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/mystyles.css">
    <link href="/css/emoji.css" rel="stylesheet">
    <link rel="icon" href="http://fawour.com/img/flyers/icon.png">
    <meta property="og:url" content="{{ str_replace('http://', 'https://', Request::url()) }}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@hasSection('Image')@yield('Image') @else http://fawour.com/img/flyers/exchange-visitors-fawour.gif @endif" />
    <meta name="google-site-verification" content="gV1maLMH4ds34A2a7iUQRgaw0eV_Ih4UolhXExvdV-A" />
</head>
<body>
  
<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=65002,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
<link href='https://fonts.googleapis.com/css?family=Lato:400' rel='stylesheet' type='text/css'>
<div class="page-container">
    <!-- top navbar -->
    <div class="navbar" role="navigation">
       <div class="container">
           <div class="navbar-header">
      
               <div class="col-xs-6 col-sm-3"><a class="navbar-brand" href="/">Fawour</a></div>
            </div>
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".dataToggleThisbar">
                <span class="sr-only">{{ trans('navi.toggle') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
        </div>
            <div id="navbar" class="navbar-collapse collapse dataToggleThisbar">
                <ul class="nav navbar-nav">
                @if ($signedIn)
                    <li class="{{ Request::is('profile/edit') ? ' currentUrl' : '' }}"><a href="{{ route('user.edit') }}"><i class="fa fa-cog" aria-hidden="true"></i>{{ trans('navi.udpateprofile') }} </a></li>
                @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if ($signedIn)
               <li class="active" i><a href="/links/share">
                    <i class="fa fa-share-square-o" aria-hidden="true"></i> {{ trans('app.shareparnterslnisk') }}
                    </a>
                </li>
                              <li class="active" id="taketour"><a href="#/">
                    <i class="fa fa-info" aria-hidden="true"></i>{{ trans('navi.help') }}</a>
                </li>
                        <li class="@if($user->notifications > 0)hasNotfications @endif
                            
                            "><a href="{{ route('notifications') }}"><i class="fa fa-bell" aria-hidden="true"></i></span> {{ trans('navi.Notifications') }} @if($user->notifications > 0)<span class="badge">{{$user->notifications}}</span> @endif</a></li> 
                        <li class="imgfieldtop"><p class="topiamgeicon"><div class="circle_the_text"><img class="media-object circle_the_text" alt="" class="" src="{{ $user->getpofilepic20() }}"></div></p></li>
                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                   {{ trans('navi.hello') }} {{ $user->name }} <span class="caret"></span>
                                </a>
                        
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/faq"><i class="fa fa-info-circle" aria-hidden="true"></i> {{ trans('navi.faq') }}</a></li>
                                    <li><a href="/settings"><i class="fa fa-cogs" aria-hidden="true"></i> {{ trans('navi.Usersettings') }}</a></li>
                                    <li ><a href="/updates"><i class="fa fa-heart-o" aria-hidden="true"></i> {{ trans('navi.updatelog') }}</a></li>
                                    <li><a href="/logout"><i class="fa fa-power-off" aria-hidden="true"></i> {{ trans('navi.Logout') }}</a></li>
                                    
                                </ul>
                        </li>

                    @else
                         <li class="{{ Request::is('more') ? ' currentUrl' : '' }}">
                            <p class="navbar-btn">
                               <a href="/more" class="btn ">{{ trans('navi.more') }}</a>
                            </p>
                        </li>
                       <li class="{{ Request::is('faq') ? ' currentUrl' : '' }}">
                            <p class="navbar-btn">
                               <a href="/faq" class="btn ">{{ trans('navi.faq') }}</a>
                            </p>
                        </li>
                        <li class="{{ Request::is('login') ? ' currentUrl' : '' }}">
                            <p class="navbar-btn">
                               <a href="/login" class="btn ">{{ trans('navi.Login') }}</a>
                            </p>
                        </li>
                         <li>
                            <p class="navbar-btn">
                               <a href="/register" class="btn ">{{ trans('navi.Signup') }}</a>
                            </p>
                        </li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
       </div>
    </div>
      
    <div class="container">
      <div class="row marginthing flexbox">
        @if ($signedIn)
        <!-- sidebar -->
        <div class="col-xs-6 col-sm-12 sidebar-offcanvas dataToggleThisbar navbar-collapse" id="sidebar" role="navigation" >
       
            <ul class="nav removethisthing">
                <li class="imageleftbar">
                <h1> <div class="circle_the_text imgbarside"><img class="media-object circle_the_text"  class="" src="{{ $user->getpofilepic80() }}"></div></h1>

                </li>
                <li class="active {{ Request::is('/') ? 'currentUrl' : '' }}" ><a href="/">
                    <i class="fa fa-home" aria-hidden="true"></i> {{ trans('navi.Home') }}</a>
                </li>
                <li class="{{ Request::is('messages') ? ' currentUrl' : '' }}"><a href="{{ route('messages') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i>  {{ trans('navi.Messages') }}</a></li>
                <li class="active {{ Request::is('partners/explore') ? ' currentUrl' : '' }}"><a href="{{ route('partners.explore') }}"><i class="fa fa-users" aria-hidden="true"></i> </span> {{ trans('navi.Partners') }}</a></li>
                <li class="active {{ Request::is('profile') ? ' currentUrl' : '' }}"><a href="/partner/{{$user->name}}"><i class="fa fa-user" aria-hidden="true"></i> {{ trans('navi.myprofile') }}</a></li>
                
        
                <li class="active {{ Request::is('links/') ? ' currentUrl' : '' }}" ><a href="{{ route('myUrls') }}"><i class="fa fa-share-alt" aria-hidden="true"></i> {{ trans('navi.Links') }}</a></li>

                 <li class="active {{ Request::is('analytics/visitors') ? ' currentUrl' : '' }}"><a href="/analytics/visitors"><i class="fa fa-bar-chart" aria-hidden="true"></i> {{ trans('navi.Analytics') }}</a></li>
                

                  <li class="active {{ Request::is('chat') ? 'currentUrl' : '' }}"><a href="/chat">
                    <i class="fa fa-commenting-o" aria-hidden="true"></i> Chat</a>
                </li>
      

   
    
            </ul>
        </div>
        @endif
 
        <!-- main area -->
        <div class="col-xs-12 col-sm-10 contentholder">
@if ($signedIn)
        <ol class="breadcrumb">
        <script>
        var path = "";
        var href = document.location.href;
        var s = href.split("/");
        for (var i=2;i<(s.length-1);i++) {
        path+="<li class='activeUrl'><A HREF=\""+href.substring(0,href.indexOf("/"+s[i])+s[i].length+1)+"/\">"+s[i]+"</A></li>";
        }
        i=s.length-1;
        path+="<li ><A HREF=\""+href.substring(0,href.indexOf(s[i])+s[i].length)+"\">"+s[i]+"</A></li>";

        var url = window.location.protocol + "" + path;
        url = url.replace(/>www.fawour.com/g , ">Home");
        url = url.replace(/>fawour.com/g , ">Home");
        url = url.replace(/https:/ , "");
        document.writeln(url);
//-->

</script>

    </ol>
    <style>
    .body, .contentholder, .page-container{
        background-color:#ffffff !important;
    }
    .breadcrumb{
        background-color:rgba(0,0,0,0);
    }
    .todocontatiner22{
        background-color:#f7f7f7 !important;
    }
    .weareinbeta1{
        background-color:#f7f7f7 !important;
    }
    </style>
  @endif
        
            
               @yield('content')

        </div><!-- /.col-xs-12 main -->

    </div><!--/.row-->

  </div><!--/.container-->

</div><!--/.page-container-->
<style>

.tour-backdrop{position:fixed;top:0;right:0;bottom:0;left:0;z-index:1100;background-color:#000;opacity:.8;filter:alpha(opacity=80)}.tour-step-backdrop{position:relative;z-index:1101}.tour-step-backdrop>td{position:relative;z-index:1101}.tour-step-background{position:absolute!important;z-index:1100;background:inherit;border-radius:6px}.popover[class*=tour-]{z-index:1102}.popover[class*=tour-] .popover-navigation{padding:9px 14px}.popover[class*=tour-] .popover-navigation [data-role=end]{float:right}.popover[class*=tour-] .popover-navigation [data-role=prev],.popover[class*=tour-] .popover-navigation [data-role=next],.popover[class*=tour-] .popover-navigation [data-role=end]{cursor:pointer}.popover[class*=tour-] .popover-navigation [data-role=prev].disabled,.popover[class*=tour-] .popover-navigation [data-role=next].disabled,.popover[class*=tour-] .popover-navigation [data-role=end].disabled{cursor:default}.popover[class*=tour-].orphan{position:fixed;margin-top:0}.popover[class*=tour-].orphan .arrow{display:none}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.js"></script>
    <script src="/js/libs.js"></script>
    <script src="/js/js.js"></script>


    @yield('scripts.footer')

    @include('flash')
    @if ($signedIn)
    <style>
    .weareinbeta{
        background-color: #19bd9a;
        padding:5px 0;
        text-align: center;
    }
        .weareinbeta1{
        background-color: #ffffff;
        padding:5px 0;
        text-align: center;
    }
    .weareinbeta p{margin-top:6px;}
    .weareinbeta p a{color:#333333; text-decoration: underline;}
</style>
    <div class="weareinbeta" target="blank"><p> {{ trans('navi.whatshouldwe') }} <a href="https://docs.google.com/forms/d/1RgqHG4U8C_5wu0CcKXZ_EwZZM5Fzs5Is676JyG3-nsc/viewform">{{ trans('navi.improve') }}</a></p></div>
    @endif
<script>


document.getElementById('taketour').onclick = function()
   {
        startTour();
   }
function startTour(where){

    $.getScript( "/js/{{ trans('navi.theTour') }}.js" )
  .done(function( script, textStatus ) {
    if(where == null){
            if (tour.ended()) {
              tour.restart();
            } else {
              tour.init();
              tour.start();
        }
    }else{
        if (tour.ended()) {
              tour.restart();
              tour.goTo(where);
            } else {
              tour.init();
              tour.start();
              tour.goTo(where);
        }
    }
    
  })
  .fail(function( jqxhr, settings, exception ) {
});
   
}
function RunTourHere(theId)
   {
    window.location.href.split('#')[0]
    history.pushState(null, null, '#tour' + theId);
    tjeckhash();
   }
function tjeckhash(){
    if(window.location.hash) {
        var hash = location.href.substr(location.href.indexOf("#"));
        switch (hash) {
          case "#tour1":
           startTour(1);
            break;

            case "#tour2":
           startTour(5);
            break;

            case "#tour3":
           startTour(9);
            break;

            case "#tour4":
           startTour(11);
            break;

            case "#tour5":
           startTour(15);
            break;

            case "#tour6":
           startTour(17);
            break;

                case "#tour7":
           startTour(21);
            break;

        case "#tour8":
           startTour(23);
            break;

         case "#tour9":
           startTour(26);
            break;

        }
    } 
}
   tjeckhash();

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50238279-11', 'auto');
  ga('send', 'pageview');


</script>

</body>


</html>